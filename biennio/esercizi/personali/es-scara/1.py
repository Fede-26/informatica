#!/usr/bin/env python3

def leggi():
    x = int(input("a: "))
    y = int(input("b: "))
    return x, y

def somma(x, y):
    return x + y

def main():
    a, b = leggi()
    print(somma(a, b))

if __name__ == "__main__":
    main()
