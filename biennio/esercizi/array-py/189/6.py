#!/usr/bin/env python3

array = []

for i in range(10):
    array.append(int(input("var: ")))

for index, item in enumerate(array):
    if index % 2 == 1:
        array[index] = item * 2


print(array)
