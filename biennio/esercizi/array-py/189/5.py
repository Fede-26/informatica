#!/usr/bin/env python3

array = []

for i in range(10):
    array.append(int(input("var: ")))

media = sum(array) / len(array)

for item in array:
    if item > media:
        print(item)
