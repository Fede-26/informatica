#!/usr/bin/env python3

x = [[120, 134, 320, 153, 342, 153],
     [123, 312, 105, 184, 174, 263],
     [124, 102, 324, 156, 148, 139]]

print("incasso totale per ogni mese")
for i in range(6):
    print("mese", i, "incasso", x[0][i] + x[1][i] + x[2][i])

print("incasso totale per ogni reparto")
for i in x:
    print(sum(i))

print("incasso totale")
print(sum(x[0]) + sum(x[1]) + sum(x[2]))
