#!usr/bin/env python3

x = 1
n = int(input("n: "))
for i in range(1, n+1):
    x = x * i

print(x)