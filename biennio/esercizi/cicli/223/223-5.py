#!usr/bin/env python3

def isPrime(x):
    for i in range(2, x):
        if x%i == 0:
            return False
    return True

n = int(input("n: "))

for i in range(1, n):
    if isPrime(i):
        print(i)