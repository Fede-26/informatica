#!/usr/bin/env python3

def divisori(n):
    div = []
    for i in range(1, n):
        if n%i == 0:
            div.append(i)
    return div

def somma_div(n):
    x = 0
    for i in divisori(n):
        x += i
    return x

c = 2
c2 = 0
while c2 <= 10:
    x = somma_div(c)
    y = somma_div(x)
    if c == y:
        print("c = {}  c2 = {} --> {} - {}".format(c, c2, x, y))
        c2 += 1
    c += 1

print(somma_div(5020), somma_div(5564))