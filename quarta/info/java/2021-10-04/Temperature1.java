import java.util.Scanner;
public class Temperature1{
  static double tempera1, tempera2;// area globale
  static int letture1, letture2;
  static void leggiDati(){      // senza parametri
    Scanner in = new Scanner(System.in);
	int citta;
    double dato;
    System.out.print("\nCalcolo temperature medie\n");
    do{
      System.out.print("Digita il codice citta': ");
      citta = in.nextInt();
      if (citta != 0)    {
        System.out.print("inserisci temperatura: ");
   	    dato = in.nextDouble();
        switch (citta){
          case 1:
            tempera1 = tempera1 + dato;
            letture1 = letture1 + 1;    break;
          case 2:
            tempera2 = tempera2 + dato;
            letture2 = letture2 + 1;    break;
        }
      }
    }while(citta!=0);
  }
  static void elaboraDati(){ //header senza parametri
    double media1, media2;
    media1 = tempera1 / letture1;
    media2 = tempera2 / letture2;
    System.out.printf("media 1 vale:%.2f%n", media1);
    System.out.printf("media 2 vale:%.2f%n", media2);
  }
    public static void main(String[] args){
    leggiDati();
    elaboraDati();
  }
}

