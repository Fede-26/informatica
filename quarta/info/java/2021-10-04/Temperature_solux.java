import java.util.Scanner;
public class Temperature_solux{
  static final int  MAXLEN = 20; 
  static double tempera1, tempera2, tempera3, tempera4;   // area globale
  static int letture1, letture2, letture3, letture4;
  static void leggiDati(){    // header senza parametri
    Scanner in = new Scanner(System.in); 
	int citta;
    double dato;
    do{
      System.out.print("Digita il codice citta': ");
      citta = in.nextInt(); 
      if (citta != 0)    {
        System.out.print("inserisci temperatura : ");
   	    dato = in.nextDouble();  
        switch (citta) {
          case 1:
           tempera1 = tempera1 + dato;
           letture1 = letture1 + 1;     break;
          case 2:
           tempera2 = tempera2 + dato;
           letture2 = letture2 + 1;     break;
          case 3:
           tempera3 = tempera3 + dato;
           letture3 = letture3 + 1;     break;
          case 4:
           tempera4 = tempera4 + dato;
           letture4 = letture4 + 1;     break;
        }
      }
    } while (citta != 0); 
  }
  static void elaboraDati(){  // header senza parametri
   double media1, media2, media3, media4;
   media1 = tempera1 / letture1;
   media2 = tempera2 / letture2;
   media3 = tempera3 / letture3;
   media4 = tempera4 / letture4;
   System.out.printf("media 1 vale :%.2f%n", media1);
   System.out.printf("media 2 vale :%.2f%n", media2);    
   System.out.printf("media 3 vale :%.2f%n", media3);
   System.out.printf("media 4 vale :%.2f%n", media4);    
  }
  static void cancellaDati() { // header senza parametri   
    Scanner in = new Scanner(System.in); 
	System.out.print("Digita il codice citta': ");
    int citta = in.nextInt(); 
    if (citta != 0){
      switch (citta){
        case 1:
          tempera1 = 0;
          letture1 = 0;     break;
        case 2:
          tempera2 = 0;
          letture2 = 0;     break;
        case 3:
          tempera3 = 0;
          letture3 = 0;     break;
        case 4:
          tempera4 = 0;
          letture4 = 0;     break;
      }
    }
  }
  
  public static void main(String[] args){ 
    int scelta;  //  variabili locali
    do {
      System.out.print ("\f");// ripulisco lo schermo
      scelta = mostraMenu();
      System.out.print ("\f");
      switch (scelta) {
        case 1:{
          leggiDati();
          break;
        }
        case 2:{
          cancellaDati();
          break;
        }
        case 3:{
          elaboraDati();
          break;
        }
         case 4:{
        //  ... ();
          break;
        }
      }
    } while (scelta != 0);
  }
  // FUNZIONE DI CREAZIONE DEL MENU' UTENTE
  public static int mostraMenu(){
    Scanner in = new Scanner(System.in); 
    int scelta;
    do
    {
     System.out.print ("\f");
     System.out.print ("\t\t********************************************\n");
     System.out.print ("\t\t*   CALCOLO TEMPERATURE MEDIE              *\n");
     System.out.print ("\t\t********************************************\n");
     System.out.print ("\n");
     System.out.print ("\t\t********************************************\n");
     System.out.print ("\t\t*\tMenu' utente principale            *\n");
     System.out.print ("\t\t********************************************\n");
     System.out.print ("\t\t* 1 Inserimento dati                       *\n");
     System.out.print ("\t\t* 2 Azzeramento dati                       *\n");
     System.out.print ("\t\t* 3 Visualizzazione medie                  *\n");
     System.out.print ("\t\t* 0 =======> USCITA                        *\n");
     System.out.print ("\t\t********************************************\n\n");
     System.out.print ("\t\tInserire scelta (1, 2, 3  oppure 0)   : ");
     scelta = in.nextInt(); ;
     if ((scelta < 0) || (scelta > 3)){
       System.out.println ("\n\nERRORE Digitare la scelta corretta.....\n\n");
     //  system("PAUSE");
     }
     else;
    } while ((scelta < 0) || (scelta > 3));
    return scelta;
  }
}
