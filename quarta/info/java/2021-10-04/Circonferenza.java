import java.util.Scanner;
public class Circonferenza{
  final static double PIGRECA = 3.1415;   // costante comune

  public static void main(String[] args){
    int raggio;
    double perimetro, area;
    raggio = leggiDati();
    perimetro = calcolaPerimetro(raggio);
    area = calcolaArea(raggio);
    stampaRisultati(perimetro, area);
  }

  static int leggiDati(){
    Scanner in = new Scanner(System.in);    //crea uno scanner
	int dato;
    do{
      System.out.print("\nInserisci il raggio : ");
	  dato = in.nextInt();
    }while (dato <= 0);
    return dato;
  }
  static double calcolaPerimetro(int raggio){
    double numero1;
    numero1 = 2 * raggio * PIGRECA;
    return numero1;
  }
  static double calcolaArea(int raggio){
    double numero1;
    numero1 = raggio * raggio * PIGRECA;
    return numero1;
  }
  static void stampaRisultati(double dato1, double dato2){
    System.out.printf("la misura del perimetro e': %.2f%n", dato1);
    System.out.printf("la misura dell'area e': %.2f%n ", dato2);    
  }
}

//    System.out.format("la misura del perimetro e': %.2f%n", dato1);
//    System.out.format("la misura dell'area e':%.2f%n ", dato2);

