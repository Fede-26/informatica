import java.util.Random;
public class Dado{
  private int faccia;
  public Dado() {
    lancia();
  };
  // costruttore  di copia
  //-copia gli attributi in un altro oggetto
  public Dado (Dado altro) {
    this.faccia = altro.faccia;
  }
  // getter/setter
  int getFaccia(){
    return faccia;
  };
  void setFaccia (int num) { 
    faccia = num;
  };        
  // lancia il dado casualmente
  void lancia(){
    faccia = (int)(1 + Math.random() * 6);  
  };    
  // stampa i valori  
  void stampa(char dado){
    System.out.println ("dado "+dado+": " + getFaccia());   	
  };  
  public boolean equals(Object o) {
    if (o != null && getClass().equals(o.getClass())) {
      Dado b = (Dado)o;
      return (b.faccia == faccia);      
    }
    else return false;
  }
   
}

