import java.util.Scanner;
public class ProvaDado{
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    System.out.println("\nCopia di oggetti");
    // creazione di due oggetti  
    Dado dado1 = new Dado();
    // creo dado 2 copiando direttamente  
    Dado dado2 = dado1;              
    // creo dado 3 col costruttore di copia
    Dado dado3 = new Dado(dado1);  
    // visualizza i dadi
    System.out.println("-valori iniziali dei dadi");
    dado1.stampa('1');                      
    dado2.stampa('2'); 
    dado3.stampa('3'); 
    System.out.println("-confronto dado1 == dado3");
    if (dado1 == dado3)
      System.out.println("sono uguali!");
    else
      System.out.println("sono diversi!");
    System.out.println("-confronto dado1 = equals(dado3)");
    if (dado1.equals(dado3))
      System.out.println("sono uguali!");
    else
      System.out.println("sono diversi!");
    // lancio il dado1 
    System.out.println("-rilancio il dado 1");
    dado1.lancia();      
    // visualizza i dadi
    dado1.stampa('1');                   
    dado2.stampa('2');   
    dado3.stampa('3');   
    if (dado1.equals(dado3))
      System.out.println("sono uguali!");
    else
      System.out.println("sono diversi!");
  }
}