import java.util.Scanner;

class Main {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        
        int quanti = 20;
        Alunno[] lista = new Alunno[quanti];
        
        //inizializzazione
        for (int i = 0; i<quanti; i++)
        {
            lista[i] = new Alunno();
            System.out.println("Alunno n."+i);
            System.out.print("Nome: ");
            lista[i].setName = in.nextLine();
        }
        
        int scelta = menu();
        if (scelta == 1)
        {
            for (int i = 0; i<quanti; i++)
            {
                System.out.println(lista[i].getName);
            }
        }
        return;
    }

    static int menu()
    {
        Scanner in = new Scanner(System.in);
        int scelta;
        do {
            System.out.println("*******************************");
            System.out.println("*  Gestione elenco alunni     *");
            System.out.println("*                             *");
            System.out.println("*  1: Visualizza gli alunni   *");
            System.out.println("*  2: Aggiungi voto alunno    *");
            System.out.println("*  3: Visualizza media alunno *");
            System.out.println("*  4: Resetta voti alunno     *");
            System.out.println("*  0: Esci                    *");
            System.out.println("*******************************");
            scelta = in.nextInt();
        } while(!(scelta<=4 && scelta >=0));
        return scelta;
    }
}