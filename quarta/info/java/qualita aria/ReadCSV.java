import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.util.Scanner; // Import the Scanner class to read text files

import javax.sound.sampled.SourceDataLine;

import java.util.ArrayList; // import the ArrayList class


public class ReadCSV {
    public static void main(String[] args){
        try {
            
            ArrayList<String[]> csvPosition = new ArrayList<String[]>(); // Create an ArrayList object
            ArrayList<String[]> csvData = new ArrayList<String[]>(); // Create an ArrayList object
            File fPosition = new File("Stazioni_qualit__dell_aria.csv");
            File fData = new File("Dati_sensori_aria.csv");
            Scanner readerPosition = new Scanner(fPosition);
            Scanner readerData = new Scanner(fData);

            readerPosition.nextLine();
            while (readerPosition.hasNextLine()) {
                String data = readerPosition.nextLine();
                String[] arrOfData = data.split(",");
                csvPosition.add(arrOfData);
            }

            readerData.nextLine();
            while (readerData.hasNextLine()) {
                String data = readerData.nextLine();
                String[] arrOfData = data.split(",");
                csvData.add(arrOfData);
            }
            
            readerPosition.close();
            readerData.close();
            
            System.out.println(csvPosition.size());
            System.out.println(csvData.size());


            for (int i=0; i<csvPosition.size(); i++) {
                System.out.print(csvPosition.get(i)[4]);
                System.out.print(" -> ");
                System.out.print(csvPosition.get(i)[1]);
                System.out.print(": ");
                
                System.out.print(csvData.get(i)[2]);
                System.out.println(csvPosition.get(i)[2]);
            }

        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}