//Classe motocicletta per creare oggetti moto

package veicoli;
import java.util.Random;

public class Motocicletta {
    private Random rd = new Random();
    private String marca;
    private String modello;
    private String colore;
    private int velMax;
    private int velNow;
    private float kmPercorsi;

    private String[] listMarche = {"Yamaha", "BMW", "MV", "Aprilia", "Suzuki"};
    private String[] listModelli = {"450", "350", "200"};
    private String[] listColori = {"nera", "bianca", "grigia", "gialla", "fucsia", "blu"};
    
    //costruttori
    public Motocicletta() {
        marca = listMarche[rd.nextInt(5)];
        modello = listModelli[rd.nextInt(3)];
        colore = listColori[rd.nextInt(6)];
        velMax = rd.nextInt(80+201)+80;     //velocità massima tra 80 e 200
        velNow = 0;
        kmPercorsi = 0;
    }
    
    public Motocicletta(String ma, String mo, String col, int vm) {
        marca = ma;
        modello = mo;
        colore = col;
        velMax = vm;
        velNow = 0;
        kmPercorsi = 0;
    }
    
    //getter e setter
    public String getMarca() {
        return marca;
    }
    
    public String getModello() {
        return modello;
    }
    
    public String getColore() {
        return colore;
    }

    public int getVelMax() {
        return velMax;
    }
    
    public int getVelNow() {
        return velNow;
    }

    public void setVelNow(int v) {
        velNow = v;
        if (velNow < 0)
            velNow = 0;
        else if (velNow > velMax)
            velNow = velMax;
        return;
    }
    
    public float getKmPercorsi() {
        return kmPercorsi;
    }
    
    public void aggiornaKmPercorsi(float km) {
        kmPercorsi += km; 
    }
    
}
