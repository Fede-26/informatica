//Main con simulazione della gara
package simulazione_verifica;

import veicoli.Motocicletta;

import java.util.Scanner;
import java.util.Random;

public class Simulazione_verifica {

    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        Random rd = new Random();
        
        int quanteMoto = 4;
        
        //creazione delle 4 moto
        String marca;
        String modello;
        String colore;
        int velMax;
        String parametri;
        
        Motocicletta listaMoto[] = new Motocicletta[quanteMoto];
        for (int i = 0; i<4; i++) {
            System.out.print("Inserire i parametri per moto numero " + (i+1) + " (y/n): ");
            parametri = sc.nextLine();
            
            if (parametri == "y") {
                System.out.print("Inserire marca per moto numero " + (i+1) + ": ");
                marca = sc.nextLine();
                System.out.print("Inserire modello per moto numero " + (i+1) + ": ");
                modello = sc.nextLine();
                System.out.print("Inserire colore per moto numero " + (i+1) + ": ");
                colore = sc.nextLine();
                System.out.print("Inserire velocità massima per moto numero " + (i+1) + ": ");
                velMax = Integer.parseInt(sc.nextLine());
                listaMoto[i] = new Motocicletta(marca, modello, colore, velMax);
        
            }
            else {
                listaMoto[i] = new Motocicletta();
                System.out.println("Marca: " + listaMoto[i].getMarca());
                System.out.println("Modello: " + listaMoto[i].getModello());
                System.out.println("Colore: " + listaMoto[i].getColore());
                System.out.println("Velocità massima: " + listaMoto[i].getVelMax());
            }
        }

        //inizio simulazione
        int deltaV;
        float km;
        int winner = 0;
        boolean isRunning = true;
        float tempo = (float) 1;  //tempo tra un ciclo e un altro, secondi
        
        while (isRunning) {
            for (int i = 0; i<4; i++) {
                deltaV = rd.nextInt(51)-25; //delta tra -10 e 10
                listaMoto[i].setVelNow(listaMoto[i].getVelNow()+deltaV);

                km = ((float)listaMoto[i].getVelNow() * (tempo / (float) 60));
                listaMoto[i].aggiornaKmPercorsi(km);

                //prints
                System.out.println("Moto " + (i+1) + " km " + listaMoto[i].getKmPercorsi() + " vel " + listaMoto[i].getVelNow());
                
                if (listaMoto[i].getKmPercorsi() >= 10) {
                    winner = i;
                    isRunning = false;
                    break;
                }
            }
            
            System.out.println();
            //Non sono riuscito ad implementare una sleep in un ciclo
        }
        
        System.out.println("Vincitore: moto " + (winner+1) + " (" + listaMoto[winner].getMarca() + " " + listaMoto[winner].getModello() + " " + listaMoto[winner].getColore() + ")");
    
    }
    
}
