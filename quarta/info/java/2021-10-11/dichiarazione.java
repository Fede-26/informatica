//Genera casualmente 10 numeri, memorizzali in un array ed effettua lo scambio tra il massimo e mi-nimo elemento (supponendo che i valori dell’array siano tutti distinti tra loro).
import java.util.Scanner;
public class dichiarazione{

  final static int TANTI = 10;  // costanti condivise
  final static int MAX = 30;
  public static void coppie(int[] a){
    int volte = 0;
    for (int x = 0; x < a.length; x++){
      for (int k = 0; k < a.length; k++){
        if (a[x]==a[k]){
          volte++;
        }
      }
      System.out.println(a[x]+" -> "+volte);
    }
  }

  public static void main(String[] args){
    int max, maxi;
    int min, mini;
    // creo il vettore di  TANTI elementi
    int mioVettore[] = new int[TANTI];
    // riempio il vettori con numeri casuali tra 0 e MAX
    for (int x = 0; x < mioVettore.length; x++)
      mioVettore[x] = (int)(MAX * Math.random());
    // visualizzo il contenuto del vettore


    min = mioVettore[0];
    mini = 0;

    max = mioVettore[0];
    maxi = 0;



    for (int x = 0; x < mioVettore.length; x++)
    {
      System.out.print(mioVettore[x]+" " );

      if (mioVettore[x]>max){
        max=mioVettore[x];
        maxi = x;
      }

      if (mioVettore[x]<min){
        min=mioVettore[x];
        mini = x;
      }
    }

    mioVettore[mini] = max;
    mioVettore[maxi] = min;
    System.out.println();
    for (int x = 0; x < mioVettore.length; x++)
      System.out.print(mioVettore[x]+" " );

    coppie(mioVettore);
  }

}

