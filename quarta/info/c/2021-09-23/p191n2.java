import java.util.Scanner;
public class p191n2{
	public static void main(String[] args) {
		float err = 0;

		Scanner in = new Scanner(System.in);

		System.out.print("Tolleranza in %: ");
		float tolleranza = in.nextFloat();
		System.out.print("\nNominale: ");
		float nominale = in.nextFloat();

		float min = nominale - (tolleranza * 100 / nominale);
		float max = nominale + (tolleranza * 100 / nominale);

		float pacchettiDiPasta = 0;
		
		do{
			System.out.print("\nInput pasta: ");
			float pasta = in.nextFloat();
			pacchettiDiPasta = pacchettiDiPasta + 1;
			
			if ((pasta > max) || (pasta < min))
				err = err + 1;

		} while(err < 5);

		System.out.println("\nErr di impacchettamento: " + (err / pacchettiDiPasta * 100));

	}
}
