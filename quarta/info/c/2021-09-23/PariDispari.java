import java.util.Scanner;
public class PariDispari{
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Input: ");
		int num1 = in.nextInt();
		while(num1 > 1) {
			num1 = num1 - 2;
		}

		if (num1 == 0)
			System.out.println("\n pari");
		else
			System.out.println("\n dispari");
	}
}
