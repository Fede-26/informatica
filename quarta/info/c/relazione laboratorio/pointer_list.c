//-- IMPORTAZIONE LIBRERIE --//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LENGTH 50

//-- DICHIARAZIONE ELEMENTO DELLA LISTA --//
typedef struct elemento {
    char valore[MAX_LENGTH];    //valore dell'elemento (stringa)
    struct elemento * next;     //puntatore all'elemento successivo nella lista
} elemento_t;

//-- FUNZIONI --//
elemento_t * creaLista()    //creazione della lista: allocazione della memoria del primo elemento con la funzione malloc
{
    elemento_t * h = NULL;
    h = (elemento_t *) malloc(sizeof(elemento_t));
    strcpy(h->valore, "");
    h->next = NULL;
    return h;              //restituisce il primo elemento della lista (head)
}

void creaElemento(elemento_t * h, int n)    //aggiunge "n" elementi alla lista
{
    elemento_t * now = h;
    while (now->next != NULL)      //ciclo per arrivare all'ultimo elemento della lista
    {
        now = now->next;
    }

    int i;
    for (i = 0; i < n; i++)        //aggiunge "n" elementi
    {
        now->next = (elemento_t *) malloc(sizeof(elemento_t));
        strcpy(now->next->valore, "");
        now->next->next = NULL;
        now = now->next;
    }
    return;
}

void assegnaValore(elemento_t * h, int index, char * val)    //assegna un valore "val" all'elemento di indice "index"
{
    elemento_t * now = h;
    int i;
    for (i = 0; i < index; i++)
    {
        now = now->next;
    }

    strcpy(now->valore, val);

    return;
}

void visualizzaValore(elemento_t * h, int index, char * stringa)    //copia il valore (stringa) dell'elemento di indice "index" nella variabile "stringa"
{
    elemento_t * now = h;
    int i;
    for (i = 0; i < index; i++)
    {
        now = now->next;
    }

    strcpy(stringa, now->valore);    //scrive nella stringa l'elemento desiderato

    return;
}

int lunghLista(elemento_t * h)        //restituisce la lunghezza della lista
{
    elemento_t * now = h;
    int i;
    for (i = 1; now->next != NULL; i++)        //parte dall'inizio, usa NULL come tappo
    {
        now = now->next;
    }
    return i;
}

void eliminaElemento(elemento_t ** h, int index)    //elimina un elemento di indice "index" dalla lista
{
    if (lunghLista(*h)!=0)
    {
        if (index == 0)    //se l'elemento da eliminare è il primo, sposta il puntatore "head" al secondo elemento
        {
            elemento_t * elSucc = (*h)->next;
            free(*h);
            *h = elSucc;
            return;
        }

        //se l'elemento non è il primo, sovrascrive il puntatore "next" dell'elemento precedente, puntando all'elemento successivo
        elemento_t * now = (*h);
        elemento_t * temp = NULL;
        int i;
        for (i = 0; i < index-1; i++)
        {
            now = now->next;
        }

        temp = now->next;
        now->next = temp->next;
        free(temp);    //dealloca la memoria utilizzata
    }
    return;
}

void salvaListaFile(elemento_t * h, char nomeFile[])   //salva la lista su un file
{
    elemento_t * now = h;
    FILE * pFile = fopen(nomeFile, "w");
    if (pFile != NULL)
    {
        while(now != NULL)      //scorre tutti gli elementi e li salva
        {
            fprintf(pFile, "%s\n", now->valore);
            now = now->next;
        }
    }
    fclose(pFile);
    return;
}

int caricaListaFile(elemento_t * h, char nomeFile[])   //carica la lista da un file
{
    FILE * pFile = fopen(nomeFile, "r");
    char * riga;
    size_t len = 0;
    ssize_t read;
    int i = 0;
    int lunghezza = lunghLista(h);

    if (pFile != NULL)
    {
        while ((read = getline(&riga, &len, pFile))  != -1)
        {
            creaElemento(h, 1);                     //crea un elemento per ogni riga
            riga[strlen(riga)-1] = '\0';            //rimuove il \n dalla riga
            assegnaValore(h, lunghezza + i, riga);  //salva la riga
            printf("%s", riga);
            i++;
        }
    }
    fclose(pFile);
    free(riga);
    return i;
}

//-- MENU --//
int menu()  //restituisce la scelta effettuata
{
    int scelta;
    do
    {
        printf("\n*****************************\n");
        printf(  "*                           *\n");
        printf(  "*  1. visualizza elementi   *\n");
        printf(  "*  2. aggiunge elemento     *\n");
        printf(  "*  3. elimina elemento      *\n");
        printf(  "*  4. cerca nella lista     *\n");
        printf(  "*  5. salva lista su file   *\n");
        printf(  "*  6. carica lista da file  *\n");
        printf(  "*  7. esci                  *\n");
        printf(  "*                           *\n");
        printf(  "*****************************\n");
        printf("\n>> ");
        scanf("%i", &scelta);
    }while (!(scelta>=1 && scelta <= 7));
    return scelta;
}

//-- MAIN --/
int main(int argc, char** argv)
{
    char stringa[MAX_LENGTH];           //stringa contenente il prodotto
    char search[MAX_LENGTH];            //stringa per la ricerca
    elemento_t * head = creaLista();    //creazione della lista

    int i, l, index, daAggiungere;
    char nomeFile[MAX_LENGTH];          //nome del file da usare nel salvataggio

    while (1)
    {
        switch (menu()) {

            case 1:
                printf("\n\n");
                for (i = 0; i < lunghLista(head); i++)        //visualizzazione di ogni elemento richiamando la funzione tante volte quante la lunghezza
                {
                    visualizzaValore(head, i, stringa);
                    printf("%i -> %s\n", i, stringa);        //'stringa' sarà riempita dalla funzione
                }
                break;

            case 2:
                printf("\n<< Quanti elementi aggiungere?\n>> ");
                scanf(" %i", &daAggiungere);
                l = lunghLista(head);
                creaElemento(head, daAggiungere);           //aggiunge "daAggiungere" elementi alla lista
                for (i = l; i < l + daAggiungere; i++)      //viene chiesto il valore da inserire nei nuovi elementi
                {
                     printf("\n<< Aggiungere al posto %i\n>> ", i);
                    scanf(" %[^\n]", &stringa);
                    assegnaValore(head, i, stringa);
                }

                visualizzaValore(head, 0, stringa);
                if (daAggiungere != 0 && strlen(stringa) == 0)  //se sono i primi elementi aggiunti, il primo elemento viene eliminato perche' vuoto
                {
                    eliminaElemento(&head, 0);
                }

                break;

            case 3:
                if (lunghLista(head)==1)    //non e' possibile eliminare tutti gli elementi perchè head deve rimanere
                {
                    printf("<< Impossibile eliminare elemento perche' ne deve rimanere almeno uno\n");
                    break;
                }

                printf("<< Quale elemento eliminare?\n>> ");
                scanf(" %i", &index);
                eliminaElemento(&head, index);
                break;

            case 4:
                printf("<< Stringa da cercare\n>> ");
                scanf(" %[^\n]", search);
                for (i = 0; i < lunghLista(head); i++)      //scorre tutti gli elementi e se la stringa e' presente lo stampa
                {
                    visualizzaValore(head, i, stringa);
                    if(strstr(stringa, search) != NULL)
                    {
                        printf("%i -> %s\n", i, stringa);
                    }
                }
                break;

            case 5:
                printf("<< Inserire nome del file su cui salvare la lista\n>> ");
                scanf(" %s", nomeFile);
                salvaListaFile(head, nomeFile);
                break;

            case 6:
                printf("<< Inserire nome del file su cui recuperare la lista\n>> ");
                scanf(" %s", nomeFile);
                if (caricaListaFile(head, nomeFile) != 0 )  //se sono i primi elementi aggiunti, il primo elemento viene eliminato perche' vuoto
                {
                    visualizzaValore(head, 0, stringa);
                    if (strlen(stringa) == 0)
                    {
                        eliminaElemento(&head, 0);
                    }

                }
                break;

            case 7:
                return 0;   //esce dal programma
        }
    }
}
