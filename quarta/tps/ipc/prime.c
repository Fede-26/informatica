#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdint.h>

#define FINE 99999

int ncpu = 4;			//numero core
int nchildren = 1;
int primi = 1;
int ultimo = FINE;		//ultimo numero su cui i figli stanno lavorando
int pidFiglio;
int status;
int pid;

int isPrime(int n)
{
    sleep(0.01);
    int is = 1;
    for (int i = 2; i<n; i++)
    {
        if (n%i == 0)
            is = 0;
    }
    if (is)
        printf("%i\n", n);
    return is;
}

int main()
{
    //fase 1

    for (; nchildren <= ncpu; nchildren++)
    {
        pid = fork();
        if (!pid)
        {
            return isPrime(ultimo);
        }
        ultimo -= 2;
    }


    //fase 2
    while (ultimo >= 3)
    {
        pid = wait(&status);
        nchildren--;
        if (status)
        {
            primi++;
        }
        pid = fork();
        if (!pid)
        {
            return isPrime(ultimo);
        }
        ultimo -= 2;
        nchildren++;


    }

    //fase 3 (2.1)
    for (int i = 0; i<4; i++)
    {
        pid = wait(&status);
        if (status)
        {
            primi++;
        }

    }


    printf("Primi => %i\n", primi);

    return 0;
}
