#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdint.h>
//#include <time.h>

static int n = 0;

int baby(int n)
{
	//static int n = 0;
	//n = n + 1;
	static int m;
	printf("n: %i\n", n);
	printf("m: %p\n", &m);
	
	printf("PID forked: %jd\n", (intmax_t) getpid());

	//srand((unsigned) &m );
 	srand((unsigned) time (NULL));
	int timer = (rand() % 15 ) + 5;
 	sleep(timer);
 	
 	//srand((unsigned) time (NULL));
 	int rcode = (rand() % 126) + 1 + n;
 	return rcode;
}

int main()
{
	pid_t pidForked, pidFiglio;
	int status;
	int kinderganden = 4;
	
	for (int id=0; id<kinderganden; id++) {
		sleep(2);
		pidForked = fork();
		if (pidForked == 0)
		{
			int r = baby(id);
			return r;
		}
	}

	//while ((w = wait(&status)) > 0)
	do
	{
		pidFiglio = wait(&status);
		if(pidFiglio > 0)
			printf("%i -- %i\n", pidFiglio, status>>8);
	} while(pidFiglio > 0);
	return 0;

}
