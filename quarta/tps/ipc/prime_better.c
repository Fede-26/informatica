#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdint.h>

int figlio(int da, int a, int nfiglio)
{
    //sleep(0.01);
    char fn[] = "nf0";
    fn[2] = (char) nfiglio + 48;
    FILE * fd = fopen(fn, "w");
    int is;
    int i, j;
    for (i = da; i<=a; i++)
    {
        is = 1;
        for (j = 2; j<i && is==1; j++)
        {
            if (i%j == 0)
                is = 0;
        }
        if (is)
            fprintf(fd, "%i\n", i);
    }
    fclose(fd);
    return 0;
}

int main(int argc, char ** argv)
{
    //fase 1

    int ncpu = 4;
    int fine = 100000;
    int nfigli, primiTot = 0, pid;
    int da, a;

    if (argc == 3)
    {
        ncpu = atoi(argv[1]);
        fine = atoi(argv[2]);
    }

    int dimBlocco = (fine-2)/ncpu;

    for (nfigli = 0; nfigli<ncpu; nfigli++)
    {
        da = nfigli * dimBlocco + 2;
        a = da + dimBlocco;
        pid = fork();
        if (pid==0)
        {
            return figlio(da, a, nfigli);
        }
    }


    //fase 2
    int status;
    for (nfigli = 0; nfigli<ncpu; nfigli++)
    {
        wait(&status);
    }

    //fase 3 (recuperare i primi dai file)
    char c;
    for (int i = 0; i<ncpu; i++)
    {
        char fn[] = "nf0";
        fn[2] = (char) i + 48;
        FILE * fd = fopen(fn, "r");
        for (c = getc(fd); c != EOF; c = getc(fd))
           if (c == '\n') // Increment count if this character is newline
                primiTot++;

        // Close the file
        fclose(fd);
    }


    printf("Primi => %i\n", primiTot);

    return 0;
}
