#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <features.h>
#include <sys/wait.h>
#include <sys/types.h>

// #define FIN 300000	/*0.3 M*/
#define FIN 30000	/*0.3 M*/
#define Star 2
/*#define deb 1*/


int isPerfect(int);

int main (int argc, char *argv[])
{
    int pid,status;
    int pid1;
    int n,n1,i=0;
    int deb=argc;
    if (argc>1)
        deb=0;
    else
        deb=1;

    printf("deb=%d\t argc=%d.\n",deb,argc);
    n=Star;

    pid=fork();
	if (pid==0)
	{
		return isPerfect(n);
	}
	pid1=pid; 
	n1=n;
	n=n+1;
		
	pid=fork();
	if (pid==0)	
	{
		return isPerfect(n);
	}

	n=n+1;
	
	while (n<FIN)
	{

		pid=wait(&status);
		status=WEXITSTATUS(status); 
		n=n+1;
		if (pid==pid1)
		{
			if (status==1)
			{
				if (deb==0)
                    printf("1 says \t%d\t is perfect.\n",n1);
				i++;
			}
			
			pid=fork();
			if (pid==0)
			{
				return isPerfect(n);
			}
			pid1=pid; 
			n1=n;
		}
		else 
		{
			
			if (status==1)
			{
				if (deb==0)
                    printf("2 says \t%d\t is perfect.\n",n1);
				i++;
			}
			
			pid=fork();
			if (pid==0)
			{
				return isPerfect(n);
			}
		}
			
	}
/*	if (deb==1)*/	printf("Found %i perfect numbers below %i.\n",i,FIN);
	return 0;  
}

int isPerfect(int n)
{
	int i;
	int sum=0;
	i=1;
	
	while (i<n)
	{
		if ( (n%i)==0)
            sum+=i;
		i=i+1;
	}
	
	if (sum==n)	
	{
		printf("found %i \n",n);
		return 1;
	}
	else
        return 0;
}
