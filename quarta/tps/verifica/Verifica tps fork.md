Verifica TPS - Zotti Federico - 2021.11.10
# Descrizione del programma

## wiki
In matematica, un numero perfetto è un numero naturale che è uguale alla somma dei suoi divisori positivi, escludendo il numero stesso.

## main
Genera 2 figli e entra in un ciclo.
Ogni volta che un figlio termina, ne genera un altro e gli assegna un valore `n` incrementato (`n` va da `Star` a `FIN`).
Se `deb == 0` stampa il valore di n se il figlio ritorna 1, quindi il numero è perfetto.
Quando `n == FIN`, stampa la quantità di numeri trovati e termina.

## isPerfect
Somma tutti i divisori di un numero e controlla se essa e uguale al numero stesso.
In quel caso ritorna 1.

# Correzione
- In `printf("2 says \t%d\t is perfect.\n",n1);`, è sbagliato perchè quello che printa è il numero assegnato al figlio "1", non al "2"
- Dopo la seconda fork non bisogna incrementare `n=n+1`

# Ottimizzazione
Per ottimizzarlo ho provato ad implementare un numero variabile di figli contemporanei (`ncpu`).
I pid e numeri assegnati ai processi vengono salvati negli array rispettivamente `pids` e `numbers`.
Ogni volta che un figlio termina viene controllato il suo pid nell'array e si trova a quale figlio appartiene.
Solo se viene specificato un secondo argomento (`deb`, senza un valore specifico) viene visualizzato il valore nel main, altrimenti viene stampato solo dalla funzione `isPerfect`.

Con più tempo avrei voluto implementare una divisione su ulteriori figli della ricerca dei divisori come nel programma della ricerca dei numeri primi.

## Tempi
Con `FIN = 30 000`

| n     | Originale (ncpu=2) | v2.0 (ncpu=1) | v2.0 (ncpu=2) | v2.0 (ncpu=4) |
| ----- | ------------------ | ------------- | ------------- | ------------- |
| 1     | $2.91$             | $5.64$        | $3.63$        | $2.39$        |
| 2     | $3.17$             | $5.38$        | $3.08$        | $2.39$        |
| 3     | $3.45$             | $5.44$        | $2.97$        | $2.77$        |
| 4     | $3.46$             | $5.97$        | $3.12$        | $2.65$        |
| 5     | $3.44$             | $5.28$        | $3.43$        | $2.30$        |
| Media | $3.29$             | $5.54$        | $3.25$        | $2.50$        | 
