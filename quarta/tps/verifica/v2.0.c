#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <features.h>
#include <sys/wait.h>
#include <sys/types.h>

#define FIN 300000	/*0.3 M*/
//#define FIN 30000	/*0.3 M*/
#define Star 2	
/*#define deb 1*/


int isPerfect(int);

int main (int argc, char *argv[])
{
    int pid,status;
    int ncpu = 1;
    int n,i=0,j=0;
    int deb=1;
    if (argc==1)
    {
        printf("Sintassi: comando ncpu [deb]\ndeb --> visualizzare il valore dal main");
        return 1;
    }
    else if (argc==2)
    {    ncpu = atoi(argv[1]);
        printf("%i", ncpu);
    }
    else if (argc==3)
    {
        ncpu = atoi(argv[1]);
        printf("%i", ncpu);
        deb=0;
    }
    int pids [ncpu];
    int numbers [ncpu];

    printf("deb=%d\t argc=%d.\n",deb,argc);
    n=Star;

    for (i=0; i<ncpu; i++)
    {
        pid=fork();
        if (pid==0)
        {
            return isPerfect(n);
        }
        pids[i]=pid;
        numbers[i]=n;
        n=n+1;
    }

	while (n<FIN)
	{
		pid=wait(&status);
		status=WEXITSTATUS(status);
        for (i=0;i<ncpu;i++)
        {
            if (pid==pids[i])
            {
                if (status==1)
                {
                    if (deb==0)
                        printf("%d says \t%d\t is perfect.\n",i+1, numbers[i]);
                    j++;
                }

                pid=fork();
                if (pid==0)
                {
                    return isPerfect(n);
                }
                pids[i]=pid;
                numbers[i]=n;
                n=n+1;
            }
        }
    }
/*	if (deb==1)*/	printf("Found %i perfect numbers below %i.\n",j,FIN);
	return 0;  
}

int isPerfect(int n)
{
	int i;
	int sum=0;
	i=1;
	
	while (i<n)
	{
		if ( (n%i)==0)
            sum+=i;
		i=i+1;
	}
	
	if (sum==n)	
	{
		printf("found %i \n",n);
		return 1;
	}
	else
        return 0;
}
