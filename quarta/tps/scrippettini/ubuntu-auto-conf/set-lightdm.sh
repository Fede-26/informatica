#!/bin/bash
set -e

## Functions

install_app () {
    #TODO(da controllare)
    sudo apt install -y vim lyx xournalpp geany nmap dns-utils thonny chrome vlc micro jdk-openjdk gcc tlp powertop
    # snap okular
    # tlpui
}

add_lightdm () {
    sudo apt install -y lightdm
    sudo sh -c 'printf "[Seat:*]\nallow-guest=true\n" > /etc/lightdm/lightdm.conf.d/40-enable-guest.conf'
}


## Prompts

read -n 1 -p "Install programs (y/N)?" choice
case "$choice" in 
  y|Y ) install_app;;
  n|N ) echo "Nope: skipped";;
  * ) echo "invalid input: skipped";;
esac

read -n 1 -p "Add lightdm (y/N)?" choice
case "$choice" in 
  y|Y ) add_lightdm;;
  n|N ) echo "Nope: skipped";;
  * ) echo "invalid input: skipped";;
esac
