#!/bin/bash
set -e

## Variables

GUEST_NAME=guest
echo "GUEST_NAME: $GUEST_NAME"
GUEST_PASSWD=$GUEST_NAME
echo "GUEST_PASSWD: $GUEST_PASSWD"


## Functions

add_user () {
	echo "Adding user"
	useradd $GUEST_NAME \
		--password $(echo $GUEST_PASSWD | openssl passwd -1 -stdin) \
		--user-group \
		--create-home \
		--shell /bin/bash
	echo "D"
	#enable passwordless login for user in $GUEST_NAME group
	sed "2iauth sufficient pam_succeed_if.so user ingroup $GUEST_NAME\n" /etc/pam.d/gdm-password
	echo "Done"
}

add_autostart_service () {
	echo -e "[Desktop Entry]\n\
Type=Application\n\
Name=Remove guest home\n\
Exec=/usr/bin/remove-home.sh\n\
X-GNOME-Autostart-enabled=true\n" > /etc/xdg/autostart/00_remove-home-guest.desktop

	echo -e "#!/bin/bash
if [ $(whoami) = guest ]\n\
then\n\
rm -f /home/guest/*\n\
rm -fr /home/guest/*\n\
fi" > /usr/bin/remove-home.sh
}


## Prompts

read -n 1 -p "Add user (y/N)?" choice
case "$choice" in 
  y|Y ) add_user;;
  n|N ) echo "Nope: skipped";;
  * ) echo "invalid input: skipped";;
esac

read -n 1 -p "Add autostart service (y/N)?" choice
case "$choice" in 
  y|Y ) add_autostart_service;;
  n|N ) echo "Nope: skipped";;
  * ) echo "invalid input: skipped";;
esac
