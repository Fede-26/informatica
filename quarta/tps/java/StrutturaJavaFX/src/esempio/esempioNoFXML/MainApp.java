/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esempio.esempioNoFXML;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author giuseppe.depietro
 */
public class MainApp extends Application implements EventHandler<ActionEvent> {
    private Button btn;
    @Override
    public void start(Stage primaryStage) {
        btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(this);
        /*
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        */
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

   

    @Override
    public void handle(ActionEvent event) {
       if (event.getSource()==btn){
            System.out.println("Hello World!");
       }
    }
    
}
