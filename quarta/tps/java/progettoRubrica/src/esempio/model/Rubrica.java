/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esempio.model;

import java.util.ArrayList;

/**
 *
 * @author giuseppe.depietro
 */
public class Rubrica {
    private ArrayList<Persona> elencoPersone;

    public Rubrica() {
        
        elencoPersone=new ArrayList<Persona>();
    }
    
    public void aggiungiPersona(Persona p){
        elencoPersone.add(p);
    }
    
    /**
     * Metodo che aggiunge una persona partendo dal nome e cognome
     * @param cognome Cognome della persona da aggiungere
     * @param nome Nome della persona da aggiungere
     */
    public void aggiungiPersona(String cognome,String nome){
        Persona p=new Persona(cognome, nome);
        aggiungiPersona(p);
    }
    
    /**
     * Metodo che ci restituisce un stringa con tutte le persone e ogni persona è separata da un ritorno a capo (\n) 
     * @return String contentente i dettagli della persona su ogni riga
     */
    public String ottieniListaPersone(){
        String lista="";
        for (int i=0;i<elencoPersone.size();i++){
            lista+=elencoPersone.get(i)+"\n";
            
        }
        
        return lista;
    }
    
    
    
    
    
    
    
    
}
