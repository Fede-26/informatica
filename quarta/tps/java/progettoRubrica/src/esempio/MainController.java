/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esempio;


import esempio.model.Rubrica;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author giuseppe.depietro
 */
public class MainController {
    private MainApp mainApp;
    private Rubrica rubrica;
    
     @FXML
     TextField txtCognome;
     @FXML
     TextField txtNome;
     @FXML
     TextField txtTelefono;
     @FXML
     TextField txtEmail;
        
     @FXML
     TextArea taElenco;
    
    @FXML
    private void initialize(){
        
    }
    
    @FXML
    private void aggiungiPersona(){
        String cognome=txtCognome.getText();
        String nome=txtNome.getText();
        String telefono=txtTelefono.getText();
        String email=txtEmail.getText();
        taElenco.appendText(cognome+" "+nome+"\n");
        rubrica.aggiungiPersona(cognome, nome);
        txtCognome.clear();       
        
        txtNome.clear();
        txtTelefono.clear();
        txtEmail.clear();
        //mainApp.aggiungiPersona(cognome, nome);
    }
    
    /**
     * 
     * @param mainApp 
     */
    void setMainApp(MainApp mainApp) {
        //To change body of generated methods, choose Tools | Templates.
        this.mainApp=mainApp;
        rubrica=mainApp.getRubrica();
    }
    
    @FXML
    private void azzeraVisualizzazioneLista(){
        taElenco.clear();
    }
    
    @FXML
    /**
     * 
     */
    private void visualizzaLista(){
        azzeraVisualizzazioneLista();        
        taElenco.setText(rubrica.ottieniListaPersone());
        
    }
    
}
