/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esempio.model;
import java.io.Serializable;

public class Persona implements Serializable{
    //attributi obbligatori
    private String cognome;
    private String nome;
    private String citta;
    private String email;
    //attributi opzionali
    private String telefono;    
    private String indirizzo;

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }


    public Persona(String cognome, String nome) {
        this.cognome = cognome;
        this.nome = nome;
    }

    public Persona(String cognome, String nome, String citta, String email, String telefono, String indirizzo) {
        this.cognome = cognome;
        this.nome = nome;
        this.citta = citta;
        this.email = email;
        this.telefono = telefono;
        this.indirizzo = indirizzo;
    }

    public Persona() {
    }

    @Override
    public String toString() {
        return cognome + " "+ nome +" " +email+" "+ telefono +" " +citta+" "+ indirizzo;
    }
    
    
    
    
    
}
