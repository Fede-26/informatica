/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esempio;


import esempio.model.Rubrica;
import esempio.model.Persona;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;

import java.util.ArrayList;


/**
 *
 * @author giuseppe.depietro
 */
public class MainController {
    private MainApp mainApp;
    private Rubrica rubrica;
    
     @FXML
     TextField txtCognome;
     @FXML
     TextField txtNome;
     @FXML
     TextField txtTelefono;
     @FXML
     TextField txtEmail;     
     @FXML
     TextField txtCity;
     @FXML
     TextField txtAddress;
     
     @FXML
     CheckBox cboxCognome;
     @FXML
     CheckBox cboxNome;
     @FXML
     CheckBox cboxEmail;
     @FXML
     CheckBox cboxTelefono;
     @FXML
     CheckBox cboxCity;
     @FXML
     CheckBox cboxAddress;
        
     @FXML
     TextArea taElenco;
    
    @FXML
    private void initialize(){
        
    }
    
    @FXML
    private void aggiungiPersona(){
        String cognome=txtCognome.getText();
        String nome=txtNome.getText();
        String telefono=txtTelefono.getText();
        String email=txtEmail.getText();
        String citta=txtCity.getText();
        String indirizzo=txtAddress.getText();

        mainApp.aggiungiPersona(cognome, nome, telefono, email, citta, indirizzo);
        azzeraVisualizzazioneLista();
        taElenco.setText(rubrica.ottieniListaPersone());

        //taElenco.appendText(cognome+" "+nome+"\n");
        //rubrica.aggiungiPersona(cognome, nome);
        txtCognome.clear();       
        txtNome.clear();
        txtTelefono.clear();
        txtEmail.clear();
        txtCity.clear();
        txtAddress.clear();
    }
    
    @FXML
    private void cercaPersona(){
        String cognome=txtCognome.getText();
        String nome=txtNome.getText();
        String telefono=txtTelefono.getText();
        String email=txtEmail.getText();
        String citta=txtCity.getText();
        String indirizzo=txtAddress.getText();
        
        ArrayList<Persona> elencoPersone = rubrica.getElencoPersone();
        ArrayList<Persona> elencoPersoneCercate = new ArrayList();
        
        for (Persona a: elencoPersone){
            if (cboxCognome.isSelected()){
                //search cognome
                if (a.getCognome().toLowerCase().contains(cognome.toLowerCase()) ){
                    elencoPersoneCercate.add(a);
                }
            } else if (cboxNome.isSelected()){
                //search nome
                if (a.getNome().toLowerCase().contains(nome.toLowerCase()) ){
                    elencoPersoneCercate.add(a);
                }

            } else if (cboxTelefono.isSelected()){
                //search telefono
                if (a.getTelefono().toLowerCase().contains(telefono.toLowerCase()) ){
                    elencoPersoneCercate.add(a);
                }

            } else if (cboxEmail.isSelected()){
                //search email
                if (a.getEmail().toLowerCase().contains(email.toLowerCase()) ){
                    elencoPersoneCercate.add(a);
                }

            } else if (cboxCity.isSelected()){
                //search citta
                if (a.getCitta().toLowerCase().contains(citta.toLowerCase()) ){
                    elencoPersoneCercate.add(a);
                }

            } else if (cboxAddress.isSelected()){
                //search indirizzo
                if (a.getIndirizzo().toLowerCase().contains(indirizzo.toLowerCase()) ){
                    elencoPersoneCercate.add(a);
                }

            }
        }
        
        String lista="";
        for (int i=0;i<elencoPersoneCercate.size();i++){
            lista+=elencoPersoneCercate.get(i)+"\n";
            
        }
        
        azzeraVisualizzazioneLista();
        taElenco.setText(lista);

        
    }
    
    @FXML
    private void esportaFile(){
                    String filepath = "export.jobj";

        try
      {
         FileOutputStream fileOut = new FileOutputStream(filepath);
         ObjectOutputStream out = new ObjectOutputStream(fileOut);
         out.writeObject(rubrica.getElencoPersone());
         out.close();
         fileOut.close();
      }
      catch(IOException i)
      {
          i.printStackTrace();
      }
    }
    
    @FXML
    private void importaFile() throws ClassNotFoundException{
                               String filepath = "export.jobj";

              try
      {
        // Reads the object
            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream objIn = new ObjectInputStream(fileIn);

            // Reads the objects
            ArrayList<Persona> elenco = (ArrayList<Persona>) objIn.readObject();

            objIn.close();

            rubrica.setElencoPersone(elenco);
            azzeraVisualizzazioneLista();
            taElenco.setText(rubrica.ottieniListaPersone());
      
      }
      catch(IOException i)
      {
          i.printStackTrace();
      }
    }
    
    
    /**
     * 
     * @param mainApp 
     */
    void setMainApp(MainApp mainApp) {
        //To change body of generated methods, choose Tools | Templates.
        this.mainApp=mainApp;
        rubrica=mainApp.getRubrica();
    }
    
    @FXML
    private void azzeraVisualizzazioneLista(){
        taElenco.clear();
    }
    
    @FXML
    /**
     * 
     */
    private void visualizzaLista(){
        azzeraVisualizzazioneLista();        
        taElenco.setText(rubrica.ottieniListaPersone());
        
    }
    
}
