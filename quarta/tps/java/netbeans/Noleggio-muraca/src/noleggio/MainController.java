package noleggio;


import java.io.BufferedReader;
import java.io.FileReader;
import model.*;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.lang.String;
import java.util.Arrays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class MainController
{
    private MainApp mainApp;
    private Negozio negozio;
    
    @FXML
     TextField txtMarca;
    
    @FXML
     TextField txtModello;
    
    @FXML
     TextField txtTarga;
    
    @FXML
     TextField txtCosto;
    
    @FXML
    TextArea taCosto;
    
    @FXML
     TableView<Auto> taElenco;
    
    @FXML
     TableColumn<Auto, String> colonnaMarca;
    
    @FXML
     TableColumn<Auto, String> colonnaModello;
    
    @FXML
     TableColumn<Auto, String> colonnaTarga;
    
    @FXML
     TableColumn<Auto, String> colonnaCosto;
    
    @FXML
     TableColumn<Auto, String> colonnaData_noleggio;
    
    
   
 
    void setMainApp(MainApp mainApp) {
        //To change body of generated methods, choose Tools | Templates.
        this.mainApp=mainApp;
        this.negozio = mainApp.getNegozio();
    }
    
    public void initialize()
    {
        colonnaMarca.setCellValueFactory(new PropertyValueFactory<>("marca"));
        colonnaModello.setCellValueFactory(new PropertyValueFactory<>("modello"));
        colonnaTarga.setCellValueFactory(new PropertyValueFactory<>("targa"));
        colonnaCosto.setCellValueFactory(new PropertyValueFactory<>("costo"));
        colonnaData_noleggio.setCellValueFactory(new PropertyValueFactory<>("data_oleggio"));
        leggiFile();
    }
    
    private void leggiFile(){
        String f="C:\\Users\\asus\\OneDrive\\Documenti\\NetBeansProjects\\Noleggio\\src\\Noleggio.csv";
        
        String delimiter = ";";
        String line= null;
        try (BufferedReader br = new BufferedReader(new FileReader(f)))
        {
            
            while((line=br.readLine())!= null)
                creaOggetto(line.split(";"));  
        }
        catch (Exception e)
        {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }
    
    private void creaOggetto(String[] l){
        String marca=l[0];
        String modello=l[1];
        String targa=l[2];
        String costo=l[3];
        String data_noleggio=l[4];
        
       
        Auto a=new Auto(marca,modello,targa,costo,data_noleggio);
        Negozio.elencoDisponibili.add(a);
        visualizzaDisponibili();
        
    }
    @FXML
    private void visualizzaDisponibili()
    {
        
        ObservableList<Auto> x = FXCollections.observableArrayList(Negozio.getElencoDisponibili());
        taElenco.setItems(x);
    }
    
    @FXML
    private void visualizzaNoleggiate()
    {
        azzeraVisualizzazioneLista();        
        ObservableList<Auto> elenco_noleggiate = FXCollections.observableArrayList(Negozio.getElencoNoleggiate());
        taElenco.setItems(elenco_noleggiate);    
    }
    
    @FXML
    private void azzeraVisualizzazioneLista(){
       ObservableList<Auto> v = FXCollections.observableArrayList(); 
       taElenco.setItems(v);
    }
    
    @FXML
    private void noleggia(){
        String marca=txtMarca.getText();
        String modello=txtModello.getText();
        String targa=txtTarga.getText();
        
        for(int i=0;i<this.negozio.SizeDisponibili();i++)
        {
            if(this.negozio.gDisponibili(i).equals(targa))
            {
                Auto a=Negozio.elencoDisponibili.get(i);
                this.negozio.AggiungiN(a);
                this.negozio.eliminaD(i);
                
                visualizzaNoleggiate();
                
                txtMarca.clear();
                txtModello.clear();
                txtTarga.clear();
            }          
        }
    }
    
    @FXML
    private void restituisci(){
        String marca=txtMarca.getText();
        String modello=txtModello.getText();
        String targa=txtTarga.getText();
        for(int i=0;i<this.negozio.SizeNoleggiate();i++){
            if(this.negozio.gNoleggiate(i).equals(targa)){
                Auto n=Negozio.elencoNoleggiate.get(i);
                this.negozio.AggiungiD(n);
                this.negozio.eliminaN(i);
        
                visualizzaDisponibili();
                txtMarca.clear();
                txtModello.clear();
                txtTarga.clear();
                
            }
        }
    }
    
    
}
