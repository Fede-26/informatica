package noleggio;

import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Negozio;


public class MainApp extends Application{
    private Stage primaryStage;
    private Negozio negozio;
    private MainController mainController;

    public Negozio getNegozio() {
        return negozio;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Esempio struttura base");

        inizializza();
        
    }
    
     public static void main(String[] args) {
        launch(args);
    }

    private void inizializza() {
        
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("mainGui.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            
            negozio=new Negozio();
            mainController=loader.getController();
            mainController.setMainApp(this);
            
            
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    
}