package model;

public class Auto 
{
    private String marca;
    private String modello;
    private String targa;
    private String costo;
    private String data_noleggio;

    public Auto(String marca, String modello, String targa, String costo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
    
    public String getModello() {
        return modello;
    }
    
    public void setModello(String modello) {
        this.modello = modello;
    }

    public String getTarga() {
        return targa;
    }
    
    public void setTarga(String targa) {
        this.targa = targa;
    }

    public String getCosto() {
        return costo;
    }
    
    public void setCosto(String costo) {
        this.costo = costo;
    }


    public Auto(String marca, String modello, String targa, String costo, String data_noleggio) {
        this.marca = marca;
        this.modello = modello;
        this.targa = targa;
        this.costo = costo;
        this.data_noleggio= data_noleggio;
    }
    
    public Auto(){
    
    }
    
    
    
    
}
