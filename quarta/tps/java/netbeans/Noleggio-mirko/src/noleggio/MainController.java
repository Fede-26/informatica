package noleggio;


import java.io.BufferedReader;
import java.io.FileReader;
import model.*;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

public class MainController
{
    private MainApp mainApp;
    private Negozio negozio;
    
    @FXML
     TextField txtMarca;
    
    @FXML
     TextField txtModello;
    
    @FXML
     TextField txtTarga;
    
    @FXML
     TextField txtCosto;
    
    @FXML
    TextArea taCosto;
    
    @FXML
     TableView<Auto> tabella;
    
    @FXML
     TableColumn<Auto, String> colonnaMarca;
    
    @FXML
     TableColumn<Auto, String> colonnaModello;
    
    @FXML
     TableColumn<Auto, String> colonnaTarga;
    
    @FXML
     TableColumn<Auto, String> colonnaCosto;
    
    @FXML
     TableColumn<Auto, String> colonnaData_noleggio;
    
    
   
 
    void setMainApp(MainApp mainApp) {
        //To change body of generated methods, choose Tools | Templates.
        this.mainApp = mainApp;
        this.negozio = mainApp.getNegozio();
    }
    
    public void initialize()
    {
        colonnaMarca.setCellValueFactory(new PropertyValueFactory<>("marca"));
        colonnaModello.setCellValueFactory(new PropertyValueFactory<>("modello"));
        colonnaTarga.setCellValueFactory(new PropertyValueFactory<>("targa"));
        colonnaCosto.setCellValueFactory(new PropertyValueFactory<>("costo"));
        colonnaData_noleggio.setCellValueFactory(new PropertyValueFactory<>("data_noleggio"));
        leggiFile();
    }
    
    private void leggiFile(){
        String path="C:\\Users\\mirko\\OneDrive\\Desktop\\Scuola\\4AI\\Tps4\\Lab\\Noleggio\\src\\Noleggio.csv";
        
        String delimiter = ";";
        String line = null;
        try (BufferedReader br = new BufferedReader(new FileReader(path)))
        {
            
            while((line=br.readLine())!= null)
                creaOggetto(line.split(";"));  
        }
        catch (Exception e)
        {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }
    
    private void creaOggetto(String[] list){
        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
        String marca = list[0];
        String modello = list[1];
        String targa = list[2];
        String costo = list[3];
        String data_noleggio=df.format(new Date());
        Auto a = new Auto(marca, modello, targa, costo, data_noleggio);
        Negozio.elencoDisponibili.add(a);
        //visualizzaDisponibili();
        
    }
    @FXML
    private void visualizzaDisponibili()
    {
        ObservableList<Auto> x = FXCollections.observableArrayList(Negozio.getElencoDisponibili());
        tabella.setItems(x);
    }
    
    @FXML
    private void visualizzaNoleggiate()
    {
        //azzeraVisualizzazioneLista();        
        ObservableList<Auto> elenco_noleggiate = FXCollections.observableArrayList(Negozio.getElencoNoleggiate());
        tabella.setItems(elenco_noleggiate);    
    }
    
    @FXML
    private void azzeraVisualizzazioneLista(){
       ObservableList<Auto> v = FXCollections.observableArrayList();    //set void list
       tabella.setItems(v);
    }
    
     @FXML
    private Auto selectByClick(){  //MouseEvent event
        Auto auto = tabella.getSelectionModel().getSelectedItem();
        return auto;
    }
    
    @FXML
    private void noleggia(){
        Negozio.elencoNoleggiate.add(selectByClick());
        Negozio.elencoDisponibili.remove(selectByClick());
    }
    
/*    @FXML
    private void restituisci(){
        String marca=txtMarca.getText();
        String modello=txtModello.getText();
        String targa=txtTarga.getText();
        for(int i=0;i<this.negozio.SizeNoleggiate();i++){
            if(this.negozio.gNoleggiate(i).equals(targa)){
                Auto n=Negozio.elencoNoleggiate.get(i);
                this.negozio.AggiungiD(n);
                this.negozio.eliminaN(i);
        
                visualizzaDisponibili();
                txtMarca.clear();
                txtModello.clear();
                txtTarga.clear();
                
            }
        }
    }*/
    
    
}
