#!/usr/bin/env python3
import os
import random as r


a = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]

b = []

#m = int(input("Lunghezza: "))
#n = int(input("Altezza: "))
m = 3
n = 3

for i in range(n):

    t = []
    for j in range(m):
        s = "Elemento " + str(i) + "," + str(j) + " "
        t.append(r.randint(1, 100))

    b.append(t)


def touch(file):
    os.system("touch " + file)

fn = "dest1"
try:
    with open(fn, 'w') as f:
        f.write(str(a)+"\n")
except:
    os.system("touch " + fn)
    with open(fn, 'w') as f:
        f.write(str(a)+"\n")

fs = "source"
touch(fs)
with open(fs, 'w') as f:
    f.write(str(r.randint(1, 100))+"\n")

with open(fs, 'r') as f:
    b[0][0]=int(f.read())

    print(f.read())

print(a)
print(b)
