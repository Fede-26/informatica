#include <stdio.h>

void cubo(int lato, int* vol)
{
	*vol = lato * lato * lato;
}

int cubo2(int lato)
{
	int vol;
	vol = lato * lato * lato;
	return vol;
}

void area(int lato, int* super)
{
	*super = lato * lato * 6;
}

int area2(int lato)
{
	int super;
	super = lato * lato * 6;
	return super;
}


int main (int argc, char ** argv)
{
	int l, v, a;
    printf("Inserire lato del cubo: ");
    scanf("%d", &l);
    cubo(l, &v);
    area(l, &a);
    printf("area --> %d\nvolume --> %d\n", a, v);
    printf("area --> %d\nvolume --> %d\n", area2(l), cubo2(l));
    return 0;
}
