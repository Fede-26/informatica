//esercizio numero 5 a pagina 373

#include <stdio.h>
#include <string.h>

void riempi(int vet[], int num, int max)
{
  int conta;
  srand((unsigned)time(NULL));      // inizializza il seme
// estrai i valori e riempi il vettore
  for (conta=0; conta<num; conta ++)
    vet[conta]=(rand()%max+1);     // valori casuali tra 1 e max
}

void visualizza(int vet[], int num)
{
  int conta;
  printf("\n");
  for (conta=0; conta<num; conta ++)
    printf ("%4d",vet[conta]);      // visualizza vettore
}


int main() {

    int len = 5;
    int array1[len];
    int array2[len];
    riempi(array1, len, 100);
    visualizza(array1, len);

    int quantiMax = 0;

    int i, j;

    for (i=0;i<len;i++)
        array2[i] = 0;

    for (i=0;i<len;i++)
    {
        quantiMax = 0;
        for (j=0;j<len;j++)
        {
            if (array1[j]<array1[i])
            {
                quantiMax++;
            }
        }
        array2[quantiMax] = array1[i];
        visualizza(array1, len);
        visualizza(array2, len);
    }

    return 0;
}
