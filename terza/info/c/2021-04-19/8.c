//es n 8 pag 330
//una parola in una stringa con un'altra

#include <stdio.h>
#include <string.h>

int main() {
    char s1[80] = "pia no piano forte ppiano";
    char find[] = "piano";
    char replace[] = "forte";
    char s2[80];

    printf(">> %s\n", s1);

    char *token = strtok(s1, " ");  //divide la stringa in token

    while(token != NULL)            //finche' c'e' qualcosa in token
    {
        if(strcmp(find, token)==0)  //se token e' uguale a find
            strcat(s2, replace);    //aggiunge replace al posto di token
        else                        //altrimenti
            strcat(s2, token);      //aggiunge token

        token = strtok(NULL, " ");  //divide il prossimo token

        if (token!=NULL)            //aggiunge uno spazio tra le parole
            strcat(s2, " ");
    }

    printf(">> %s\n", s2);

    return 0;
}
