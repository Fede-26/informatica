#include <stdio.h>

int main(int argc, char ** argv)
{

    int n, n2=0, n3=0;  //numero elementi totali di ogni array
    int x;  //elemento
    int dis;
    int scelta;
    int i;

    do {
        printf("quanti numeri: ");
        scanf("%d", &n);
    } while(!(n>0));

    int vett1[n];
    int vett2[n];
    int vett3[n];

    for(i=0; i<n; i++)	//riempie l'array 1
    {
        printf("n: ");
        scanf("%d", &x);
        vett1[i] = x;
    }

    printf("inserire numero discriminante: ");
    scanf("%d", &dis);

    for(i=0; i<n; i++)	//riempie gli altri array
    {
        if(vett1[i]<dis)
        {
            vett2[n2] = vett1[i];
            n2++;
        }

        else if(vett1[i]>dis)
        {
            vett3[n3] = vett1[i];
            n3++;
        }
    }

    printf("1) numeri minori\n2) numeri maggiori\n>> ");
    scanf("%d", &scelta);

    switch (scelta) {
        case 1:
            for(i=0; i<n2; i++)	//riempie l'array 1
                printf("%d ", vett2[i]);
            break;

        case 2:
            for(i=0; i<n3; i++)	//riempie l'array 1
                printf("%d ", vett3[i]);
            break;
    }
    printf("\n");
    return 0;
}
