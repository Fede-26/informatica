#include <stdio.h>

int main()
{
    int voti[10] = {0};
    int voto = 0;
    while (voto!=-1)
    {
        printf("inserire voto da 1 a 10 (-1 per uscire)");
        scanf("%d", &voto);
        if (voto<=10 && voto>=1)
        {
            voti[voto-1]++;
        }
    }

    printf("voti inseriti: \n");
    for (int i = 0; i < 10; i++) {
        printf("voti per %d: %d\n", i+1, voti[i]);
    }
    return 0;
}


/*il vettore è grande 100, mentre il ciclo for itera per 500 volte (da 0 a 499), quindi si assegnerà il valore 0 per zone di memoria non appartenenti al vettore.*/
/*printf("%d ", a[i]);

for (i=0; i<10; i++)
{
    b[i] = a[i];
}*/
