#include <stdio.h>

int main (int argc, char ** argv)
{
    int int1 = 10; //intervallo inferiore
    int int2 = 50; //intervallo superiore
    int num; //numero dell'utente
    scanf("%d", &num);

    if (num<int1)
        printf("-1");
    else if (num>int2)
        printf("1");
    else
        printf("0");

    return 0;
}
