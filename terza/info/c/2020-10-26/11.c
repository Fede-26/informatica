#include <stdio.h>
#include <math.h>

int main (int argc, char ** argv)
{
    float x1; //coordinate punto 1
    float y1; //coordinate punto 1
    float x2; //coordinate punto 2
    float y2; //coordinate punto 2
    printf("\nInserire x del punto 1: ");
    scanf("%f", &x1);
    printf("\nInserire y del punto 1: ");
    scanf("%f", &y1);
    printf("\nInserire x del punto 2: ");
    scanf("%f", &x2);
    printf("\nInserire y del punto 2: ");
    scanf("%f", &y2);


    printf("\ndistanza: %f\n", sqrt(pow(x2 - x1, 2)+pow(y2-y1, 2)));

    return 0;
}
