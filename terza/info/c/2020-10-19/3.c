#include <stdio.h>

int main (int argc, char ** argv)
{
    int scelta;
    printf("1 - differenza di ore\n2 - conversione ore");
    scanf("%d", &scelta);
    int hh1, mm1;
    int hh2, mm2;
    int sec1, sec2;
    int hhtot, mmtot, sectot;
    int secondi;
    int minuti, ore;
    switch(scelta)
    {
        case 1:

        
            printf("\nInserire hh1 < ");
            scanf("%d", &hh1);
        
            printf("\nInserire mm1 < ");
            scanf("%d", &mm1);
            
            printf("\nInserire hh2 < ");
            scanf("%d", &hh2);
        
            printf("\nInserire mm2 < ");
            scanf("%d", &mm2);
        
            sec1 = (hh1*3600) + (mm1*60);
            sec2 = (hh2*3600) + (mm2*60);
            sectot = sec2 - sec1;
            if (sectot < 0)
                sectot = 86400 + sectot;
        

            hhtot = sectot / 3600;
            sectot = sectot % 3600;

            mmtot = sectot / 60;
            sectot = sectot % 60;
        
        
            printf("\ndifferenza %d,%d\n", hhtot, mmtot);
            break;

        case 2:

            printf("\nInserire secondi < ");
            scanf("%d", &secondi);
        
            
            ore = secondi / 3600;
            secondi = secondi % 3600;
        
            minuti = secondi / 60;
            secondi = secondi % 60;
        
        
            if (ore>0)
                printf("ore > %d\n", ore);
            if (minuti>0)
                printf("minuti > %d\n", minuti);
            if (secondi>0)
                printf("secondi > %d\n", secondi);
            break;
        }
        
    return 0;
}
