#include <stdio.h>

int main (int argc, char ** argv)
{
    int secondi;
    int minuti, ore;
    printf("\nInserire secondi < ");
    scanf("%d", &secondi);

    
    ore = secondi / 3600;
    secondi = secondi % 3600;

    minuti = secondi / 60;
    secondi = secondi % 60;


    if (ore>0)
        printf("ore > %d\n", ore);
    if (minuti>0)
        printf("minuti > %d\n", minuti);
    if (secondi>0)
        printf("secondi > %d\n", secondi);

    return 0;
}
