#include <stdio.h>
#include <time.h>

int stampa(int vett[], int l)
{
    int i, contaDispari=0;
    float sommaPari=0, sommaDispari=0;
    for(i=0; i<l; i++) //somma i numeri
    {
        if (i%2==0)
            sommaPari+=vett[i];

        if (vett[i]%2==1)
        {
            sommaDispari+=vett[i];
            contaDispari++;
        }
    }


    printf("somma posto pari --> %f\n", sommaPari);
    printf("media posto dispari --> %f\n", sommaDispari/contaDispari);

    return 0;
}


int main(int argc, char ** argv)
{

    int len;

    int i;

    srand(time(NULL));

    do {
        printf("inserire quanto: ");
        scanf("%d", &len);
    } while(!(len>0));

    int v[len];

    for(i=0; i<len; i++)	//riempie l'array con numeri casuali
    {
            v[i] = rand()%10;
    }

    for(i=0; i<len; i++)	//visualizza l'array
    {
        printf("%d\t", v[i]);
        printf("\n");
    }

    stampa(v, len);

    return 0;
}
