#include <stdio.h>

int main (int argc, char ** argv)
{
	int cifrabin, cifradec, leng;
	int ndec = 0;
	int cont = 0;
	int contpotenza = 0;
	
	printf("inserire quante cifre: ");
	scanf("%d", &leng);
	while (cont!=leng)
	{
		printf("\ninserira cifra 2^%d: ", cont);
		scanf("%d", &cifrabin);

		cifradec = 1;
		contpotenza = 1;
		while (contpotenza <= cont)
		{
			cifradec *= 2;
			contpotenza++;
		}
		
		ndec += cifrabin * cifradec;
		cont++;
	}

	printf("\nnumero decimale: %d", ndec);
    return 0;
}
