//scrivi un programma che leggendo 2 numeri ne esegue la divisione tramite sottrazioni successive stampando ogni iterazione
#include <stdio.h>

int main (int argc, char ** argv)
{
	int dividendo;
	int divisore;
	int risultato = 1;

	printf("inserire un dividendo: ");
	scanf("%d", &dividendo);
	printf("inserire un divisore: ");
	scanf("%d", &divisore);

    while (dividendo > divisore)
	{
		dividendo -= divisore;
		if (dividendo > divisore)
			risultato++;
		printf("%d %d\n", dividendo, risultato);
	}

    printf("risultato: %d\n", risultato);

	return 0;
}
