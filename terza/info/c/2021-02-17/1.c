//es 4 pag 268
#include <stdio.h>
#include <time.h>
#define LEN 10

//srand((unsigned)time(NULL));

int main(int argc, char ** argv)
{
	srand(time(NULL));
	int array1[LEN];
    int array2[LEN];
	int i;

	for(i=0; i<LEN; i++)	//riempie l'array con numeri casuali
		array1[i] = rand()%10;


	for(i=0; i<LEN; i++)							//stampa l'array
		printf("i=%d --> %d\n", i, array1[i]);
    printf("\n");

    for(i=0; i<LEN; i++)	//scambia l'ordine
		array2[i] = array1[LEN-(i+1)];

	for(i=0; i<LEN; i++)							//stampa l'array modificato
		printf("i=%d --> %d\n", i, array2[i]);

    return 0;
}
