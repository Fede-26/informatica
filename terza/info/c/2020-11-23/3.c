#include <stdio.h>

int main (int argc, char ** argv)
{
    int dividendo;
    int divisore;
    int resto;
    int quoziente = 0;
    
	printf("\ndividendo: ");
	scanf("%d", &dividendo);
	printf("\ndivisore: ");
	scanf("%d", &divisore);

    while (dividendo>=divisore && divisore>0)
    {
		dividendo -= divisore;
		quoziente++;
	}
	resto = dividendo;
	printf("\nquoziente: %d\n", quoziente);
	printf("\nresto: %d\n", resto);

    return 0;
}
