#include <stdio.h>

int main (int argc, char ** argv)
{

	float num;		//numero inserito
	float somma = 0;	//somma dei numeri
	int cont = 0;	//contatore
	float media;	//media dei numeri
	float risultato = 45;	//numero da raggiungere
	
	do
	{
		printf("mancano %f a %f\n", risultato-somma, risultato);
		printf("inserire un numero: ");
		scanf("%f", &num);
		somma += num;
		cont++;
	} while (somma != risultato);

	media = somma/cont;
	printf("\nrisultato %f raggiunto\nmedia = %f\n", risultato, media);
	
	return 0;
}
