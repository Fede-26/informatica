//es 4 pag 268
#include <stdio.h>
#include <time.h>
#define LEN 10

//srand((unsigned)time(NULL));


void swapMinMax(int *ar, int len)
{
	int minValue = 0, minIndex = 0;
	int maxValue = 0, maxIndex = 0;
	int i;
	
	for(i=0; i<len; i++)		//trovare valori minori e maggiori
	{
		if(ar[i] < minValue || i == 0)
		{
			minValue = ar[i];
			minIndex = i;
		}

		if(ar[i] > maxValue || i == 0)
		{
			maxValue = ar[i];
			maxIndex = i;
		}
	}

	ar[minIndex] = maxValue;
	ar[maxIndex] = minValue;
	
	return;
}


int main(int argc, char ** argv)
{
	srand(time(NULL));
	int array[LEN];
	int i;

	for(i=0; i<LEN; i++)	//riempie l'array con numeri casuali
		array[i] = rand();

	
	for(i=0; i<LEN; i++)							//stampa l'array
		printf("i=%d --> %d\n", i, array[i]);	

	swapMinMax(array, LEN);
	
	for(i=0; i<LEN; i++)							//stampa l'array modificato
		printf("i=%d --> %d\n", i, array[i]);
		
    return 0;
}

