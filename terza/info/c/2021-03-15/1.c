//Federico Zotti esercizio n.1 verifica 20210315
#include <stdio.h>

void caricaVettorePari(int v[], int len)	//funzione per caricare il vettore con numeri pari
{
	int num, i;
	for (i=0; i<len; i++)
	{
		do {
			printf("inserire numero pari per il posto con indice %d: ", i);
			scanf("%d", &num);
		} while(!(num%2==0));
		v[i] = num;
	}
}

void stampaVettori(int v[], int len)	//stampa il vettore
{
	printf("\n");
	int i;
	for (i=0; i<len; i++)
	{
		printf("v[%d] --> %d\n", i, v[i]);
	}
}

int maggioreInVettore(int v[], int len)	//trova e ritorna la posizione dell'elemento maggiore
{
	int i, massimo, posMassimo;
	for (i=0; i<len; i++)
	{
		if (i==0 || v[i] > massimo)
		{
			massimo = v[i];
			posMassimo = i;
		}
	}

	return posMassimo;
}

int main(int argc, char ** argv)
{
	int len;	//lunghezza vett1

	do {											//crea vett1 e vett2
		printf("inserire lunghezza vettore: ");
		scanf("%d", &len);
	} while(!(len>0));
	int vett1[len];
	int vett2[len];

	caricaVettorePari(vett1, len);					//carica vett1

	int posizioneMax = maggioreInVettore(vett1, len);

	int i;											//riempie vett2
	for (i=0; i<len; i++)
	{
		if (i == posizioneMax)
			vett2[i] = vett1[i];
		else
			vett2[i] = 0;
	}

	stampaVettori(vett1, len);						//stampa entrambi i vettori
	stampaVettori(vett2, len);

    return 0;
}
