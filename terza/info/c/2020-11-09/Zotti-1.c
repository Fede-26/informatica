#include <stdio.h>

int main (int argc, char ** argv)
{
	int saldo = 1000;	//soldi sul conto
	printf("\nSaldo: %d euro\n", saldo);
	
	int prelievo;		//soldi da prelevare
	printf("\ninserire quanto si vuole prelevare: ");
	scanf("%d", &prelievo);

	if (prelievo > saldo)
		printf("\nERRORE: saldo insufficente\n");

	else if (prelievo < 0)
		printf("\nERRORE: il prelievo non puo essere negativo\n");

	else if (prelievo < saldo)
	{
		saldo = saldo - prelievo;
		printf("\nSono state prelevati %d euro\n", prelievo);
		printf("\nSul conto sono ora presenti %d euro\n", saldo);
	}

	else if (prelievo == saldo)
	{
		printf("\nSono stati prelevati tutti i soldi del conto: %d euro\n", prelievo);
		saldo = 0;
	}

	
    return 0;
}
