//es 4 pag 268
#include <stdio.h>



int main(int argc, char ** argv)
{
	int n;
    int scelta;

	do
	{
		printf("inserire numero positivo: \n");
		scanf("%d", &n);
	}while(n<0);

	printf("cerchio 1; quadrato 2\n");
	scanf("%d", &scelta);

	switch (scelta) {
		case 1:
			printf("raggio: %d\narea: %d", n, n*n*3.14);
		case 2:
			printf("lato: %d\narea: %d", n, n*n);
	}

    return 0;
}
