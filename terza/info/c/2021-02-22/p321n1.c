#include <stdio.h>
#include <time.h>


int main(int argc, char ** argv)
{

    int n, nMax=10;
    int m, mMax=20;
    int x, i, j;
    srand(time(NULL));

    do {
        printf("inserire righe: ");
        scanf("%d", &n);
        printf("\ninserire colonne: ");
        scanf("%d", &m);
    } while(!(n<=nMax && n>0 && m<=mMax && m>0));

    int matrice1[m][n];
    int matrice2[m][n];

    for(i=0; i<m; i++)	//riempie l'array con numeri casuali
    {
        for (j=0; j<n; j++)
        {
            x = rand()%10;
            matrice1[i][j] = x;
            matrice2[i][j] = x;
            //matrice1[i][j] = i+j;
            //matrice2[i][j] = i+j;
        }
    }

    for(i=0; i<m; i++)	//visualizza l'array
    {
        for (j=0; j<n; j++)
        {
            printf("%d\t", matrice1[i][j]);
        }
        printf("\n");
    }


    for(i=1; i<m; i+=2) //inverte l'array
    {
        for(j=0; j<n; j++)
        {
            matrice2[i-1][j] = matrice1[i][j];
            matrice2[i][j] = matrice1[i-1][j];
        }
    }


    for(i=0; i<m; i++)	//visualizza l'array
    {
        for (j=0; j<n; j++)
            printf("%d\t", matrice2[i][j]);
        printf("\n");
    }
    return 0;
}
