#include <stdio.h>

int main(int argc, char ** argv)
{
	int len, num, test;
	int isEqual = 1;
	int massimo = 30;

	do {
		printf("inserire lunghezza vettore minore di %d: ", massimo);
		scanf("%d", &len);
	} while(!(len>0 && len<massimo));
	int array1[len];

	int i;
	for(i=0; i<len; i++)	//riempie l'array con numeri casuali
	{
		printf("inserire num: ");
		scanf("%d", &num);
		array1[i] = num;
	}


	for(i=0; i<len; i++)							//stampa l'array
		printf("i=%d --> %d\n", i, array1[i]);
    printf("\n");

	test = array1[0];
    for(i=0; i<len; i++)
	{
		if(array1[i]!=test)
			isEqual = 0;
	}

	printf("isEqual: %d", isEqual);

    return 0;
}
