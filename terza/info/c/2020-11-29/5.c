#include <stdio.h>

int main (int argc, char ** argv)
{
    int pos = 0, neg = 0, zero = 0;
    int num;

    do
    {
        printf("\ninserire num: ");
        scanf("%d", &num);
        if (num>0)
    	    pos++;
        else if (num<0)
            neg++;
        else
            zero++;
    }
    while (num>=0);

    printf("\npos: %d, neg: %d, zero: %d", pos, neg, zero);

    return 0;
}
