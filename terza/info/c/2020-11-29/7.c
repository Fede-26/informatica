#include <stdio.h>

int main (int argc, char ** argv)
{
    int max = 20;
    int num;

    do
    {
        printf("\ninserire num: ");
        scanf("%d", &num);
    }
    while (num>max || num<=0);

	do
	{
		printf("%d\n", num);
		num--;
	}
	while (num>0);

    return 0;
}
