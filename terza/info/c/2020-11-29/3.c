#include <stdio.h>

int main (int argc, char ** argv)
{
    int len, numero;
    int somma = 0;
    int cont = 0;
    
    do
    {
        printf("quanti numeri inserire:");
        scanf("%d", &len);
    }
    while(len<=0);

    do
    {
    	printf("\ninserire numero: ");
    	scanf("%d", &numero);
		somma += numero;
        cont++;
		
    }
    while (cont<len);
    printf("\nsomma numeri inseriti: %d\n", somma);

    return 0;
}
