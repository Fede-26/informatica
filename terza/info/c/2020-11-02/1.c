#include <stdio.h>

int main (int argc, char ** argv)
{
    int lato1; //lati
    int lato2;
    int lato3;
    scanf("%d", &lato1);
    scanf("%d", &lato2);
    scanf("%d", &lato3);

    if (lato1 == lato2 && lato1 == lato3)
        printf("equilatero");
    else if (lato1 == lato2 || lato2 == lato3 || lato1 == lato3)
        printf("isoscele");
    else
        printf("scaleno");

    return 0;
}
