//esercizio di verifica 2021-01-18 Federico Zotti

#include <stdio.h>

int main (int argc, char ** argv)
{

	int totFamiglie = 4;	//totale famiglie
	int contFamiglie = 0;	//contatore famiglie
	int vinceFamiglie;		//famiglia con la media piu alta
	
	int totArticoli;	//totale articoli per famiglia
	float prezzoArticoli;		//prezzo dell'articolo corrente
	float sommaArticoli;		//somma dei prezzi
	int contArticoli;		//contatore dei prezzi
	
	float mediaArticoli;	//media dei prezzi
	float maxMediaArticoli = 0;	//media dei prezzi massima
	



	for (contFamiglie = 1; contFamiglie <= totFamiglie; contFamiglie++)
	{

		sommaArticoli = 0;
		printf("\ninserimento dati famiglia n.%d\n", contFamiglie);

		//inserimento totArticoli con controllo
		do
		{
			printf("Inserire numero articoli per famiglia n.%d: ", contFamiglie);
			scanf("%d", &totArticoli);
		}while (totArticoli<=0);

		//input dei prezzi
		for (contArticoli = 1; contArticoli <= totArticoli; contArticoli++)
		{	
			do
			{
				printf("inserire prezzo articolo n.%d: ", contArticoli);
				scanf("%f", &prezzoArticoli);
			}while (prezzoArticoli <= 0);

			sommaArticoli += prezzoArticoli;
		}

		//calcolo della media
		mediaArticoli = sommaArticoli/totArticoli;
		printf("media per la famiglia n.%d: %f\n", contFamiglie, mediaArticoli);
		
		//registrazione della media massima
		if (mediaArticoli > maxMediaArticoli)
		{
			maxMediaArticoli = mediaArticoli;
			vinceFamiglie = contFamiglie;
		}
	}

	printf("Vince la famiglia n.%d con la media: %f\n", vinceFamiglie, maxMediaArticoli);
	return 0;
}
