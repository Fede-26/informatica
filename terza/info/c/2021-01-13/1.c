/* File: esempio01.c */
/* Time-stamp: "2011-09-06 03:26:06 paolo" */
/* Scopo: funzioni                */
#include <stdio.h>
int max(int m, int n) /* header della funzione con parametri formali*/         
{
 if (m >= n)
  return m;           /* valore di ritorno */
 else
  return n;           /* valore di ritorno */
}
int main(void)
/* programma che visualizza il maggiore tra due numeri */
{
  int massimo,numero1,numero2;
  printf("\ninserisci due numeri interi\n");
  scanf ("%d%d",&numero1,&numero2);
  massimo=max(numero1,numero2);    /* chiamata della funzione: parametri attuali */
  printf("Il maggiore dei due numeri inseriti e' %d ", massimo);
 
  printf("\n\n");
  return 0;
}

