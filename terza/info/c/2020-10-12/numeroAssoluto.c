#include <stdio.h>

int main (int argc, char ** argv)
{
    int a, b;
    printf("Inserire a e b\n");
    scanf("%d%d", &a, &b);

    
    if (b<0)
    {
        printf("b è negativo\n");
        b*=-1;
    }
    else
        printf("b è positivo\n");

    if (a%2 == 0)
        printf("a è pari\n");
    else
        printf("a è dispari\n");

    if (a<0)
        a*=-1;

    printf("Somma massima --> %d\n", a+b);
    return 0;
}
