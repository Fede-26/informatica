#include <stdio.h>

int main (int argc, char ** argv)
{
	int numero;
	int x;
	int n;
	int i;
	int minori = 0;
	int uguali = 0;
	int maggiori = 0;
	
	printf("inserire un numero x: ");
	scanf("%d", &x);
	printf("quanti numeri inserire: ");
	scanf("%d", &n);

	for (i=1; i<=n; i++)
	{
		printf("inserire un numero: ");
		scanf("%d", &numero);

		if (numero < x)
			minori++;
		else if (numero > x)
			maggiori++;
		else
			uguali++;
	}

	
	printf("minori di x: %d\n", minori);
	printf("uguali a x: %d\n", uguali);
	printf("maggiori di x: %d\n", maggiori);

	return 0;
}
