//Federico Zotti esercizio n.2 verifica 20210315
#include <stdio.h>

void caricaVettore(int v[], int len)	//funzione per caricare il vettore
{
	int num, i;
	for (i=0; i<len; i++)
	{
		printf("inserire numero per il posto con indice %d: ", i);
		scanf("%d", &num);
		v[i] = num;
	}
}

void stampaVettori(int v[], int len)	//stampa il vettore
{
	printf("\n");
	int i;
	for (i=0; i<len; i++)
	{
		printf("v[%d] --> %d\n", i, v[i]);
	}
}


int main(int argc, char ** argv)
{
	int len = 5;	//lunghezza vett1

	int vett1[len];	//crea i vettori
	int vett2[len];

	caricaVettore(vett1, len);	//carica i vettori

	int i;
	for (i=0; i<len; i++)
	{
		vett2[i] = vett1[len-i];
	}

	int sommaVett1 = vett1[len] + vett1[len-1];		//trova la somma degli ultimi 2 elementi per ogni vettore
	int sommaVett2 = vett2[len] + vett2[len-1];

	if (sommaVett1>sommaVett2)			//stampa il vettore con la somma maggiore
	{
		printf("vett1");
		stampaVettori(vett1, len);
	}
	else if (sommaVett1<sommaVett2)
	{
		printf("vett2");
		stampaVettori(vett2, len);
	}
	else
	{
		printf("la somma e' uguale");
		stampaVettori(vett1, len);
		printf("\n");
		stampaVettori(vett2, len);
	}

    return 0;
}
