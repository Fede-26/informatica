#include <stdio.h>

int main(int argc, char ** argv){
    float somma = 0;
    float i = 0;
    float n = 1;

    printf("Inserire numeri e terminare con 0:\n");
    while (n!=0){
        scanf("%f", &n);
        somma = somma + n;
        if (n!=0){
            i ++;
        }
    }
    
    printf("Media --> %f", somma/i);

    return 0;
}
