#include <stdio.h>

int main (int argc, char ** argv)
{
    int massimo = 1000;
    int numero;
    int somma = 0;
    int contatore = 0;
    
	do
	{
		printf("\ninserire numero: ");
		scanf("%d", &numero);
		if (numero> massimo-somma)
			printf("numero troppo grande");
		else
		{
			somma += numero;
			contatore++;
		}
		
		printf("\nmanca %d a %d", massimo-somma, massimo);

	}while (somma<massimo);

	printf("\nnumeri inseriti: %d\n", contatore);

    return 0;
}
