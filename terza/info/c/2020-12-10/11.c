#include <stdio.h>

int main (int argc, char ** argv)
{
    char scelta = 'n';
    float voto;
    float somma = 0;
    float contatore = 0;
    
	do
	{
		do
		{
			printf("\ninserire voto: ");
			scanf("%f", &voto);
		}while (voto<1 || voto>10);
		
		somma += voto;
		contatore++;

		do
		{
			printf("\nterminare? s/n: ");
			scanf(" %c", &scelta);
		}while (scelta!='s' && scelta!='n');

	}while (scelta=='n');

	printf("\nvoti inseriti: %f", contatore);
	printf("\nmedia voti: %f\n", somma/contatore);

    return 0;
}
