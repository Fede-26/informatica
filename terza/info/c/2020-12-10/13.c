#include <stdio.h>

int main (int argc, char ** argv)
{
	int num1, num2;
	int prodotto = 0;
	int contatore = 0;
   
	do
	{
		printf("\ninserire primo numero: ");
		scanf("%d", &num1);
		printf("\ninserire secondo numero: ");
		scanf("%d", &num2);
	}while (num1<=0 || num1<=0);

	do
	{
		prodotto += num1;
		contatore++;

	}while (contatore!=num2);

	printf("\n%d moltiplicato %d = %d\n", num1, num2, prodotto);

    return 0;
}
