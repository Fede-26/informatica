#include <stdio.h>

int main (int argc, char ** argv)
{
    float voto;
    float somma = 0;
    float contatore = -1;
    
	do
	{
		do
		{
			printf("\ninserire voto: ");
			scanf("%f", &voto);
		}while (voto<0 || voto>10);
		
		somma += voto;
		contatore++;

	}while (voto!=0);

	printf("\nvoti inseriti: %f", contatore);
	printf("\nmedia voti: %f\n", somma/contatore);

    return 0;
}
