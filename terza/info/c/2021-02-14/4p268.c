//es 4 pag 268
#include <stdio.h>
#define GIORNI 3

void media(int ar[], int len)
{
	float m;
	float somma=0;
	for(int j=0; j<len; j++)
	{
		somma += ar[j];
	}
	m = somma / len;
	printf("la media delle temperature: %f\n", m);
}


void diffMin(int ar1[], int ar2[], int len)
{
	int diff;
	int min;
	int minGiorno;

	for(int j=0; j<len; j++)
	{
		diff = ar1[j] - ar2[j];
		
		if(diff<min || j==0)
		{
			min = diff;
			minGiorno = j;
		}
	}
	printf("il giorno con la differenza minore: %d\n", minGiorno+1);
}


int main(int argc, char ** argv)
{
	int massime[GIORNI];
	int minime[GIORNI];
	int scelta;
	int looping = 1;

	for(int i=0; i<GIORNI; i++)
	{
		printf("inserire temperatura massima del giorno %d\n", i+1);
		scanf("%d", &massime[i]);
		printf("inserire temperatura minima del giorno %d\n", i+1);
		scanf("%d", &minime[i]);
	}
	
	while(looping!=0)
	{

		printf("\n1) media temperatura massime;\n2) giorno con differenza minore;\n3) esci;\n");
		scanf("%d", &scelta);

		switch (scelta) {
		case 1:
			media(massime, GIORNI);
			break;
		
		case 2:
			diffMin(massime, minime, GIORNI);
			break;
		
		case 3:
			looping = 0;
			break;
		}
	
	}
    return 0;
}

