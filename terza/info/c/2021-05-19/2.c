#include <stdio.h>
#include <string.h>
#define MAX 32

typedef struct {
    char cognome[30];
    char nome[20];
    int voto;
} alunno;

int main() {
      int n,i,j,trovato;
      char cognome_alunno[20];

      do{
          printf("Inserisci il numero di alunni:");
          scanf("%d", &n);
      } while (n<1 || n>MAX);

      alunno a[n]; /*tabella come array di struttura, si crea un vettore di elementi di tipo alunno*/

      for(i=0; i<n; i++){
          printf("Inserisci il cognome dell'alunno:");
          scanf("%s", a[i].cognome);
          printf("Inserisci il nome dell'alunno:");
          scanf("%s", a[i].nome);
          do{
              printf("Inserisci il voto:");
              scanf("%d", &a[i].voto);
          }while(a[i].voto<1 || a[i].voto>10);
      }

      printf("\nInserisci il cognome dell'alunno da cercare: ");
      scanf("%s", cognome_alunno);

      trovato=0;
      for(j=0;j<n;j++){
          if(strcmp(a[j].cognome, cognome_alunno)==0) {
              printf("\nAlunno trovato in posizione %d\nnome:%s\nvoto:%d\n",j+1,a[j].nome, a[j].voto);
              trovato=1;
          }
      }

      if (!trovato)
          printf("\nAlunno non trovato.\n");

      return 0;
}
