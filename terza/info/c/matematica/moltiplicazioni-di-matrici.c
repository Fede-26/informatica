#include <stdio.h>

/*
CONVENZIONI

m -> numero colonne
n -> numero righe

x -> colonna corrente
y -> righa corrente

i, j, k, ... -> contatori

matrice1[x][y], matrice2[x][y], ... --> matrici

dimMatrice1[0]    -> numero colonne matrice1 (m)
dimMatrice1[1]    -> numero righe matrice1 (n)

scelta -> scelta effettuata nel menu (int positivo)
*/


int menu()
{
    int scelta;
    int nScelte = 8;    //numero di scelte
    do {
        printf(" _______________________________________________________________\n");
        printf("/                                                               \\\n");
        printf("|            Calcolatore per operazioni con matrici             |\n");
        printf("|                                                               |\n");
        printf("|        Menu:                                                  |\n");
        printf("|          - 0: uscita;                                         |\n");
        printf("|          - 1: moltiplicazione righe per colonne               |\n");
        printf("|          - 2: moltiplicazione matrice per numero              |\n");
        printf("|          - 3: addizione 2 matrici                             |\n");
        printf("|          - 4: addizione matrice per numero                    |\n");
        printf("|          - 5: sottrazione 2 matrici                           |\n");
        printf("|          - 6: sottrazioni matrice per numero                  |\n");
        printf("|          - 7: determinante 2x2                                |\n");
        printf("|          - 8: determinante 3x3                                |\n");
        printf("|                                                               |\n");
        printf("\\______________________________________________________________/\n");
        printf("\n>> ");
        scanf("%d", &scelta);
    } while (!(scelta >= 0 && scelta <= nScelte));
    return scelta;
}

int visualizzaMatrice(int dimensioni[], int mat[dimensioni[0]][dimensioni[1]])
{
    int m = dimensioni[0];
    int n = dimensioni[1];
    int x, y;

    for (y=0; y<n; y++)
    {
        for (x=0; x<m; x++) {
            printf("%d\t|\t", mat[x][y]);
        }
        printf("\n");
    }
    return 0;
}

int dimensioniMatrice(int nomeMatrice, int dimensioni[])
{
    int num;
    do {
        printf("inserire quante colonne per la matrice n.%d\n>> ", nomeMatrice);
        scanf("%d", &num);
    }while (!(num > 0));
    dimensioni[0] = num;

    do {
        printf("inserire quante righe per la matrice n.%d\n>> ", nomeMatrice);
        scanf("%d", &num);
    }while (!(num > 0));
    dimensioni[1] = num;

    return 0;
}

int riempiMatrice(int dimensioni[], int mat[dimensioni[0]][dimensioni[1]])
{
    int x, y;
    int m = dimensioni[0];
    int n = dimensioni[1];

    for (y=0; y<n; y++)
    {
        for (x=0; x<m; x++)
        {
            printf("inserire valore (%d, %d)\n>> ", x, y);
            scanf("%d", &mat[x][y]);
        }
    }
    return 0;
}


//operazioni disponibili

int moltiplicazioneRigheColonne()
{
    //inizializzazione prima matrice
    int dimMatrice1[2];
    dimensioniMatrice(1, dimMatrice1);
    int matrice1[dimMatrice1[0]][dimMatrice1[1]];
    riempiMatrice(dimMatrice1, matrice1);

    //inizializzazione seconda matrice
    int dimMatrice2[2];
    dimensioniMatrice(2, dimMatrice2);
    int matrice2[dimMatrice2[0]][dimMatrice2[1]];
    riempiMatrice(dimMatrice2, matrice2);

    //visualizza matrice appena riempite
    visualizzaMatrice(dimMatrice1, matrice1);
    printf("\n");
    visualizzaMatrice(dimMatrice2, matrice2);
    printf("\n");

    if (dimMatrice1[0] != dimMatrice2[1])
    //if(0)
    {
        printf("if0");
        return 1;
    }
    else
    {
        int dimMatriceRisultato[2];
        dimMatriceRisultato[0] = dimMatrice2[0];
        dimMatriceRisultato[1] = dimMatrice1[1];
        int matriceRisultato[dimMatriceRisultato[0]][dimMatriceRisultato[1]];

        int i, j, k;
        for (i=0;i<dimMatrice1[1];i++)
        {
            for (j=0;j<dimMatrice2[0];j++)
            {
                matriceRisultato[i][j]=0;
                for (k=0;k<dimMatrice1[0];k++)
                {
                    matriceRisultato[i][j]+=matrice1[i][k]*matrice2[k][j];
                }
            }
        }

        visualizzaMatrice(dimMatriceRisultato, matriceRisultato);
        return 0;
    }
}

int addizioneMatrici()
{
    printf("Le matrici devono essere di uguali dimensioni.\n");

    //inizializzazione prima matrice
    int dimMatrice[2];
    dimensioniMatrice(1, dimMatrice);
    printf("Prima matrice\n");
    int matrice1[dimMatrice[0]][dimMatrice[1]];
    riempiMatrice(dimMatrice, matrice1);

    //inizializzazione seconda matrice
    printf("Seconda matrice\n");
    int matrice2[dimMatrice[0]][dimMatrice[1]];
    riempiMatrice(dimMatrice, matrice2);

    //visualizza matrice appena riempite
    visualizzaMatrice(dimMatrice, matrice1);
    printf("\n");
    visualizzaMatrice(dimMatrice, matrice2);
    printf("\n");


    int matriceRisultato[dimMatrice[0]][dimMatrice[1]];

    int x, y;
    for (y=0;y<dimMatrice[1];y++)
    {
        for (x=0;x<dimMatrice[0];x++)
        {
            matriceRisultato[x][y] = matrice1[x][y] + matrice2[x][y];
        }
    }

    visualizzaMatrice(dimMatrice, matriceRisultato);
    return 0;
}

int moltiplicazioneNumeroMatrice()
{
    //inizializzazione matrice
    int dimMatrice[2];
    dimensioniMatrice(1, dimMatrice);
    int matrice[dimMatrice[0]][dimMatrice[1]];
    riempiMatrice(dimMatrice, matrice);

    int num;
    printf("Inserisci numero per moltiplicare\n>> ");
    scanf("%d", &num);

    //visualizza matrice appena riempita
    visualizzaMatrice(dimMatrice, matrice);
    printf("\n");


    int matriceRisultato[dimMatrice[0]][dimMatrice[1]];

    int x, y;
    for (y=0;y<dimMatrice[1];y++)
    {
        for (x=0;x<dimMatrice[0];x++)
        {
            matriceRisultato[x][y] = matrice[x][y] * num;
        }
    }

    visualizzaMatrice(dimMatrice, matriceRisultato);
    return 0;
}

int addizioneNumeroMatrice()
{
    //inizializzazione matrice
    int dimMatrice[2];
    dimensioniMatrice(1, dimMatrice);
    int matrice[dimMatrice[0]][dimMatrice[1]];
    riempiMatrice(dimMatrice, matrice);

    int num;
    printf("Inserisci numero per aggiungere\n>> ");
    scanf("%d", &num);

    //visualizza matrice appena riempita
    visualizzaMatrice(dimMatrice, matrice);
    printf("\n");


    int matriceRisultato[dimMatrice[0]][dimMatrice[1]];

    int x, y;
    for (y=0;y<dimMatrice[1];y++)
    {
        for (x=0;x<dimMatrice[0];x++)
        {
            matriceRisultato[x][y] = matrice[x][y] + num;
        }
    }

    visualizzaMatrice(dimMatrice, matriceRisultato);
    return 0;
}

int sottrazioneMatrici()
{
    printf("Le matrici devono essere di uguali dimensioni.\n");

    //inizializzazione prima matrice
    int dimMatrice[2];
    dimensioniMatrice(1, dimMatrice);
    printf("Prima matrice\n");
    int matrice1[dimMatrice[0]][dimMatrice[1]];
    riempiMatrice(dimMatrice, matrice1);

    //inizializzazione seconda matrice
    printf("Seconda matrice\n");
    int matrice2[dimMatrice[0]][dimMatrice[1]];
    riempiMatrice(dimMatrice, matrice2);

    //visualizza matrice appena riempite
    visualizzaMatrice(dimMatrice, matrice1);
    printf("\n");
    visualizzaMatrice(dimMatrice, matrice2);
    printf("\n");


    int matriceRisultato[dimMatrice[0]][dimMatrice[1]];

    int x, y;
    for (y=0;y<dimMatrice[1];y++)
    {
        for (x=0;x<dimMatrice[0];x++)
        {
            matriceRisultato[x][y] = matrice1[x][y] - matrice2[x][y];
        }
    }

    visualizzaMatrice(dimMatrice, matriceRisultato);
    return 0;
}

int determinante2x2()
{
    int d;
    int dimMatrice[2];
    dimMatrice[0] = 2;
    dimMatrice[1] = 2;
    int matrice[dimMatrice[0]][dimMatrice[1]];
    riempiMatrice(dimMatrice, matrice);
    d=(matrice[0][0]*matrice[1][1])-(matrice[0][1]*matrice[1][0]);
    printf("Determinante >> %d\n\n", d);
    return 0;
}

int determinante3x3()
{
    int a1, a2, b1, b2, c1, c2, d;
    int dimMatrice[2];
    dimMatrice[0] = 3;
    dimMatrice[1] = 3;
    int M[dimMatrice[0]][dimMatrice[1]];
    riempiMatrice(dimMatrice, M);
    a1 = M[0][0] * M[1][1] * M[2][2];
    b1 = M[1][0] * M[2][1] * M[0][2];
    c1 = M[2][0] * M[0][1] * M[1][2];
    a2 = M[2][0] * M[1][1] * M[0][2];
    b2 = M[0][0] * M[2][1] * M[1][2];
    c2 = M[1][0] * M[0][1] * M[2][2];
    d  = a1 + b1 + c1 - a2 - b2 - c2;
    printf("Determinante >> %d\n\n", a1);
    printf("Determinante >> %d\n\n", b1);
    printf("Determinante >> %d\n\n", c1);
    printf("Determinante >> %d\n\n", a2);
    printf("Determinante >> %d\n\n", b2);
    printf("Determinante >> %d\n\n", c2);
    printf("Determinante >> %d\n\n", d);
    return 0;
}

int sottrazioneNumeroMatrice()
{
    int dimMatrice[2],num,x,y;
    dimensioniMatrice(1, dimMatrice);
    int matrice[dimMatrice[0]][dimMatrice[1]];
    int matriceRisultato[dimMatrice[0]][dimMatrice[1]];
    riempiMatrice(dimMatrice, matrice);
    printf("inserisci il numero che vuoi sottrarre\n>> ");
    scanf("%d",&num);
    for (x=0;x<dimMatrice[0];x++)
    {
        for (y=0;dimMatrice[1];y++)
        {
            matrice[x][y]-=num;
        }
    }
    visualizzaMatrice(dimMatrice, matriceRisultato);
    return 0;
}


int main()
{
    int scelta;
    int looping = 1;
    while (looping)
    {
        scelta = menu();
        switch (scelta) {

        case 1:
            moltiplicazioneRigheColonne();
            break;
        case 2:
            moltiplicazioneNumeroMatrice();
            break;
        case 3:
            addizioneMatrici();
            break;
        case 4:
            addizioneNumeroMatrice();
            break;
        case 5:
            sottrazioneMatrici();
            break;
        case 6:
            sottrazioneNumeroMatrice();
            break;
        case 7:
            determinante2x2();
            break;
        case 8:
            determinante3x3();
            break;
        case 0:
            printf("\n\nBUONA SERATA, PIU' CHE BUONGIORNO!\n\n");
            looping = 0;
            break;

        }
    }
    return 0;

}
