#!/usr/bin/env python3

#A = [[1, 2, 3],
#     [0, 2, 1]]

#B = [[0, 3, 2],
#     [3, 1, 2],
#     [1, 1, 0]]

A = []
B = []
C = []

colA = int(input("colonne di A: "))
rigA = int(input("righe di A: "))
colB = int(input("colonne di B: "))
rigB = int(input("righe di B: "))

print("prima matrice")
for rig in range(rigA):
    A.append([])
    for col in range(colA):
        n = int(input(str(rig) + str(col) + "--> "))
        A[rig].append(n)


print("seconda matrice")
for rig in range(rigB):
    B.append([])
    for col in range(colB):
        n = int(input(str(rig) + str(col) + "--> "))
        B[rig].append(n)

if len(A[0]) == len(B):
    for i in range(len(A)):
        C.append([])
        for j in range(len(B[0])):
            C[i].append([])
            for k in range(len(A[0])):
                C[i][j].append(A[i][k]*B[k][j])
            C[i][j] = sum(C[i][j])


    for rig in C:
        for col in rig:
            print("| "+str(col), end=" ")
        print("|")

else:
    print("moltiplicazione impossibile")