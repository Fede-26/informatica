#include <stdio.h>

void mah(int* a, int b, int* c)
{
    b = *c + *a;
    *a = *c + b - 2;
    *c = b - *a + 3;
    printf("\n*a --> %d |b --> %d |*c --> %d", *a, b, *c);
}

int main()
{
    int x, y, z;
    x = 1;
    y = 2;
    z = 3;
    printf("\nx  --> %d |y --> %d |z  --> %d", x, y, z);
    mah(&y, x, &z);
    printf("\nx  --> %d |y --> %d |z  --> %d", x, y, z);
    return 0;
}