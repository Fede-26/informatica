#include <stdio.h>
#include <math.h>

void binario(int* b)
{
    int resto, esponente, bc=0;
    for (esponente = 0; *b != 0; esponente++)
    {

        resto = *b % 2;
        printf("resto della divisione tra %d e 2 == %d\n", *b,resto);
        bc = bc + resto*pow(10, esponente);
        *b /= 2;
    }
    *b = bc;
}

void ottale(int* o)
{
    int resto, esponente, oc=0;
    for (esponente = 0; *o != 0; esponente++)
    {

        resto = *o % 8;
        printf("resto della divisione tra %d e 8 == %d\n", *o,resto);
        oc = oc + resto*pow(10, esponente);
        *o /= 8;
    }
    *o = oc;
}

int main()
{
    int n, scelta;
    do
    {
        printf("inserisci un numero: ");
        scanf("%d", &n);
        //while(n<0)
        //{
            //printf("rinserisci un numero (il numero deve essere positivo) ");
            //scanf("%d", &n);
        //}
        printf("inserisci 2 per binario o 8 per ottale: ");
        scanf("%d", &scelta);
        switch (scelta) {
        
        case 2:
            binario(&n);
            break;
        case 8:
            ottale(&n);
            break;

        }
        printf("%d\n", n);
    }
    while(n!=0);
}