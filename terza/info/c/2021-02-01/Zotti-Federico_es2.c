#include <stdio.h>

int main (int argc, char ** argv)
{

	float velocita;	//input velocita del concorrente
	float sommaVelocita = 0;	//somma di tutte le velocita, serve per la media
	float maxVelocita = 0;	//velocita massima
	float mediaVelocita;	//velocita media
	int contVelocita = 0;	//contatore velocita totali
	int contVelocitaConc;	//contatore per le velocita per le prove
	int contVelocitaTotConc = 4;	//prove totali per concorrente 

	int numConcorrenti;	//numero di concorrenti
	char concorrente;	//nome concorrente
	char concorrenteMaxVelocita;	//concorrente con velocita massima

	float recordMaxVelocita = 120;	//record di velocita massima per battere il record
	float recordMediaVelocita = 112;	//record di velocita media per battere il record


	while (1)
	{
		printf("inserire iniziale nome concorrente (* per terminare): ");
		scanf(" %c", &concorrente);
		if (concorrente == '*')	//se viene inserito * il ciclo termina
			break;
		contVelocitaConc = 1;
		while (contVelocitaConc <= contVelocitaTotConc)	//input velocita per concorrente
		{
			printf("\ninserire velocita n%d: ", contVelocitaConc);
			scanf("%f", &velocita);
			sommaVelocita += velocita;

			if (velocita > maxVelocita)	//controlla se e' la velocita massima
			{
				maxVelocita = velocita;
				concorrenteMaxVelocita = concorrente;				
			}

			contVelocita++;		//incrementa i contatori di velocita
			contVelocitaConc++;
		}
	
	}

	mediaVelocita = sommaVelocita / contVelocita;	//calcola la media

	printf("\nmedia velocita: %f", mediaVelocita);
	printf("\nvelocita massima: %f", maxVelocita);
	printf("\nconcorrente velocita massima: %c\n", concorrenteMaxVelocita);
	if (maxVelocita > recordMaxVelocita && mediaVelocita > recordMediaVelocita)
		printf("RECORD\n");
	
	return 0;
}
