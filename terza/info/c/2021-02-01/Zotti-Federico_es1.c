#include <stdio.h>

int bin(int dec_num)
{
	int a = 1;
	int bin_num;
	int rem;

	while(dec_num > 0)
    {
    	rem = dec_num%2;
    	dec_num = dec_num/2;
    	bin_num = bin_num + (a * rem);
    	a = a * 10;
    }
	return bin_num;
}

int oct(int n)
{
	return n;
}

int hex(int n)
{
	return n;
}

int viges(int n)
{
	return n;
}

int roman(int n)
{
	return n;
}

int main (int argc, char ** argv)
{

	int tappo = 0;
	int num;
	int scelta;
	int numConv;

	do{
		printf("inserire numero: ");
		scanf("%d", &num);
		printf("\ninserire la base per la conversione:\n -2 binario\n -8 ottale\n -16 esadecimale\n -20 vigesimale\n -5 romano\n -0 esci");
		scanf("%d", &scelta);
		
		switch (scelta) {
		case 2:
			numConv = bin(num);
			break;
		
		case 8:
			numConv = oct(num);
			break;
		
		case 16:
			numConv = hex(num);
			break;
		
		case 20:
			numConv = viges(num);
			break;

		case 5:
			numConv = roman(num);
			break;

		default:
			break;
		}

		printf("\nnumero convertito: %d\n", numConv);
	}while (num!=tappo);

	return 0;
}
