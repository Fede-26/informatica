#include <stdio.h>
#include <string.h>
#define MAX 30

typedef struct {
	char cognome[30];
	char nome[20];
	int voto;
} alunno;

int main() {
	FILE *puntafile;
	char c[] = "0";
	char endoffile[] = "";
	int x,n,i,j,trovato;
	char cognome_alunno[20];
	char content[2000];
	//int* punt;
	int num;
	
	do{
		printf("Inserisci il numero di alunni:");
		scanf("%d", &n);
	} while (n<1 || n>MAX);
	
	
	
	alunno a[MAX]; //tabella come array di struttura, si crea un vettore di elementi di tipo alunno
	puntafile = fopen("Salvataggio_2.txt", "r");
	while(!feof(puntafile))
	{
    	c[0] = fgetc(puntafile);
    	strcat(content, c);
	}
	strcat(content, endoffile);
	fclose(puntafile);
	
	char *token = strtok(content, "\n");  //divide la stringa in token
	i=0;
    while(token != NULL)            //finche' c'e' qualcosa in token
    {
    	strcpy(a[i].cognome, token);
        token = strtok(NULL, "\n");  //divide il prossimo token
    	strcpy(a[i].nome, token);
        token = strtok(NULL, "\n");  //divide il prossimo token
        num=atoi(token);
		a[i].voto=num;
		token = strtok(NULL, "\n");
    	i=i+1;
	}
	
	puntafile= fopen("Salvataggio_2.txt","a");
	x=i;
	for(; i<n+x; i++){
		printf("Inserisci il cognome dell'alunno:");
		scanf("%s", a[i].cognome);
		printf("Inserisci il nome dell'alunno:");
		scanf("%s", a[i].nome);

		do
		{
			printf("Inserisci il voto:");
			scanf("%d", &a[i].voto);
		}while(a[i].voto<1 || a[i].voto>10);

		fprintf(puntafile,"\n%s\n%s\n%d", a[i].cognome, a[i].nome, a[i].voto);
	}
	fclose(puntafile);		//lettura del file	
	

	printf("\nInserisci il cognome dell'alunno da cercare: ");
	scanf("%s", cognome_alunno);
	
	trovato=0;
	for(j=0;j<x+n;j++){

		printf("\ncognome %s", a[j].cognome);
		printf("\nnome %s", a[j].nome);
		printf("\nvoto %d", a[j].voto);

		if(strcmp(a[j].cognome, cognome_alunno)==0) {
			printf("\nAlunno trovato in posizione %d\nnome:%s\nvoto:%d",j+1,a[j].nome, a[j].voto);
			trovato=1;
		}	
	}
	
	if (!trovato) 
		printf("\nAlunno non trovato.\n");

  return 0;
}

/*Array di record
#include <stdio.h>
#include <string.h>

#define MAX 10

typedef struct {
  char marca[20];  
  int cilindrata;  
  int anno;        
  char cognome[20];
  char nome[20];   
} automobili;

int main(){
 	int i,n,anno,tot=0;
 	automobili autosalone[MAX]; //tabella autosalone di tipo automobili che al pi� contiene 10 elementi
 	char marca[20];
 	
 	do{
 	printf("Quante auto vuoi inserire?:");
	scanf("%d", &n);	 
	} while (n<1 || n>MAX);
  
        for(i=0; i<n; i++){
 	   printf("\nAuto %d", i+1);
	   printf("\nMarca:");
	   scanf("%s", autosalone[i].marca);
	
	   do  {
	     printf("Cilindrata:");
	     scanf("%d", &autosalone[i].cilindrata);
	   } while ((autosalone[i].cilindrata)<800 || (autosalone[i].cilindrata)>2500);
		
	   do  {
	     printf("Anno immatricolazione:");
	     scanf("%d", &autosalone[i].anno);
	   } while ((autosalone[i].anno)<2000 || (autosalone[i].anno)>2019);
		
	    printf("Cognome:");
	    scanf("%s", autosalone[i].cognome);
	    printf("Nome:");
	    scanf("%s", autosalone[i].nome);
        }
 
 	printf("\nQuale marca di auto cerchi?:");
 	scanf("%s", marca);
 	for (i=0; i<n; i++)
 		if(strcmp(autosalone[i].marca,marca)==0) 
	 		printf("%s %s\n ", autosalone[i].cognome, autosalone[i].nome);
 
 	printf("\n\nProprietari con auto di cilindrata maggiore di 1800 cc:");
 	for (i=0; i<n; i++)
		if(autosalone[i].cilindrata>1800)
	 		printf(" %s", autosalone[i].cognome);

	printf("\n\nAnno:");
	scanf("%d", &anno);
 	for (i=0; i<n; i++)
		if(autosalone[i].anno==anno)
			tot++;
		printf("Auto immatricolate: %d", tot);
}*/
