//esercizio 10 pag 247
#include <stdio.h>

int main (int argc, char ** argv)
{

	int totNum;				//totale numeri da inserire
	int maxTotNum = 100;		//massimo per totNum
	int somma = 0;
	int num;
	int i;

	do
	{
		printf("quanti numeri inserire (min di %d): ", maxTotNum);
		scanf("%d", &totNum);
	}while (!(totNum < maxTotNum && totNum > 0));

	for (i = 0; i < totNum; i++)
	{
		printf("inserire numero: ");
		scanf("%d", &num);
		if (num % 2 == 1)
			somma += num;
	}
	
	printf("somma: %d", somma);


	return 0;
}
