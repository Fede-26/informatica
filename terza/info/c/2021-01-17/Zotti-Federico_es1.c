//esercizio riportato su classroom
#include <stdio.h>

int main (int argc, char ** argv)
{

    int totAlunni;          //alunni totali
    int voto;
    int somma = 0;          //somma dei voti
    int votiAlunni = 9;     //voti per ogni alunno
    float media;            //media dei voti
    int i, j;
    
    printf("Alunni in 3ai: ");
    scanf("%d", &totAlunni);
    for (i = 1; i <= totAlunni; i++)
    {
        printf("alunno n.%d\n", i);
        for (j = 1; j <= votiAlunni; j++)
        {
            printf("inserire voto n.%d dell'alunno %d: ", j, i);
            scanf("%d", &voto);
            somma += voto;
        }

        media = somma / votiAlunni;
        printf("\nmedia per alunno %d: %f\n", i, media);
    }
    
    return 0;
}
