//insert sort
#include <stdio.h>
int main()
{
    int a[100], numero, i, j, t;

    printf("\nNumero di elementi: ");
    scanf("%d", &numero);

    printf("\nElementi: ");
    for(i = 0; i < numero; i++)
        scanf("%d", &a[i]);

    for(i = 1; i <= numero - 1; i++)
    {
        for(j = i; j > 0 && a[j - 1] > a[j]; j--)
        {
                t = a[j];
                a[j] = a[j - 1];
                a[j - 1] = t;
        }
    }
    printf("\n>> ");
    for(i = 0; i < numero; i++)
    {
        printf(" %d \t", a[i]);
    }
    printf("\n");
    return 0;
}
