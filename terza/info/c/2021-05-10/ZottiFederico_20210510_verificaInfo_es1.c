//Zotti Federico
//2021-05-10
//Verifica di informatica - esercizio 1


#include <stdio.h>
#include <string.h>

void carica_array(int dim, int arr[])
{
	int i;
	for (i=0; i<dim; i++)
	{
		printf("inserire valore (%d)\n>> ", i+1);
		scanf("%d", &arr[i]);
	}
}

void stampa_array(int dim, int arr[])
{
	int i;
	for (i=0; i<dim; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void bubble(int dim, int arr[])
{
	int i, sentinella=1, temp;

	while(sentinella)
	{
		sentinella = 0;
		for (i=0; i<dim-1; i++)
		{
			if (arr[i]>arr[i+1])
			{
				sentinella = 1;
				temp = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp;
			}
		}
	}
}

void selection(int dim, int arr[])
{
	int i, j;
	int min, temp;
	for (i=0; i<dim; i++)
	{
		min = i;
		for(j=i; j<dim; j++)
		{
			if (arr[min]>arr[j])
			{
				min = j;		
			}
		}

		temp = arr[i];
		arr[i] = arr[min];
		arr[min] = temp;
	}
}


void main() {

	int len;
	printf("Inserire lunghezza array: ");
	scanf("%d", &len);
	while (!(len>0))
	{
		printf("Inserire un valore positivo: ");
		scanf("%d", &len);
	}

	int array[len];

	carica_array(len, array);
	stampa_array(len, array);

	int scelta;
	printf("Inserire 1 per il bubble sort,\n2 per il selection sort: ");
	scanf("%d", &scelta);
	while (!(scelta==1 || scelta ==2))
	{
		printf("Inserire 1 per il bubble sort,\n2 per il selection sort: ");
		scanf("%d", &scelta);
	}

	switch (scelta)
	{
	case 1:
		bubble(len, array);
		break;
	
	case 2:
		selection(len, array);
		break;

	default:
		break;
	}

	stampa_array(len, array);

}
