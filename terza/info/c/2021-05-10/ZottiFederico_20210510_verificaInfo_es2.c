//Zotti Federico
//2021-05-10
//Verifica di informatica - esercizio 2

#include <stdio.h>
#include <string.h>

void main() {

	int dim = 80;
	char string[dim];
	char vocali[] = "aeiouAEIOU";

	printf("Inserire stringa (senza spazi): ");
	scanf("%s", string);

	int i, j, cont=0;
	for(i=0; i<dim; i++)
	{
		for(j=0; j<strlen(vocali); j++)
		{
			if (string[i] == vocali[j])
				cont++;
		}
	}

	printf("Numero di vocali: %d\n", cont);

}
