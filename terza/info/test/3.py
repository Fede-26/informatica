#!/usr/bin/env python

vocali = ["a", "e", "i", "o", "u"]

def generatore(parola):
    parolaNuova = []
    for lettera in parola:
        if lettera in vocali:
            parolaNuova.append(lettera)
        else:
            parolaNuova.append(lettera)
            parolaNuova.append("o")
            parolaNuova.append(lettera)
    return str(parolaNuova)


def main():
    parola = input("parola: ")
    print(generatore(parola))


if __name__ == "__main__":
    main()
