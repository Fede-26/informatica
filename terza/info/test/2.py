#!/usr/bin/env python

def inserimento():
    lista = []
    while 1:
        x = input("x: ")
        if x == "*":
            break
        else:
            lista.append(int(x))

    return lista


def draw(lista):
    print()
    for item in lista:
        print(str(item) + ": ", end="")
        print("*" * item)


def main():
    draw(inserimento())


if __name__ == "__main__":
    main()
