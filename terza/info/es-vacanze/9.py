#!/usr/bin/env python

"""
Scrivi una funzione che, data in ingresso una lista A contenente parole, restituisce in output una lista B di interi che rappresentano la lunghezza delle parole contenute in A.
"""

def lenParole(a):
    b = []
    for i in a:
        b.append(len(i))
    return b

print("lenParole:", lenParole(input("Inserire parole separate da spazi: ").split(" ")))
