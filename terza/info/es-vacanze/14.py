#!/usr/bin/env python

"""
Scrivi un programma che, passata come parametro una lista di interi, fornisce in output il maggiore tra i numeri contenuti nella lista.
"""

def maggiore(x):
    return max(x)

lista = []

for i in input("Separare i numeri con spazi: ").split(" "):
    lista.append(int(i))

print("max:", maggiore(lista))
