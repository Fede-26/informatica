#!/usr/bin/env python

"""
Definire una funzione eratostene(n) che ritorni tutti i numeri primi da 2 a n inclusi, utilizzando il procedimento del Crivello di Eratostene.
"""


def eratostene(n):
    prime = []
    output = []
    for i in range(2, n+1):
        if i not in prime:
            output.append(i)
            for j in range(i*i, n+1, i):
                prime.append(j)
    return output


print(eratostene(int(input("n: "))))
