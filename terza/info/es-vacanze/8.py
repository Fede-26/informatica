#!/usr/bin/env python

"""
 Scrivere un programma che stampi tutti i numeri perfetti inferiori ad un numero n dato in ingresso.
"""

def divisori(n):
    for i in range(1, int(n/2)+1):
        if n% i == 0:
            yield i

def isPerfect(n):
    if n == 0:
        return False
    elif sum(divisori(n)) == n:
        return True
    else:
        return False


print("isPerfect:", isPerfect(int(input("n: "))))
