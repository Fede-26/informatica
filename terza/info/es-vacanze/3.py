#!/usr/bin/env python

"""
Sullo stipendio dei dipendenti di una ditta viene applicata una trattenuta fiscale
"""

def trattenuta(scaglione):
    x = [5, 10, 15, 25, 35]
    return x[scaglione-1]

print("trattenuta:", trattenuta(int(input("scaglione: "))), "%")
