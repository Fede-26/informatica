#!/usr/bin/env python

"""
Scrivi una funzione generatrice di password. La funzione deve generare una stringa alfanumerica di 8 caratteri qualora l'utente voglia una password semplice, o di 20 caratteri ascii qualora desideri una password più complicata.
"""

import random

def password(isSimple):
    chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
    if isSimple:
        length = 8
    else:
        length = 20

    password = ""

    for i in range(length):
        password += random.choice(chars)

    return password

print("password:", password(int(input("1 for simple, 0 for strong: "))))
