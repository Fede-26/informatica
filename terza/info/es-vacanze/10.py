#!/usr/bin/env python

"""
Scrivi una funzione a cui passerai come parametro una stringa e ti restituirà una versione della stessa stringa al contrario (ad esempio "abcd" diventa "dcba".
"""

def reverseString(x):
    return x[::-1]

print("reverse:", reverseString(input("x: ")))
