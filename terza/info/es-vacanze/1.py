#!/usr/bin/env python

"""
Calcola l’imponibile relativo all’acquisto di una certa merce, noti la quantità acquistata, il prezzo unitario e lo sconto, tenendo presente che lo sconto viene applicato soltanto per quantità superiori a un valore dato.
"""

def sconta(prezzo, sconto):
    return prezzo - (prezzo * (sconto/100))

def scaglioni(n):
    scaglioni = { "1": 10,
                  "3": 20,
                  "5": 30,
                  "10": 35,
                  "11": 40 }
    if n > 10:
        n = 11
    return scaglioni[str(n)]

prezzi = { "1": 300,
           "2": 450,
           "3": 670 }

tipoMerce = input("tipoMerce:\n1-legno\n2-carbone\n3-ferro\n")
quantaMerce = int(input("quantaMerce: "))

prezzo = prezzi[tipoMerce] * quantaMerce

prezzo = sconta(prezzo, scaglioni(quantaMerce))

print("prezzo finale:", prezzo)
