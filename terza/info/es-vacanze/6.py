#!/usr/bin/env python

"""
Scrivere un programma che stampi la lunghezza delle stringhe fornite dall'utente, finchè l'utente non inserisce la stringa 'exit'.
"""

while 1:
    stringa = input("stringa: ")
    if stringa == "exit":
        break
    else:
        print(len(stringa))
