#!/usr/bin/env python

"""
In Svezia, i bambini giocano spesso utilizzando un linguaggio un pó particolare, detto "rövarspråket", che significa "linguaggio dei furfanti": consiste nel raddoppiare ogni consonante di una parola e inserire una "o" nel mezzo. Ad esempio la parola "mangiare" diventa "momanongogiarore". Scrivi una funzione in grado di tradurre una parola o frase passata tramite input() in "rövarspråket".
"""

def rovarspraket(x):
    risultato = ""
    for lettera in x:
        if not lettera in vocali:
            risultato += lettera+"o"+lettera
        else:
            risultato += lettera
    return risultato

vocali = [" ", "a", "e", "i", "o", "u"]

print(rovarspraket(input("x: ")))
