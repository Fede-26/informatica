#!/usr/bin/env python

"""
Un negoziante, per incrementare le sue vendite, prevede di applicare uno sconto progressivo sull’importo della fattura, in base al numero di pezzi acquistati.
"""

def sconto(n):
    scaglioni = { "1": 10,
                  "3": 20,
                  "5": 30,
                  "10": 35,
                  "11": 40 }
    if n > 10:
        n = 11
    return scaglioni[str(n)]

prezzo = int(input("prezzo: "))
print("prezzo scontato:", prezzo - (prezzo*(sconto(prezzo)/100)))
