#!/usr/bin/env python

"""
Progettare un algoritmo che legga da terminale una sequenza di interi positivi e negativi terminati dal valore 0 (uno su ogni linea) e stampi il prodotto degli interi positivi e la somma dei negativi.
"""

def inp():
    lista = []
    while 1:
        num = int(input("num: "))
        if num:
            lista.append(num)
        else:
            break

    return lista


prodPos = 1
somNeg = 0

for num in inp():
    if num > 0:
        prodPos *= num
    else:
        somNeg += num

print("prodPos:", prodPos)
print("somNeg:", somNeg)
