#!/usr/bin/env python

"""
Scrivi un algoritmo che dato in input il numero corrispondente al mese dell’anno, visualizzi il numero di giorni di cui il mese è composto. 
"""

mesi = {"gennaio": 31,
        "febbraio": 28,
        "marzo": 31,
        "aprile": 30,
        "maggio": 31,
        "giugno": 30,
        "luglio": 31,
        "agosto": 31,
        "settembre": 30,
        "ottobre": 31,
        "novembre": 30,
        "dicembre": 31}

def nGiorni(mese):
    if mese == 2:
        return 28

    elif mese <= 7:
        if mese%2 == 0:
            return 30
        else:
            return 31

    else:
        if mese%2 == 0:
            return 31
        else:
            return 30


print(nGiorni(int(input("mese: "))))
