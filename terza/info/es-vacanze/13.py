#!/usr/bin/env python

"""
Il ROT-13 è un semplice cifrario monoalfabetico, in cui ogni lettera del messaggio da cifrare viene sostituita con quella posta 13 posizioni più avanti nell'alfabeto. Scrivi una semplice funzione in grado di criptare una stringa passata, o decriptarla se la stringa è già stata precedentemente codificata.
"""

alfabeto = "abcdefghijklmnopqrstuvwxyz"

def rot13(x):
    y = ""
    for i in range(len(x)):
        for j in range(26):
            if x[i] == alfabeto[j]:
                if j >= 13:
                    y += alfabeto[j-13]
                else:
                    y += alfabeto[j+13]
    return y

print(rot13(input("x: ")))
