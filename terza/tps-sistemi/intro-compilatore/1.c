// Esempio di buffer overflow e downflow

#include <stdio.h>

int main(){

    int i = 0;
    while(i<=0){
        i = i - 1;
    }

    printf("%i\n\n", i);

}
