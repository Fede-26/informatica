#define DIM 100
#define MOD 100

int V[DIM];

int main()
{
	int m,VAR;
	carica(V,DIM);
	m=media(V,DIM);
	VAR=Varianza(V,DIM,m);
	return VAR;
}

void carica (int * v, int d)
{
	int i=0;
	for (;i<d;i++)
	{
		v[i]=rand()%MOD;
	}
}

int media(int * v, int d)
{
	int s=0; i;
	for(i=0;i<d;i++)
	{
		s=s+v[i];
	}
	return s/d;
}

int Varianza(int *v, int d, int m)
{
	int s=0; i;
	for(i=0;i<d;i++)
	{
		s=s+(v[i]-m)*(v[i]-m);
	}
	return s/d;
}
