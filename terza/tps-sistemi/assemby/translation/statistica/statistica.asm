.data
V dd 100 dup(?)

.stack MAIN
m       [ebp-8]
VAR     [ebp-4]
old ebp [ebp]
ret     [ebp+4]

.code MAIN
main:
    push ebp
    mov ebp, esp
    sub esp, 8
    pushall

    lea ebx, [V]

    push 100
    push ebx
    call carica
    add esp, 8

    push 100
    push ebx
    call media
    add esp, 8
    mov [ebp-8], eax

    push eax
    push 100
    push ebx
    call Varianza
    add esp, 12
    mov [ebp-4], eax

    popall
    add esp, 8
    pop ebp
    ret

.stack carica
i       [ebp-4]
old ebp [ebp]
ret addr[ebp+4]
*v      [ebp+8]
d       [ebp+12]

.code carica
carica:
    push ebp
    mov ebp, esp
    sub esp, 4
    pushall

    mov dword ptr [ebp-4], 0
    mov esi, 0
    mov ebx, [ebp+8]

    for:
    cmp esi, [ebp+12]
    jge eFor

        call rand
        mov ecx, 100
        mov edx, 0
        idiv ecx
        mov [ebx+4*esi], edx

    inc esi
    jmp for
    eFor:

    mov [ebp-4], esi
    add esp, 4
    pop ebp
    popall
    ret

.stack media
s       [ebp-8, eax]
i       [ebp-4, esi]
old ebp [ebp]
ret addr[ebp+4]
*v      [ebp+8, ebx]
d       [ebp+12]

.code media
media:
    push ebp
    mov ebp, esp
    sub esp, 8
    pushall

    mov dword ptr [ebp-8], 0
    mov dword ptr [ebp-4], 0
    mov eax, 0
    mov esi, 0
    mov ebx, [ebp+8]

    for:
    cmp esi, [ebp+12]
    jge eFor

        add eax, [ebp+4*esi]

    inc esi
    jmp for
    eFor:

    mov edx, 0
    idiv [ebp+12]

    add esp, 8
    pop ebp
    popall
    ret


.stack Varianza
s       [ebp-8, eax]
i       [ebp-4, esi]
old ebp [ebp]
ret addr[ebp+4]
*v      [ebp+8, ebx]
d       [ebp+12]
m       [ebp+16]

.code Varianza
Varianza:
    push ebp
    mov ebp, esp
    sub esp, 8
    pushall

    mov dword ptr [ebp-8], 0
    mov dword ptr [ebp-4], 0
    mov eax, 0
    mov esi, 0
    mov ebx, [ebp+8]

    for:
    cmp esi, [ebp+12]
    jge eFor

        mov ecx, [ebp+4*esi]
        sub ecx, [ebp+16]
        imul ecx, ecx
        add eax, ecx

    inc esi
    jmp for
    eFor:

    mov edx, 0
    idiv [ebp+12]

    add esp, 8
    pop ebp
    popall
    ret
