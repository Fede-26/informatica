V dd 100 dup (?)
i dd ?
p dd ?
d dd ?

.stack main
old ebp [ebp]
ret addr [ebp+4]

.code main
main:
    push ebp
    mov ebp, esp
    
    lea eax, [V]

    mov [i], 0
    for:
    mov esi, [i]
    cmp esi, 100
    jge eFor

        call rand
        mov [ebx+4*esi], eax

        inc [i]
    jmp for
    eFor:

    push 100
    push ebx
    call contaT
    mov [p], eax
    add esp, 8

    pop ebp
    ret

.stack contaT
int i   [ebp-8]
int t   [ebp-4]
old ebp [ebp]
ret addr[ebp+4]
int *V  [ebp+8]
int d   [ebp+12]

.code contaT
contaT:
    pushall
    push ebp
    mov ebp, esp
    sub esp, 8

    mov dword ptr [ebp-4], 0

    mov ebx, [ebp+8]

    mov dword ptr [ebp-8], 0
    for:
    mov esi, [ebp-8]
    cmp esi, [ebp+12]
    jge eFor

        if:
        push [ebp+4*esi]
        call T
        add esp, 4
        cmp eax, 1
        jne eIf

            inc dword ptr [ebp-4]

        eIf:

    inc dword ptr [ebp-8]
    jmp for
    eFor:

    add esp, 8
    pop ebp
    popall
    mov eax, [ebp-4]
    ret

.stack T
int i   [ebp-8]
int s   [ebp-4]
old ebp [ebp]
ret addr[ebp+4]
n       [ebp+8]

.code T
T:
    pushall
    push ebp
    mov ebp, esp
    sub esp, 8

    mov dword ptr [ebp-8], 1
    mov dword ptr [ebp-4], 0

    mov eax, [ebp-8]
    for:
    cmp eax, [ebp+8]
    jge eFor

        add eax, [ebp-8]

    inc dword ptr [ebp-8]
    jmp for
    eFor:

    mov [ebp-4], eax

    if:
    cmp eax, [ebp+8]
    jne else

        mov eax, 1
    
    jmp eIf
    else:

        mov eax, 0
    
    eIf:

    popall
    add esp, 8
    pop ebp
    ret

pushall:
push ebx
push ecx
push edx
push edi
push esi

popall:
pop ebx
pop ecx
pop edx
pop edi
pop esi
