/* Conta Triangolari in un vettore*/
#include <stdio.h>

#define DIM 100

int V[DIM],i,p,d;

int T( int );
int contaT(int *, int );

int main()
{
	for (i=0;i<DIM;i++)
		V[i]=rand();
		
	p=contaT(V,DIM);
	
	return p;
}

int contaT( int * V,  int d)
{
	int i;
	int t=0;
	for (i=0; i<d; i++)
		if ( T(V[i])==1 )
			t++;

	return t;
}

int T  (int n)
{
	int i=1;
	int s=0;
	
	for (i=1; s<n; i++)
		s=s+i;
	if (s==n) 
		return 1;
	else return 0;
}