.DATA
.STACK
; c --> ebp=12
; b --> ebp=8
; a --> ebp=4


main:
	push ebp
	mov ebp, esp
	add ebp, 16

	mov eax, [ebp-4]
	cmp eax, [ebp-8]
	jge fineif
	mov [ebp-12], eax
	mov ebx, [ebp-8]
	mov [ebp-4], ebx
	mov ebx, [ebp-12]

	mov [ebp-4], eax
	mov [ebp-8], ebx	

fineif:
	cmp eax, 0
	jle finewhile

	cmp eax, [ebp-8]
	jge fineif2
	mov [ebp-12], eax
	mov ebx, [ebp-8]
	mov [ebp-4], ebx
	mov ebx, [ebp-12]

	mov [ebp-4], eax
	mov [ebp-8], ebx

fineif2:
	sub eax, [ebp-8]
finewhile:
	mov eax, [ebp-4]
	add esp, 12
	pop ebp
	ret
