/* Conta pari e dispari in un vettore */
#include <stdio.h>

#define DIM 50 000 000

int V[DIM],i,p,d;

int contaPari( int *, int );
int contaDespar(int *, int );

int main()
{
	for (i=0;i<DIM;i++)
		V[i]=rand();
		
	p=contaPari(V,DIM);
	d=contaDespar(V,DIM);
	printf("pari = %i e dispari = %i. \n",p,d);
	if (DIM==p+d)
		return 0;
	else return d;
}

int contaPari( int * V,  int d)
{
	int i;
	int pari=0;
	for (i=0; i<d; i++)
		if (V[i]%2 == 0)
			pari++;

	return pari;
}

int contaDespar( int * V,  int d)
{
	int i;
	int spari=0;
	for (i=0; i<d; i++)
		if (V[i]%2 != 0)
			spari++;

	return spari;
}
