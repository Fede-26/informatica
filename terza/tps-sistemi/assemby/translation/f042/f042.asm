.DATA
DIM dd 50000000
V dd DIM dup(?)
i dd ?
p dd ?
d dd ?

.STACK contaPari

ebp-8	pari
ebp-4	i
ebp		old ebp
ebp+4	ret addr
ebp+8	*V
ebp+12	d

.CODE
contaPari:
	push ebp
	mov ebp, esp
	sub esp, 8

	mov ebx, [ebp+8]

	mov [ebp-8], 0

	mov [ebp-4], 0
	for:
	mov esi, [ebp-4]
	cmp esi, [ebp+12]
	jge eFor

		if:
		mov edx, 0
		mov eax, [ebx+4*esi]
		mov ecx, 2
		idiv ecx
		cmp edx, 0
		jne eIf

			inc [ebp-8]
		
		eIf:

		
	inc [ebp-4]
	eFor:
	
	mov eax, [ebp-8]
	add esp, 8
	pop ebp
	ret

.STACK main

ebp		old ebp
ebp+4	ret addr

.CODE
main:

	push ebp
	mov esp, ebp
	
	lea ebx, [V]
	
	mov [i], 0
	for:
	mov esi, [i]
	cmp esi, [DIM]
	jge eFor

		call rand
		mov [ebx+4*esi], eax

	inc [i]
	eFor:

	push [DIM]
	push ebx
	call contaPari
	add esp, 8
	mov [p], eax

	push [DIM]
	push ebx
	call contaDespar
	add esp, 8
	mov [d], eax

	;printf()

	if:
	mov ecx, [p]
	add ecx, [d]
	cmp [DIM], ecx
	jne else

		mov eax, 0

	jmp eIf

	else:

		mov eax, [d]

	eIf:

	pop ebp
	ret


#Pushall
	push ebx
	push ecx
	push edx
	push esi
	push edi

#Popall
	pop edi
	pop esi
	pop edx
	pop ecx
	pop ebx
