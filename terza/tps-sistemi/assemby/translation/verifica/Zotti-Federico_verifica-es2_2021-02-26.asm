.data

v1 dd 20 dup(?)
v2 dd 20 dup(?)
i dd 1


.code

main:
    push ebp
    mov ebp, esp
    sub esp, 12
    ;t=[ebp-4]
    ;s=[ebp-8]
    ;j=[ebp-12]

    mov [ebp-8], 0  ;s=0
    mov [ebp-4], 0  ;t=0

    lea ebx, [v1]

    mov [i], 0  ;for (i=0;i<20;i++)
    for:
    mov esi, [i]
    cmp esi, 20
    jge eFor

        mov [ebp-12], 2 ;j=2

        while:  ;while (j<v1[i])
        mov eax, [ebx+esi*4]
        cmp [ebp-12], eax
        jge eWhile

            if1:    ;if (v1[i]%j==0)
            mov edx, 0
            idiv [ebp-12]
            jne else1

                inc [ebp-8]
                jmp eIf1

            else1:

                inc [ebp-4]
                inc [ebp-12]

            eIf1:

        jmp while
        eWhile:


    inc [i]
    jmp for
    eFor:

    if2:    ;if ( (t<s) && (s%3==0) )
    mov eax, [ebp-8]
    cmp [ebp-4], eax
    jge else2

    mov edx, 0
    idiv 3
    cmp edx, 0
    jne else2

        mov eax, 1
        jmp eIf2

    else2:

        mov eax, 0

    eIf2:

    add esp, 12
    pop ebp
    ret
