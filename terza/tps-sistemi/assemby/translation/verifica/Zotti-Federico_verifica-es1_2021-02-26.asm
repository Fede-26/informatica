.data

s dd ?
i dd 1


.code

main:
    push ebp
    mov ebp, esp
    sub esp, 8
    ;j=[ebp-4]
    ;n=[ebp-8]

    mov [s], 0  ;s=0

    mov [i], 1  ;for (i=1; i<=n; i++)
    for:
    mov esi, [i]
    cmp esi, [ebp-8]
    jg eFor

        mov [ebp-4], 2  ;j=2

        while:  ;while (j<i)
        cmp [ebp-4], esi
        jge eWhile

            if1:    ;if (i%j==0)
            mov eax, esi
            mov edx, 0
            idiv [ebp-4]
            cmp eax, 0
            jne eIf1

                mov [ebp-4], esi    ;j=i+1
                                    ;j=j+1
                add [ebp-4], 2

            eIf1:

            if2:    ;if (j==i)
            cmp [ebp-4], esi
            jne eIf2

                if3:    ;if(n%i==0)
                mov eax, [ebp-8]
                mov edx, 0
                idiv esi
                cmp edx, 0
                jne eIf3

                    add [s], esi    ;s=s+i

                eIf3:

            eIf2:


        jmp while
        eWhile:

    inc [i]
    jmp for
    eFor:

    if4:    ;if (s/2 ==n)
    mov ecx, [s]
    shr ecx, 1
    cmp ecx, [ebp-8]
    jne else4

        mov eax, 1
        jmp eIf4

    else4:

        mov eax, 0

    eIf4:

    add esp, 8
    pop ebp
    ret
