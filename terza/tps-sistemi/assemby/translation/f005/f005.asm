.CODE        
main:    PUSH    EBP
    MOV    EBP,ESP
    SUB    ESP,8
    MOV    DWORD PTR [EBP-4],0
    MOV    BYTE PTR [EBP-8],'\0'
    MOV    BYTE PTR [EBP-8],'A'
    MOV    DWORD PTR [EBP-4],4
        
    MOV    EAX,0
    MOV    AL,'x'
    PUSH    EAX
    PUSH    8
    PUSH    7
    PUSH    6
    PUSH    5
    PUSH    4
    PUSH    3
    PUSH    2
    PUSH    1
    CALL    ciccio
    ADD    ESP,36
        
    SHL    [EBP-4],1
    INC    [EBP-8]
        
    MOV    EAX,0
    MOV    AL,'b'
    PUSH    EAX
    PUSH    8
    PUSH    [EBP-4]
    PUSH    5
    PUSH    4
    PUSH    3
    PUSH    2
    PUSH    1
    PUSH    [EBP-4]
    CALL    ciccio
    ADD    ESP,36
    MOV    EAX,0
    ADD    ESP,8
    POP    EBP
    RET    
        
ciccio:    PUSH    EBP
    SUB    ESP,4
    MOV    [EBP-4],0
    MOV    EAX,[EBP-4]
    ADD    EAX,[EBP+8]
    ADD    EAX,[EBP+12]
    ADD    EAX,[EBP+16]
    ADD    EAX,[EBP+20]
    ADD    EAX,[EBP+24]
    ADD    EAX,[EBP+28]
    ADD    EAX,[EBP+32]
    ADD    EAX,[EBP+36]
    MOV    [EBP-4],EAX
    MOV    BYTE PTR [EBP+40],EAX
    ADD    ESP.4
    POP    EBP
    RET



.STACK    
.main    
char c    [EBP-8]
int a    [EBP-4]
old ebp    [EBP]
ret address    [EBP+4]

.STACK    
.ciccio    
    
int ci    [EBP-4]
old ebp    [EBP]
ret address    [EBP+4]
int z0    [EBP+8]
int z1    [EBP+12]
int z2    [EBP+16]
int z3    [EBP+20]
int z4    [EBP+24]
int z5    [EBP+28]
int z6    [EBP+32]
int z7    [EBP+36]
int c    [EBP+40
