/* funzione void con un sacco di parametri */
void ciccio(int, int,int,int,int,int,int,int,char);

int main()
{
	int a=0;
	char c='\0';
	
	c='A';
	a=4;
	ciccio(1,2,3,4,5,6,7,8,'x');
	
	a=a*2;
	c=c+1;
	ciccio(a,1,2,3,4,5,a,8,'b');
	
	return 0;
}

void ciccio(int z0,int z1,int z2,int z3,int z4,int z5,int z6,int z7,char c)
{
	register int ci=0;
	ci=z0+z1+z2+z3+z4+z5+z6+z7;
	c=(char) ci;
}
