/*Conta primi in un vettore */
#include <stdio.h>

#define DIM 100

int V[DIM],i,p,d;

int Primo( int );
int contaPrimi(int *, int );

int main()
{
	for (i=0;i<DIM;i++)
		V[i]=rand();
		
	p=contaPrimi(V,DIM);
	
	return p;
}

int contaPrimi( int * V,  int d)
{
	int i;
	int primi=0;
	for (i=0; i<d; i++)
		if ( Primo(V[i]==1) )
			primi++;

	return primi;
}

int Primo  (int n)
{
	int i=2;
	
	while (i<n)
	{
		if ( (n%i)==0 )
			n=0;
		n++;
	}
	if (i==n) 
		return 1;
	else return 0;
}
