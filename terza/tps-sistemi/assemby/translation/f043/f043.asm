.DATA

.STACK		
i	[EBP-4]	
OLD EBP	[EBP]	
RET ADDRESS	[EBP+4]	
n	[EBP+8]	
					
.CODE					
Primo:
	PUSH	EBP			
	MOV	EBP,ESP			
	SUB	ESP,4			
	Pushall				
	MOV	DWORD PTR [EBP-4],2			
	while1:
	MOV	EAX, [EBP+8]			
	CMP	[EBP-4], EAX			
	JGE	Fine_while1			

		if3:
		XOR	EDX,EDX		
		IDIV	[EBP-4]		
		CMP	EDX, 0		
		JNE	Fine_if3		
			MOV	DWORD PTR [EBP+8],0	
		INC	[EBP+8]		
		JMP	while1		
	Fine_while1:					

	if4:
	CMP	[EBP-4],EAX			
	JNE	else4			
		MOV	EAX,1		
		JMP	Fine_if4		
	else4:
		MOV	EAX,0		
	Fine_if4:
	Popall				
	ADD	ESP,4			
	POP	EBP			
	RET				
					
