;define
;DIM = 100

.data
V dd 100 dup (?)

.stack main
s       [ebp-8]
c       [ebp-4]
old ebp [ebp]
ret addr[ebp+4]

.code main
main:
    push ebp
    mov ebp, esp
    sub esp, 8
    pushall

    lea ebx, [V]
    ;carica(V, DIM)
    push 100
    push ebx
    call carica
    add esp, 8

    ;s=somma()
    push 100
    push ebx
    call somma
    mov [ebp-8], eax
    add esp, 8

    ;c=cerca(V, DIM, 2)
    push 2
    push 100
    push ebx
    call cerca
    mov [ebp-4], eax
    add esp, 12

    mov eax, 0
    popall
    add esp, 8
    pop ebp
    ret


.stack carica
i           [ebp-4, esi]
old ebp     [ebp]
ret addr    [ebp+4]
*v          [ebp+8]
f           [ebp+12]

.code carica
carica:
    push ebp
    mov ebp, esp
    sub esp, 4
    pushall

    mov ebx, [ebp+8]

    mov dword ptr [ebp-4], 0
    mov esi, 0
    for:
    cmp esi, [ebp+12]
    jge eFor

        call rand
        ;mov edx, 0
        ;mov ecx, 1024
        ;idiv ecx
        ;mov [ebx+4*esi], edx
        and eax, 1023
        mov [ebx+4*esi], eax

    inc esi
    jmp for
    eFor:

    ;mov eax, 0
    popall
    add esp, 4
    pop ebp
    ret


.stack somma
i           [ebp-8, esi]
s           [ebp-4, edi]
old ebp     [ebp]
ret addr    [ebp+4]
*v          [ebp+8]
f           [ebp+12]

.code somma
somma:
    push ebp
    mov ebp, esp
    sub esp, 8
    pushall

    mov dword ptr [ebp-4], 0
    mov dword ptr [ebp-8], 0

    mov edi, 0
    mov ebx, [ebp+8]

    mov esi, 0
    for:
    cmp esi, [ebp+12]
    jge eFor

        add edi, [ebx+4*esi]

    inc esi
    jmp for
    eFor:

    mov eax, edi
    popall
    add esp, 8
    pop ebp
    ret


.stack cerca
i           [ebp-8, esi]
c           [ebp-4, edi]
old ebp     [ebp]
ret addr    [ebp+4]
*v          [ebp+8]
f           [ebp+12]
d           [ebp+16]

.code cerca
cerca:
    push ebp
    mov ebp, esp
    sub esp, 8
    pushall

    mov edi, 0
    mov dword ptr [ebp-4], 0
    mov dword ptr [ebp-8], 0

    mov ebx, [ebp+8]

    mov esi, 0
    for:
    cmp esi, [ebp+12]
    jge eFor

        if:
        mov edx, [ebp+16]
        cmp [ebx+4*esi], edx
        jne eIf

            inc edi

        eIf:

    inc esi
    jmp for
    eFor:

    mov eax, edi
    popall
    add esp, 8
    pop ebp
    ret
