#define DIM 100


void carica(int *, int);
int somma(int *, int);
int cerca(int *,int, int);


int V[DIM];


int main()
{
int s,c;
carica(V,DIM);
s=somma(V,DIM);
c=cerca(V,DIM,2);
return 0;
}


void carica(int * v, int f)
{
register int i;
for(i=0;i<f;i++)
v[i]=rand()%1024;
}


int somma(int * v, int f)
{
register int i;
register int s=0;
for(i=0;i<f;i++)
s=s+v[i];
return s;
}


int cerca(int *v, int f, int d)
{
register int i;
register int c=0;
for(i=0;i<f;i++)
if (v[i]==d) c++;
/* c+=(v[i]==d); */
return c;
}

//v[i]==d -> 0/1
