;equ
;DIM 5000000

.data
Vet dd 50000000 dup(?)
i dd ?
p dd ?
e dd ?

.stack main


old ebp     [ebp]
ret addr    [ebp+4]

.code

main:
    push ebp
    mov ebp, esp

    lea ebx, [Vet]
    mov [i], 0
    for:
    mov esi, [i]
    cmp esi, 50000000
    jge eFor

        call rand
        mov [ebx+4*esi], eax

    inc [i]
    jmp for
    eFor:

    push 50000000
    push ebx
    call ordina
    add 8, esp

    call rand
    mov [e], eax

    push [e]
    push 50000000-1
    push 0
    push ebx
    call trova
    mov [p], eax
    add 16, esp

    pop ebp
    ret

.stack ordina

i           [ebp-12]
s           [ebp-8]
uffa        [ebp-4]
old ebp     [ebp]
ret addr    [ebp+4]
V           [ebp+8, ebx]
50000000         [ebp+12, ecx]

.code ordina
ordina:
    push ebp
    mov ebp, esp
    sub esp, 12
    pushall

    mov ebx, [ebp+8]
    mov ecx, [ebp+12]

    while:
    mov edx, [ebp+12]
    cmp edx, 0
    je eWhile

        mov dword ptr [ebp+12], 0

        mov dword ptr [ebp-12], 1
        for:
        mov esi, [ebp-12]
        cmp esi, ecx
        jge eFor

            if:
            mov eax, [ebx+4*esi]
            dec esi
            cmp eax, [ebx+4*esi]
            inc esi
            jle eIf

                push [ebx+4*esi]
                pop [ebx-8]

                dec esi
                push [ebx+4*esi]
                inc esi
                pop [ebx+4*esi]

                push [ebx-8]
                dec esi
                pop [ebx+4*esi]
                inc esi

                mov dword ptr [ebp-4], 0

            eIf:

        inc [ebp-12]
        jmp for
        eFor:

    jmp while
    eWhile:

    popall
    add esp, 12
    pop ebp
    ret

.stack trova

i           [ebp-4]
old ebp     [ebp]
ret addr    [ebp+4]
V           [ebp+8, ebx]
a           [ebp+12, eax]
b           [ebp+16, ecx]
x           [ebp+20, edx]

.code trova
trova:
    push ebp
    mov ebp, esp
    sub esp, 20
    pushall

    mov ebx, ebp+8
    mov eax, ebp+12
    mov ecx, ebp+16
    mov edx, ebp+20

    mov esi, eax
    sub esi, ecx
    shr esi, 1
    mov [ebp-4], esi

    if:
    cmp [ebx+4*esi], edx
    jne else

        push [ebp-4]

    jmp eIf

    else:

        if2:
        jge else2

            push edx
            push esi
            push eax
            push ebx
            call trova
            add esp, 16
            push eax

        jmp eIf2
        else2:

            push edx
            push ecx
            push esi
            push ebx
            call trova
            add esp, 16
            push eax

        eIf2:

    eIf:

    pop eax
    popall
    add esp, 20
    pop ebp
    ret
