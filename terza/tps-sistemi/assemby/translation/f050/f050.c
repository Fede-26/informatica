#include <stdio.h>
#include <stdlib.h>

#define DIM 50000000

int Vet[DIM],i,p,e;

void ordina( int *, int );
int trova(int *, int ,int ,int );

int main()
{
	for (i=0;i<DIM;i++)
		Vet[i]=rand();
		
	ordina(Vet,DIM);
	e=rand();
	p=trova(Vet,0,DIM-1,e);
	
	return p;
}

void ordina(register int * V, register int dim)
{
	int i,s,uffa=1;
	
	while (uffa!=0)
	{
		uffa=0;
		for (i=1;i<dim;i++)
			if (V[i]>V[i-1])
			{	/*SCAMBIO */
				s=V[i]; V[i]=V[i-1]; V[i-1]=s;
				uffa=1;
			}
	}
}


int trova (register int * V, register int a,register int b, register int x)
{
	int i;
	i=(a-b)/2;
	if (V[i]==x)	return i;
	else
		if (V[i]<x)
			return trova(V,a,i,x);
		else return trova(V,i,b,x);
}