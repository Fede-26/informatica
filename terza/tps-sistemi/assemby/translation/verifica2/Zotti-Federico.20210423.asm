.data

.stack main
a           [ebp-108]
b           [ebp-104]
v[25]       [ebp-25*4, ebp-100, ebx]
old ebp     [ebp]
ret addr    [ebp+4]

.code main
main:
    push ebp
    mov ebp, esp
    sub esp, 108
    pushall

    lea ebx, [ebp-100]

    ;carica(v, dim)
    push 100
    push ebx
    call carica
    add esp, 8

    ;bubble(dim, v)
    push ebx
    push 100
    call bubble
    add esp, 8

    if:
    mov eax, [ebx+96]   ;96=DIM-1 *4
    cmp eax, 100
    jle eIf

        mov [ebx], eax

    eIf:

    ;a = pinpals(dim,v);
    push ebx
    push 100
    call pinpals
    add esp, 8
    mov [ebp-108], eax

    popall
    add esp, 108
    pop ebp
    ret


.stack carica
i       [ebp-8, esi]
c       [ebp-4, ecx]
old ebp [ebp]
ret addr[ebp+4]
*v      [ebp+8, ebx]
d       [ebp+12]

.code carica
carica:
    push ebp
    mov ebp, esp
    sub esp, 8
    pushall

    mov dword ptr [ebp-8], 0
    mov esi, 0
    mov dword ptr [ebp-4], 100
    mov ecx, 100

    mov ebx, [ebp+8]

    for:
    cmp esi, [ebp+12]
    jge eFor

        mov edx, 44
        imul edx, ecx
        mov [ebx+4*esi], edx

    inc esi
    jmp for
    eFor:

    mov [ebp-8], esi    ;serve solo a sincronizzare il valore
                        ;del registro con la variabile locale

    popall
    add esp, 8
    pop ebp
    ret


.stack bubble
i           [ebp-12, edi]
j           [ebp-8, esi]
c           [ebp-4, ecx]
old ebp     [ebp]
ret addr    [ebp+4]
a           [ebp+8]
*b          [ebp+12, ebx]

.code bubble
bubble:
    push ebp
    mov ebp, esp
    sub esp, 12
    pushall

    mov dword ptr [ebp-12], 0
    mov edi, 0

    mov ebx, [ebp+12]

    for1:
    mov edx, [ebp+8]
    sub edx, 2  ;edx=a-2
    cmp edi, edx
    jge eFor1

        mov esi, edi
        inc esi
        inc edx ;edx=a-1
        for2:
        cmp esi, edx
        jge eFor2

            if:
            inc esi
            mov eax, [ebx+4*esi]    ;eax=b[j+1]
            dec esi
            cmp eax, [ebx+4*esi]
            jge eIf

                mov ecx, [ebx+4*esi]
                mov [ebx+4*esi], eax
                inc esi
                mov [ebx+4*esi], ecx
                dec esi

            eIf:

        inc esi
        jmp for2
        eFor2:

    inc edi
    jmp for1
    eFor1:

    mov [ebp-4], ecx
    mov [ebp-8], esi
    mov [ebp-12], edi    ;serve solo a sincronizzare il valore
                        ;del registro con la variabile locale

    popall
    add esp, 12
    pop ebp
    ret


.stack pinpals
i           [ebp-12, esi]
l           [ebp-8, edx]
c           [ebp-4]
old ebp     [ebp]
ret addr    [ebp+4]
a           [ebp+8, eax]
*b          [ebp+12, ebx]

.code pinpals
pinpals:
    push ebp
    mov ebp, esp
    sub esp, 12
    pushall

    mov edx, [ebp+8]    ;l=a/2
    shr edx, 1

    mov eax, [ebp+8]

    mov dword ptr [ebp-12], 0
    mov esi, 0

    for:
    cmp esi, edx
    jg eFor

        dec eax

        if:
        mov ecx, [ebx+4*esi]
        cmp ecx, [ebx+4*eax]
        je eIf

            mov [ebp+12], ebx
            mov [ebp+8], eax
            mov [ebp-8], edx
            mov [ebp-12], esi    ;serve solo a sincronizzare il valore
                                ;del registro con la variabile locale


            mov eax, 0
            popall
            add esp, 12
            pop ebp
            ret

        eIf:

    inc esi
    jmp for
    eFor:

    mov [ebp+12], ebx
    mov [ebp+8], eax
    mov [ebp-8], edx
    mov [ebp-12], esi    ;serve solo a sincronizzare il valore
                        ;del registro con la variabile locale

    mov eax, 1
    popall
    add esp, 12
    pop ebp
    ret
