.DATA

V dw 20 dup (?)
i dw ?          ;ebx
ContaPari dw 0

.CODE

main:

    push ebp
    mov ebp, esp

	lea ebx, [V]	;ecx = &V
	mov [i], 0		;for(i=0;i<20;i++)
    for:
    mov esi, [i]
    cmp esi, 20
    jge eFor
  
        call rand				;V[i]=rand()
        mov [ebx+esi*4], eax

        inc [i]

        jmp for

    eFor:

    for2:    		;for(i=0;i<20;i++)
    mov esi, [i]
    cmp esi, 20
    jge eFor2

		lea ebx, [V]			;edx=V[i]%2
        mov eax, [ebx+esi*4]
        mov edx, 0
        mov ecx, 2
        idiv ecx

    if:			;if(edx==0)
    cmp edx, 0
    jne eIf

        inc [ContaPari]	;contapari++
    eIf:

    inc [i]
    jmp for2

    eFor2:
    mov [i], esi	;i=ebx
    
    pop ebp
    ret [ContaPari]
