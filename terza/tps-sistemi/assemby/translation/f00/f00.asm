;STACK somma

;s (int)         ;ebp-4
;
;ebp
;
;retaddress ;ebp+4
;a          ;ebp+8
;b          ;ebp+12

somma:
    push ebp
    mov ebp, esp
    sub esp, 4

    mov eax, [ebp+12]
    add eax, [ebp+8]
    mov [ebp-4], eax

    add esp, 4
    pop ebp
    ret


;STACK main

;s      ;ebp-16
;d      ;ebp-12
;c      ;ebp-8
;a      ;ebp-4
;
;ebp


main:
    push ebp
    mov ebp, esp
    sub esp, 16

    mov dword ptr [ebp-4], 3  ;a=3
    mov dword ptr [ebp-8], 5  ;c=5

    push [ebp-8]        ;push c
    push [ebp-4]        ;push a
    call somma          ;eax=somma(a,c)
    add esp, 8          ;ripulire i parametri dallo stack
    mov [ebp-16], eax   ;s=eax

    push 6              ;push 6
    push [ebp-16]       ;push s
    call somma          ;d=somma(s,6)
    add esp, 8          ;ripulire i parametri dallo stack
    mov [ebp-12], eax   ;s=eax

    pop ebp
    add esp, 16
    ret
