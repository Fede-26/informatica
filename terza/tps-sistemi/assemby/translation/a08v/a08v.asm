.DATA

v dd 100 dup (?)

.STACK

.CODE

MAIN:
    push ebp
    mov ebp, esp
    add ebp, 8
    ;i = ebp-4; esi
    ;j = ebp-8; edi
    ;DIM = 100; eax

    mov eax, 100

    lea ebx, [v]    ;ebx = &v

    mov [ebp-4], 0  ;for(i=0;i<DIM-1;i++)
    for:
    mov esi, [ebp-4]
    dec eax
    cmp esi, eax
    inc eax
    jge eFor

        dec eax
        mov [ebp-8], eax ;for(j=DIM-1;j>i;j--)
        inc eax
        for2:
        mov edi, [ebp-8]
        cmp edi, esi
        jle eFor2

            mov edx, [ebp+edi*4]    ;edx = v[j]

            dec edi
            mov ecx, [ebp+edi*4]     ;ecx = v[j-1]

            if:
            cmp edx, ecx
            jle eIf

                add edx, ecx        ;edx=edx+ecx -> v[j]=v[j-1]+v[j];
                sub ecx, edx        ;ecx=ecx-edx -> v[j-1]=v[j-1]-v[j];     ;v[j-1]=v[j]-v[j-1]
                imul ecx, ecx, -1   ;ecx=ecx*-1
                add edx, ecx        ;edx=edx+ecx -> v[j]=v[j]-v[j-1];

                mov [ebp+edi*4], ecx    ;ecx = v[j-1]
                inc edi

                mov [ebp+edi*4], edx    ;edx = v[j]

            eFi:

        dec [ebp-8]
        eFor2:

    inc [ebp-4]
    eFor:
    ret
