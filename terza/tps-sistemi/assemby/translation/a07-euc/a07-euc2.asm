.DATA
.STACK

.CODE
main:
	push ebp
	mov ebp, esp
	add ebp, 12

	mov eax, [ebp-4]	;eax = a
	cmp eax, [ebp-8]
	jge fineif

	mov [ebp-12], eax	;c = eax
	
	mov ebx, [ebp-8]	;ebx = b	;a = b
	mov [ebp-4], ebx	;a = ebx
	
	mov ebx, [ebp-12]	;ebx = c 	;b = c
	mov [ebp-8], ebx	;b = ebx

fineif:

	mov eax, [ebp-4]	;eax = a 	;reset register to variables
	mov ebx, [ebp-8]	;ebx = b
	mov ecx, [ebp-12]	;ecx = c

	mov edx, 0			;edx = 0
	idiv ebx			;eax = edx:eax / ebx
						;edx = edx:eax % ebx
	mov eax, [ebp-4]	;eax = a

	mov ecx, edx		;ecx = edx

while:					;while (c>0)
	cmp ecx, 0
	jle finewhile

	mov eax, ebx		;eax = ebx
	mov ebx, ecx		;ebx = ecx

	mov edx, 0			;edx = 0
	idiv ebx			;eax = edx:eax / ebx
						;edx = edx:eax % ebx
	mov eax, [ebp-4]	;eax = a

	mov ecx, edx		;ecx = edx
	
	jmp while

finewhile:
	mov [ebp-4], eax	;a = eax
	mov [ebp-8], ebx	;b = ebx
	mov [ebp-12], ecx	;c = ecx
	
	add esp, 12
	pop ebp
	ret
