.code
main:
    push ebp
    mov ebp, esp
    ;a=[ebp-4]
    ;b=[ebp-8]
    ;c=[ebp-12]
    sub ebp, 12

    if:
    mov eax, [ebp-4]    ;eax=a
    cmp eax, [ebp-8]
    jge eIf

        mov [ebp-12], eax   ;c=eax
        mov eax, [ebp-8]    ;eax=b
        mov [ebp-4], eax    ;a=eax
        mov ebx, [ebp-12]   ;ebx=c
        mov [ebp-8], ebx    ;b=ebx

    eIf:

    mov edx, 0  ;c=a%b
    idiv [ebp-8]    ;eax=edx:eax/b
                    ;edx=edx:eax%b
    mov [ebp-12], edx

    while:
    cmp edx, 0
    jle eWhile

        mov eax, [ebp-8]    ;eax=b
        mov [ebp-4], eax    ;a=eax
        mov [ebp-8], edx    ;b=edx(c)

        mov edx, 0
        idiv [ebp-8]
        mov [ebp-12], edx

    jmp while
    eWhile:

    add ebp, 12
    pop ebp
    mov eax, [ebp-4]
    ret
