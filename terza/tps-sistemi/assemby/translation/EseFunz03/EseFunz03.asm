.stack main
S[100]  db  [ebp-116, ebx]
s db        [ebp-16]
i dd        [ebp-12, esi]
p dd        [ebp-8]
c dd        [ebp-4, ecx]

.code main
main:
    push ebp
    mov ebp, esp
    sub esp, 116
    pushall

    mov byte ptr [ebp-16], 0
    mov dword ptr [ebp-4], 0
    mov esi, 0
    mov ecx, 0
    lea ebx, [ebp-116]
    mov eax, 0

    for:
    cmp esi, 100
    jge eFor

        mov edx, 0
        mov dl, [ebx+esi]
        push edx
        call upcase
        add esp, 4
        mov [ebx+esi], al

        mov dl, [ebx+esi]
        push edx
        call cara
        add esp, 4
        add ecx, eax

    inc esi
    jmp for
    eFor:

    mov eax,  0
    add esp, 116
    pop ebp
    popall
    ret

.stack upcase
old ebp     [ebp]
ret addr    [ebp+4]
c db        [ebp+8]

.code upcase
upcase:
    push ebp
    mov ebp, esp
    pushall

    mov eax, [ebp+8]

    if:
    cmp al, 'z'
    jg eIf
    cmp al, 'a'
    jl eIf

        add al, 'Z'
        sub al, 'z'

    eIf:
    pop ebp
    popall
    ret


.stack cara
old ebp     [ebp]
ret addr    [ebp+4]
c db        [ebp+8]

.code cara
cara:
    push ebp
    mov ebp, esp
    pushall

    mov eax, [ebp+8]
    push eax
    call upcase
    add esp, 4

    if:
    cmp al, 'Z'
    jg else
    cmp al, 'A'
    jl else

        mov eax, 1

    jmp eIf
    else:

        mov eax, 0

    eIf:

    pop ebp
    popall
    ret
