.DATA
dim dd 50
v dd dim dup (?)

.CODE

main:
    push ebp
    mov ebp, esp
    sub esp, 16
    ;i=[ebp-4]
    ;j=[ebp-8]
    ;m=[ebp-12]
    ;conta=[ebp-16]

    lea ebx, [v]

    mov [ebp-4], 0  ;for (i=0;i<DIM;i++)
    for:
    mov esi, [ebp-4]
    cmp esi, [dim]
    jge eFor

        call rand
        mov [ebx+esi*4], eax

    inc [ebp-4]
    eFor:

    mov [ebp-12], 0 ;m=0
    mov [ebp-16], 0 ;conta=0

    mov [ebp-4], 0  ;for (i=0;i<DIM-1;i++)
    dec [dim]
    for2:
    mov esi, [ebp-4]
    cmp esi, [dim]
    jge eFor2

        push [ebx+esi*4]
        pop [ebp-12]
        mov [ebp-8], 2

        if:
        cmp [ebp-12], 0
        jge eIf

            neg [ebp-12]

        eIf:

        mov eax, [ebp-12]   ;while (j<m)
        while:
        cmp [ebp-8], eax
        jge eWhile

            if2:
            mov edx, 0
            idiv [ebp-8]
            mov eax, [ebp-12]
            cmp edx, 0
            jne eIf2

                mov [ebp-8], eax

            eIf2:

            inc [ebp-8]

        eWhile:

        if3:
        cmp [ebp-8], eax
        jne eIf3

            inc [ebp-16]

        eIf3:

    inc [ebp-4]
    eFor2:

    mov eax, [ebp-16]
    add esp, 16
    pop ebp
    ret
