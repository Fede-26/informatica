//AV05-pr.c
/* include */

#define DIM 50

int V[DIM];

int main()
{
	int i,j;
	int m;
	int conta;
	for (i=0;i<DIM;i++)
		V[i]=rand();

	m=0;
	conta=0;
	for(i=0;i<DIM-1;i++)
	{
		m=V[i];
		j=2;
		if (m<0)
			m=-m;
		while (j<m)
		{
			if (m%j==0)
				j=m+1;
			else j++;
		}
		if (j==m)
			conta++;
	}

	return (conta);
}
