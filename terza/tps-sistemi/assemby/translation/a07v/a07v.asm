.DATA

V DD 100 dup (?)
i DD ?
c DD ?
ptr DD ?
j DD ?

.CODE

main:

    PUSH EBP
    MOV EBP, ESP

    MOV [i], 0      ;i=0
    LEA EAX, [j]    ;ptr = &j
    MOV [ptr], EAX  ;
    MOV EAX, [j]    ;ptr = j
    MOV [ptr], EAX  ;
    MOV [c], 0      ;c=0
    LEA EBX, [V]    ;EBX=&V

    for:          ;for(i=0; i<100; i++)
    MOV ESI, [i]
    CMP ESI, 100
    JGE eFor

        MOV [j], 2

        while:
        MOV ECX, [j]
        CMP ECX, [EBX+ESI*4]
        JGE eWhile

            if:
            MOV EAX, [EBX+ESI*4]
            MOV EDX, 0
            IDIV ECX        ;EAX = EDX:EAX / ECX
                            ;EDX = EDX:EAX % ECX
            CMP EDX, 0
            JNE eIf

                MOV EAX, [EBX+ESI*4]
                ADD EAX, 2
                MOV [j], EAX

            eIf:

            INC [j]

        JMP while
        eWhile:

        if2:
        MOV ECX, [j]
        CMP ECX, [EBX+ESI*4]
        JLE eIf2

            INC [c]

        eIf2:

    INC [i]
    JMP for
    eFor:

    POP EBP
    RET
