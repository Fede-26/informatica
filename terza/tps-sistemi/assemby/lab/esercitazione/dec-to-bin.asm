.MODEL small  
.STACK 

.DATA
    numero DW 2345 

.CODE
.STARTUP
start:
    
    MOV AX, [numero]
    
    ciclo:
    
        MOV DX, 0

        MOV CX, 2
        DIV CX
        MOV [numero], AX
        
        ADD DL, 30h
        MOV AH, 02
        INT 21h
        
        MOV AX, [numero]
        
    CMP AX, 0
    JNE ciclo        


    
    
.EXIT
END   
       
