.MODEL small  
.STACK 

.DATA
    numero DW 2465
    migliaia DW ?
    migliaiastr DB " migliaia",0Dh,0Ah,"$"
    centinaia DW ?
    centinaiastr DB " centinaia",0Dh,0Ah,"$"
    decine DW ?
    decinestr DB " decine",0Dh,0Ah,"$"
    unita DW ?
    unitastr DB " unita",0Dh,0Ah,"$"

.CODE
.STARTUP
start:

;    2465/1000
;    2 | 465/100
;    2 | 4 | 65/10
;    2 | 4 | 6 | 5

    MOV DX, 0
    MOV AX, [numero]
    MOV CX, 1000
    DIV CX
    MOV [migliaia], AX
    MOV AX, DX
    
    MOV DX, 0
    MOV CX, 100
    DIV CX
    MOV [centinaia], AX    
    MOV AX, DX

    MOV DX, 0
    MOV CX, 10
    DIV CX
    MOV [decine], AX
    MOV [unita], DX
    
    
    MOV DX, [migliaia] ;numero da stampare
    ADD DL, 30h
    MOV AH, 02
    INT 21h
    
    LEA DX, [migliaiastr]
    MOV AH, 9
    INT 21h

    MOV DX, [centinaia] ;numero da stampare
    ADD DL, 30h
    MOV AH, 02
    INT 21h
    
    LEA DX, [centinaiastr]
    MOV AH, 9
    INT 21h

    MOV DX, [decine] ;numero da stampare
    ADD DL, 30h
    MOV AH, 02
    INT 21h
    
    LEA DX, [decinestr]
    MOV AH, 9
    INT 21h

    MOV DX, [unita] ;numero da stampare
    ADD DL, 30h
    MOV AH, 02
    INT 21h
    
    LEA DX, [unitastr]
    MOV AH, 9
    INT 21h

    
    
.EXIT
END   
       
