;Zotti Federico
;Verifica di assembly laboratorio - esercizio 1
;2021/05/07

;Dati due numeri di 2 byte memorizzati in due variabili (dividendo e divisore) eseguire la divisione
;come serie DI sottrazioni e memorizzare il risultato ed il resto IN altre due variabili.

.MODEL small  
.STACK 

.DATA
    dividendo DW 34
    divisore DW 7
    risultato DW 0
    resto DW ?
    
    msgerr DB "Impossibile dividere per zero",0Dh,0Ah,"$"
    msgdivisore DB "Divisore: $"
    msgdividendo DB "Dividendo: $"
    msgrisultato DB "Risultato: $"
    msgresto DB "Resto: $"
    nl DB 0Dh,0Ah,"$"

.CODE
.STARTUP
start:

    MOV AX, divisore    ;stampa errore se il divisore e' 0
    CMP AX, 0
    JE error
    
    
    ;PRINT DIVIDENDO
    LEA DX, msgdividendo
    MOV AH, 9
    INT 21h
    
;stampa il numero a piu cifre
;    for(i=10000; i>0; i=i/10)
;    {
;        print(num/i)
;        num = num%i
;    }
    MOV BX, 10000
    MOV CX, dividendo
    forprint1:
    CMP BX, 0
    JLE eForprint1
    
        MOV DX, 0
        MOV AX, CX
        DIV BX

        MOV CX, DX  ;cx(num) = num%i
        
        MOV DX, AX  ;stampa num/i
        ADD DL, 30h
        MOV AH, 02
        INT 21h

    MOV DX, 0
    MOV AX, BX
    MOV BX, 10
    DIV BX
    MOV BX, AX
    
    JMP forprint1
    eForprint1:
    
    ;stampa \n
    LEA DX, nl
    MOV AH, 9
    INT 21h
    
    
    ;PRINT DIVISORE
    LEA DX, msgdivisore
    MOV AH, 9
    INT 21h
    
;stampa il numero a piu cifre
;    for(i=10000; i>0; i=i/10)
;    {
;        print(num/i)
;        num = num%i
;    }
    MOV BX, 10000
    MOV CX, divisore
    forprint2:
    CMP BX, 0
    JLE eForprint2
    
        MOV DX, 0
        MOV AX, CX
        DIV BX

        MOV CX, DX  ;cx(num) = num%i
        
        MOV DX, AX  ;stampa num/i
        ADD DL, 30h
        MOV AH, 02
        INT 21h

    MOV DX, 0
    MOV AX, BX
    MOV BX, 10
    DIV BX
    MOV BX, AX
    
    JMP forprint2
    eForprint2:
    
    ;stampa \n
    LEA DX, nl
    MOV AH, 9
    INT 21h
    
    
    ;DIVIDE IL NUMERO
    
    MOV BX, dividendo   ;utilizzo i registri al posto delle variabili
    MOV CX, divisore
    while:              ;finche' il dividendo e' maggiore del divisore
    CMP BX, CX
    JL eWhile

        SUB BX, CX      ;sottraggo il divisore al dividendo
        INC risultato   ;incremento il risultato
    
    JMP while
    eWhile:
    
    MOV resto, BX       ;sincronizzo la variabile resto


    ;PRINT RISULTATO
    LEA DX, msgrisultato
    MOV AH, 9
    INT 21h
    
;stampa il numero a piu cifre
;    for(i=10000; i>0; i=i/10)
;    {
;        print(num/i)
;        num = num%i
;    }
    MOV BX, 10000
    MOV CX, risultato
    forprint3:
    CMP BX, 0
    JLE eForprint3
    
        MOV DX, 0
        MOV AX, CX
        DIV BX

        MOV CX, DX  ;cx(num) = num%i
        
        MOV DX, AX  ;stampa num/i
        ADD DL, 30h
        MOV AH, 02
        INT 21h

    MOV DX, 0
    MOV AX, BX
    MOV BX, 10
    DIV BX
    MOV BX, AX
    
    JMP forprint3
    eForprint3:
    
    ;stampa \n
    LEA DX, nl
    MOV AH, 9
    INT 21h



    ;PRINT RESTO
    LEA DX, msgresto
    MOV AH, 9
    INT 21h
    
;stampa il numero a piu cifre
;    for(i=10000; i>0; i=i/10)
;    {
;        print(num/i)
;        num = num%i
;    }
    MOV BX, 10000
    MOV CX, resto
    forprint4:
    CMP BX, 0
    JLE eForprint4
    
        MOV DX, 0
        MOV AX, CX
        DIV BX

        MOV CX, DX  ;cx(num) = num%i
        
        MOV DX, AX  ;stampa num/i
        ADD DL, 30h
        MOV AH, 02
        INT 21h

    MOV DX, 0
    MOV AX, BX
    MOV BX, 10
    DIV BX
    MOV BX, AX
    
    JMP forprint4
    eForprint4:
    
    ;stampa \n
    LEA DX, nl
    MOV AH, 9
    INT 21h


JMP fine

error:

LEA DX, msgerr ;stampa l'errore
MOV AH, 09h
INT 21h

fine:

.EXIT
END   
       
