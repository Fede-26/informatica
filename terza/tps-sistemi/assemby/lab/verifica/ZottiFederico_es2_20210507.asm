;Zotti Federico
;Verifica di assembly laboratorio - esercizio 2
;2021/05/07

;Convertire IN binario un numero memorizzato IN una variabile DI 2 byte.
;Stampare a video l�equivalente binario (non importa l�ordine se dalla cifra pi� significativa o meno
;significativa)

.MODEL small  
.STACK 

.DATA

    num DW 1024
    bin DW ?    ;variabile temporanea per la conversione,
                ;serve per non perdere il valore num,
                ;non contiene il valore convertito
    
    msgdec DB "Numero in decimale: $"
    msgbin DB "Numero in binario (in ordine inverso): $"
    nl DB 0Dh,0Ah,"$"

.CODE
.STARTUP
start:

print:

    LEA DX, msgdec
    MOV AH, 9
    INT 21h
    
;stampa il numero a pi� cifre
;    for(i=10000; i>0; i=i/10)
;    {
;        print(num/i)
;        num = num%i
;    }
    MOV BX, 10000
    MOV CX, num
    forprint:
    CMP BX, 0
    JLE eForprint
    
        MOV DX, 0
        MOV AX, CX
        DIV BX

        MOV CX, DX  ;cx(num) = num%i
        
        MOV DX, AX  ;stampa num/i
        ADD DL, 30h
        MOV AH, 02
        INT 21h

    MOV DX, 0
    MOV AX, BX
    MOV BX, 10
    DIV BX
    MOV BX, AX
    
    JMP forprint
    eForprint:
    
    

    ;stampa \n
    LEA DX, nl
    MOV AH, 9
    INT 21h
    
    ;stampa il numero in binario
    LEA DX, msgbin
    MOV AH, 9
    INT 21h


conversione:

    MOV AX, num
    MOV bin, AX

    ciclo:
    
        MOV DX, 0   ;effettua divisioni di 2 successive
        MOV CX, 2
        DIV CX
        MOV bin, AX
        
        ADD DL, 30h ;stampa ogni volta il resto
        MOV AH, 02
        INT 21h
        
        MOV AX, bin
        
    CMP AX, 0
    JNE ciclo

    ;stampa \n
    LEA DX, nl
    MOV AH, 9
    INT 21h
.EXIT
END   
       
