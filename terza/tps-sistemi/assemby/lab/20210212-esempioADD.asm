.MODEL small

.STACK

.DATA
    num1 DB 25
    num2 DB 20
    numB1 DW 120
    numB2 DW 255
    risultato DB ?
    risultatoB DW ?

.CODE
.STARTUP

    INC num1
    DEC num2
    
    MOV AL, num1        ;risultato = num1 + num2
    ADD AL, num2
    MOV risultato, AL
    
    INC numB1
    
    MOV AX, numB1       ;risultatoB (16bit) = numB1(16bit) + numB2(16bit)
    ADD AX, numB2
    MOV risultatoB, AX

.EXIT
END