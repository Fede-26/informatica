.MODEL small
.STACK
.DATA
    num1 DB 15
    num2 DB 30
    msg1 DB "num1 > num2$"
    msg2 DB "num1 < num2$"
    msg3 DB "num1 = num2$"

.CODE
.STARTUP
    MOV AL, num1
    CMP AL, num2
    JL minore
    JE uguale
    
;maggiore:
    ;load msg1 for printing
    LEA DX, msg1
    JMP stampa

minore:
    ;load msg2 for printing
    LEA DX, msg2
    JMP stampa

uguale:
    ;load msg3 for printing
    LEA DX, msg3

stampa:
    ;print
    MOV AH, 9
    INT 21h

.EXIT
END