;somma con overflow
.MODEL small

.STACK

.DATA
    num1 DB 150
    num2 DB 200
    risultato DB ?

.CODE
.STARTUP

    MOV AL, num1
    MOV AH, 0
    
    MOV DL, num2
    MOV DH, 0
    
    ADD AX, DX
    
    MOV risultato, AX


.EXIT
END