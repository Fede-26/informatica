; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
;esempi di dichiarazioni
num1        DB 5    ;num1 da 1 byte inizializzato al valore 5 (decimale)
num2        DB 0Ah  ;num2 da 1 byte con valore esadecimale
num3        DB ?    ;variabile non inizializzata
stringa1    DB '1234' ;dichiarazione di stringa
stringa2    DB 'hello world',0Dh,0Ah   ;stringa con caratteri per il ritorno a capo
vettore     DW 129, 10, 52, 4, 6, 320   ;vettore con numeri a 16bit
vettore1    DB 50 DUP(?)    ;vettore di 50 elementi da 1 byte non inizializzato
CR          EQU  0Dh        ;dichiarazione di costanti
LF          EQU  0Ah
stringa3    DB 'hello world',CR,LF

.CODE
.STARTUP



.EXIT
END   
       
