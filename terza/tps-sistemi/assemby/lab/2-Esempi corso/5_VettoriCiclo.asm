; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
; area per la dichiarazione delle variabili 
dim         DW 6
vettore     DW 9, 10, 2, 4, 6, 17
risultato   DW ?

.CODE
.STARTUP

MOV AX, 0   
MOV CX, dim         ;si carica nel contatore la dimensione
MOV DI, 0           ;si azzera il registro DI (Destination Index)

inizio:
    ADD AX, vettore[DI] ;l'indirizzo dell'operando e' dato dalla somma tra
                        ;l'indirizzo della variabile e il contenuto del registro
    ADD DI, 2           ;si incrementa di 2 byte per passare al valore successivo
    DEC CX              ;decremento del contatore
    CMP CX,0            ;istruzione di confronto che agisce sul registro dei flag
    JNZ inizio          ;si controlla il flag di zero, se diverso salta all'inizio
    

MOV risultato, AX

.EXIT
END   
       
