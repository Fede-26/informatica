; dichiarazione dei segment
.MODEL small  
.STACK 
.DATA
num1        DB ?    
num2        DB ?  
.CODE
.STARTUP     
        
        ; Lettura da tastiera del primo numero
        ; il valore ascii letto viene inserito in AL
        MOV AH,01h ;servizio DOS 01 per lettura da tastiera
        INT 21h  
        ;si sottrae il valore ascii 30h per ottenere la cifra inserita
        SUB AL,30h
        MOV num1,AL 
        
        ;lettura secondo numero
        MOV AH,01h 
        INT 21h          
        SUB AL,30h
        MOV num2,AL
        
        ;infine si esegue la sottrazione tra num1-num2 con risultato in BL
        MOV BL,num1
        SUB BL,num2        
        ;in caso di risultato negativo il flag CF viene impostato a 1
        
        
        ; stampa a video del risultato
        ; il carattere da stampare deve essere inserito in DL 
        ADD BL,30h         
        MOV DL,BL
        MOV AH,02h ;servizio DOS 02 per output
        INT 21h
.EXIT
END   
       
