; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
; area per la dichiarazione delle variabili
valore1     DW 8
valore2     DW 4
risultato   DW ?

.CODE
.STARTUP

mov AX, valore1
ADD AX, valore2
MOV risultato, AX

.EXIT
END            
