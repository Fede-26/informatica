; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
; area per la dichiarazione delle variabili 
num db 4

.CODE
.STARTUP
MOV AL,num         
ADD AL,48d           ; si somma 48 (codice ASCII di 0) al numero
	        
MOV DL,AL             ;carico in DL il valore
MOV AH,2             ;stampa il codice ASCII corrispondente al numero
INT 21H
        
MOV AH,4ch           ;chiude il programma
INT 21h

.EXIT
END   
       
