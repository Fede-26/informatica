
        

; dichiarazione dei segment
.MODEL small 

.STACK 

.DATA
;esempio 1:divisione tra due numeri di 1 byte 
; 201/7 = 28 (in AL) con resto 5 (in AH)
num1	db 201
num2	db 7

;esempio 2:divisione tra un numero di  2 byte ed un numero di 1 byte 
;ATTENZIONE se il risultato supera un byte si verifica errore (esempio2.1) 
; 290/7 = 41 (in AL) con resto 3 (in AH)
num3	dw 290
num4	db 7	



;esempio 3: divisione tra due numeri di 2 byte 
; 2500/6 = 416 (in AX) con resto 4 (in DL)
;Attenzione: da errore se si dichiara num2 di 1 byte perch� il risultato sta in 2 byte e il resto non pu� stare in AX
num5 dw 2500
num6 dw 6

.CODE
.STARTUP  

    MOV AL,num1
    DIV num2
    
    MOV AX,num3
    DIV num4    
    
    ;esempio 2.1: errore perche' il risultato super un byte 
    MOV num3, 10000
    MOV num4, 5
    MOV AX,num3
    ;DIV num4  ;decommentare per visualizzare l'errore 
    
    MOV AX,num5
    MOV dx,0 ; evita eventuali errori
    DIV num6
    
    MOV AH, 4Ch
    INT 21h

.EXIT
END            
