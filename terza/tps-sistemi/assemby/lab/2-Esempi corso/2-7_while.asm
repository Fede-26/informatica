; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
var         DB 10
msg1        DB "Fine ciclo$"     

.CODE
.STARTUP 
    
ciclo:  CMP var,0
        JBE fine ; se var <=0 salta alla fine del ciclo
        ; blocco di codice.............
        DEC var
        JMP ciclo        

fine:   
        LEA dx, msg1
        mov ah, 9
        int 21h  


.EXIT
END   
       
