; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
; area per la dichiarazione delle variabili
vettore     DW 9, 10, 2, 4
risultato   DW ?

.CODE
.STARTUP

mov AX, 0 
ADD AX, vettore
ADD AX, vettore+2
ADD AX, vettore+4
ADD AX, vettore+6

MOV risultato, AX

.EXIT
END            
