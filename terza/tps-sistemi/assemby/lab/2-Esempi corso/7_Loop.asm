; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
; area per la dichiarazione delle variabili 
msg1 db "Oggi e' una bella giornata$"
newLine db 0Dh,0Ah,"$"


.CODE
.STARTUP

MOV CX,15

inizio:

	lea dx, msg1
	mov ah, 9
	int 21h
	
	
	;stampa nuova riga
	lea dx, newLine
	mov ah, 9
	int 21h
	
	
	
LOOP inizio ;decrementa CX e salta	


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;        
MOV AH,4ch           ;chiude il programma
INT 21h
        
.EXIT
END   
       
