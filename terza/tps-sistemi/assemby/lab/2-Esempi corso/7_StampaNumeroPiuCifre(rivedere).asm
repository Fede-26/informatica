; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
; area per la dichiarazione delle variabili 
numero	dw 5423
divisore dw 10000
dieci dw 10

.CODE
.STARTUP
inizio:	
	MOV AX,numero		;carichiamo il numero in AX per poter effettuare la divisione
	MOV DX,0			;IMPORTANTE: prima della divisione conviene azzerare DX
	DIV divisore        ;AX=AX/divisore AX conterra' la parte intera della divisione 
	
	
	CALL stampa	
	 
	MUL divisore		;si sottrae la cifra gia' stampata
	SUB numero,AX
	
	MOV AX,divisore		;si divide il divisore per 10
	DIV dieci
	MOV divisore,AX	
	
	CMP divisore,0		;verifica se abbiamo terminato
	JG inizio 
	
	JMP fine
	
	    
stampa:
        ADD AL,48d
        MOV DL,AL
        MOV AH,2             ;stampa il codice ASCII corrispondente al numero
        INT 21H
        ;si rimette a posto la cifra
        SUB AL,48
        MOV AH,0
        RET
fine:        
		MOV AH,4ch           ;chiude il programma
        INT 21h
        
.EXIT
END   
       
