; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
num1        DB 25    
num2        DB 20
msg1        DB "operando1 minore di operando2$" 
msg2        DB "operando1 maggiore di operando2$" 

.CODE
.STARTUP 
    
        MOV AL,num1
        CMP AL,num2
        JB  lbl1
        lea dx, msg2
        JMP stampa

lbl1:   lea dx, msg1
stampa: mov ah, 9
        int 21h   


.EXIT
END   
       
