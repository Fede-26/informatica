; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
num1        DB 150    
num2        DB 200  
risultato   DW ?
 .CODE
.STARTUP 
   
        
        ; somma tra num1 e num2 con risultato che supera 8 bit
        ;e' necessario utilizzare due registri a 16 bit 
        ; altrimenti ci perdiamo la parte eccedente
        MOV AL,num1 
        MOV AH, 0
        MOV DL,num2
        MOV DH,0
        ADD AX,DX 
        MOV risultato,AX
 
.EXIT
END   
       
