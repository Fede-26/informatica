
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

include 'emu8086.inc'
ORG    100h

LEA    SI, msg1       ; set up pointer (SI) to msg
                      ; to ask for the number
CALL   print_string   ; print message that SI points to

LEA    DI, buffer     ; set up pointer (DI) to input buffer
MOV    DX, bufSize    ; set size of buffer
CALL   get_string     ; get name & put in buffer

LEA    SI, newln      ; point at CR/LF / Hello message 
CALL   print_string   ; print message that SI points to

RET                   ; return to operating system.

; data
msg1   DB "Enter your name: ", 0  
newln  DB 13, 10
       DB "Hello, "
buffer DB 20 DUP (0)  ; input buffer for get_string   
bufSize = $-buffer    ; calculates size of buffer

DEFINE_GET_STRING
DEFINE_PRINT_STRING
END                   ; directive to stop the compiler.




