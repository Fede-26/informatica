; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
; area per la dichiarazione delle variabili 
msg1 db " Stringa inserita: $"
msgend db  "premi un tasto per terminare$"

;dichiara un array di 3 byte con i codici di invio e il carattere terminatore ($)
newLine db 0Dh,0Ah,"$"

;dichiara un array di 20 byte inizializzati con il $
stringa DB 20 dup('$')


.CODE
.STARTUP

lea dx, stringa
mov ah, 0ah
int 21h

;aggiunta della terminazione stringa
mov bx, dx
mov ah, 0
mov al, ds:[bx+1]
add bx, ax ; punta alla fine della stringa.
mov byte ptr [bx+2], '$' ; inserisce un $ alla fine. 


lea bx, stringa

mov ch, 0
mov cl, [bx+1] ; prende la lunghezza di una stringa. 


;stampa di due righe
lea dx, newLine
mov ah, 9
int 21h
lea dx, newLine
mov ah, 9
int 21h
lea dx, msg1
mov ah, 9
int 21h


; Stampa della stringa (la stringa parte 2 locazioni di memoria)
lea dx, stringa+2
mov ah, 9
int 21h




;stampa nuova riga
lea dx, newLine
mov ah, 9
int 21h



;messaggio di fine programma
lea dx, msgend
mov ah, 9
int 21h

;Premi un tasto per terminare    
    mov ah, 1
    int 21h

MOV AH,4ch           ;chiude il programma
INT 21h

        
.EXIT
END   
       
