; dichiarazione dei segment
.MODEL small  
.STACK 

.DATA
num1        DB 25    
num2        DB 20  
numB1       DW 120
numB2       DW 255 
risultato   DB ?
risultatoB  DW ?
 
.CODE
.STARTUP 
    
        INC num1
        DEC num2 
        ;ADD num1,num2  ;non possibile 
        ; somma tra num1 e num2 si passa sempre per un registro
        MOV AL,num1
        ADD AL,num2  
        MOV risultato,AL
        ;INC e DEC possono operare anche su numeri a 16 bit 
        INC numB1
        ; somma tra due numeri a 16 bit
        MOV AX,numB1 
        ADD AX,numB2   
        MOV risultatoB, AX
 
.EXIT
END   
       
