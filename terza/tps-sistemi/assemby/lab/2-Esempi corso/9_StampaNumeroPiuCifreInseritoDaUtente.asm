; dichiarazione dei segment
.MODEL small  
.STACK 
DW 50 DUP (?) ;dichiaro un'area senza contenuto grande 50 word o 100 byte
.DATA
; area per la dichiarazione delle variabili 
messaggio		DB 	"Programma che visualizza un numero di piu' cifreinserite dall'utente.",10,13,'$'
messaggio2		DB 10,13, 'Fine programma.$'			


.CODE
.STARTUP

                LEA DX,messaggio  ;stampa messaggio
				MOV AH,9h
				INT 21h
		    	MOV CL,'0'
ciclo:	        MOV AH,8h ;servizio che aspetta che l'utente prema un tasto
				INT 21h   ;il carattere letto viene inserito in AL
				CMP AL,13 ;se si inserisce un invio salta a fineciclo
				JE fineciclo
				CMP AL,'0' ;'0' Equivale a scrivere 30h, che e' il codice ascii dello 0
				JC ciclo
				CMP AL,'9' ;'0' Equivale a scrivere 39h, che e' il codice ascii dello 9
				JA ciclo
				MOV DL,AL
				MOV AH,2h  ;stampa la cifra inserita
				INT 21h
				INC CL
				CMP CL,5
				JA ciclo
				
				LEA DX,messaggio2
				MOV AH,9h
				INT 21h

fineciclo:
				MOV AH,4Ch;
				INT 21h;sto richiamando il servizio dos per chiudere il programma
 					
        
.EXIT
END   
       
