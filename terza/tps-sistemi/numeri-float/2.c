#include <stdio.h>
#include <math.h>
void bin(int x)
{
 while (x>0)
 {
  printf("%d",x%2);
  x = x/2;
 }
 printf("\n");
}

int main(int argc, char ** argv)
{
 float f = 1.0;
 float old=10.0;
 float * pf;
 int * pi;
 int i=0;
 pf = &f;
 pi = pf;
 while (f>0.0)
 {
  printf("%e %d -->",f,*pi );
  bin( (*pi) );
  f = f/2.0;
  i ++;
 }
 printf("eseguite %i iterazioni\n",i);
 return 0;

}