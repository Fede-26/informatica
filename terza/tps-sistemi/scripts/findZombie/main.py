#!/usr/bin/env python

"""
Trova tutti i processi zombie.
"""

import os


def findStr(fn, stringa):
    """
    fn: file name
    stringa: stringa da cercare
    """

    if os.path.isfile(fn):
        with open(fn) as f:
            s = f.read()
        return bool(stringa in s)

    return False


def status(fn):
    """
    fn: file name
    """

    with open(fn) as f:
        s = f.read()
    s = s.split("\n")
    for x in s:
        if "State:" in x:
            y = x.split("\t")
            #print(y[1][0])
            return y[1][0]


l = os.listdir("/proc")
pids = list()

for x in l:
    if x.isdigit():
        pids.append(x)

for x in pids:
    path = "/proc/{}/status".format(x)
    if findStr(path, "zombie"):
        print(">> Zombie trovato: {}".format(x))

for x in pids:
    path = "/proc/{}/status".format(x)
    #status(path)
    if status(path).lower() == "z":
        print(">> Zombie trovato: {}".format(x))
