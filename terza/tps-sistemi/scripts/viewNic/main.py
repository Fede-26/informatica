#!/usr/bin/env python3

import subprocess as sp

def links():
    """
    Ritorna la lista delle nics
    """
    cmd = "ip link".split(" ")
    r = sp.run(cmd, capture_output=True)
    s = r.stdout.decode()
    t = s.split("\n")
    l = list()
    for x in t:
        if len(x) > 0:
            if x[0].isdigit():
                y = x.split(": ")
                l.append(y[1])

    return l


if __name__ == "__main__":
    print(links())
