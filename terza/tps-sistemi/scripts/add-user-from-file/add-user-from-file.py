#!/usr/bin/env python3
#Zotti Federico

import os
import string

def backup():	#crea un backup dei file di sistema
	"""
	rsync -a /etc/passwd .
	rsync -a /etc/group .
	"""

	os.system("rsync -a /etc/passwd .")
	os.system("rsync -a /etc/group .")
	print(">> backup done")


def leggiFile(fn):	#legge gli utenti e le passwd e li memorizza in una lista
	nuova = list()
	with open(fn, 'r') as f:
		s = f.read()
	s = s.split('\n')
	for i in s:
		nuova.append(i.split("\t"))
	return nuova


def creaUtente(u):	#crea l'utente
	if checkUsername(u[0]) and checkPasswd(u[1]):
		os.system("useradd " + u[0] + " -p $(echo " + u[1] + " | openssl passwd -1 -stdin)")
		print(">> utente " + u[0] + " creato")
	else:
		print("!> formato non consentito")


def delUtente(u):	#elimina l'utente
	if checkUsername(u[0]) and checkPasswd(u[1]):
		os.system("userdel " + u[0])
		print(">> utente " + u[0] + " eliminato")

	else:
		print("!> formato non consentito")


def checkUsername(name):	#controlla se il nome utente è valido
	"""
	FROM MAN PAGE OF USERADD
		It is usually recommended to only use usernames that begin with a lower case letter or an underscore, followed by lower case letters,
		digits, underscores, or dashes. They can end with a dollar sign. In regular expression terms: [a-z_][a-z0-9_-]*[$]?

		On Debian, the only constraints are that usernames must neither start with a dash ('-') nor plus ('+') nor tilde ('~') nor contain a
		colon (':'), a comma (','), or a whitespace (space: ' ', end of line: '\n', tabulation: '\t', etc.). Note that using a slash ('/') may
		break the default algorithm for the definition of the user's home directory.

		Usernames may only be up to 32 characters long.
	"""

	primaLet = list(string.ascii_lowercase) + ['_']
	consentiti = primaLet + list(string.digits) + ['-']

	if not name[0] in primaLet:
		return False

	for i in range(len(name)):
		if i == len(name)-1:
			if not name[i] in consentiti+['$']:
				return False
		elif not name[i] in consentiti:
			return False

	return True

def checkPasswd(passwd):	#controlla se la password è valida
	#consentiti = caratteri accettati
	consentiti = list(string.ascii_letters) + list(string.digits) + list(string.punctuation)

	for i in passwd:
		if not (i in consentiti):
			return False
	return True



users = leggiFile("utenti")		#memorizza in users gli utenti
print(">> utenti: ", users)

if not input("?> proseguire? [y/N] ").lower() == 'y':
	exit()

backup()	#esegue il backup

for user in users:		#per ogni utente
	creaUtente(user)	#crea l'utente

if not input("?> eliminarli? [y/N] ").lower() == 'y':
	exit()

for user in users:		#per ogni utente
	delUtente(user)		#elimina l'utente