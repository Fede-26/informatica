#!/usr/bin/env bash
exit

## Zotti Federico
# questi sono comandi che si possono eseguire nella shell.
# non è implementato il controllo dei caratteri validi nei nomi e nelle password.

## Formato file 'utenti'
utente1	passwd1
pipppo	test
pluto	passwordsicura

## Backup dei file

rsync -a /etc/passwd . && rsync -a /etc/group .


## Aggiunge gli utenti dal file 'utenti' (tab separated)

awk '{ system( "useradd "$1" -p $vibing(echo "$2" | openssl passwd -1 -stdin)" ) }' utenti && echo "Utenti aggiunti"

## sudo variant

sudo awk '{ system( "useradd "$1" -p $(echo "$2" | openssl passwd -1 -stdin)" ) }' utenti && echo "Utenti aggiunti"


## Descrizione:
# awk divide ogni riga di una stringa in colonne, i valori si possono utilizzare con le variabili $n.
# la funzione system di awk permette di eseguire un comando shell.
# il comando useradd ha come parametri il nome dell'utente ($1) e la password criptata con crypt (in questo caso usando l'output del comando openssl).
# se il comando non riporta errori viene stampato un avviso.


## Elimina tutti gli utenti aggiunti dopo la creazione del backup (anche utenti non presenti nel file)

diff /etc/passwd passwd | awk -F ':' '$1 ~ /</ { system( "userdel " substr( $1, 3 ) ) }' && echo "Utenti rimossi"

## sudo variant

diff /etc/passwd passwd | sudo awk -F ':' '$1 ~ /</ { system( "userdel " substr( $1, 3 ) ) }' && echo "Utenti rimossi"


## Descrizione:
# diff mostra le differenze tra due file, in questo caso /etc/passwd e il backup fatto in precedenza.
# l'output viene mandato ad awk (con separatore ':' e non '\t'), il quale lo divide e applica il comando solo alla riga la cui prima colonna contiene il carattere '<',
# questo perchè diff ha come prima riga di output il numero delle righe diverse.
# come il comando precedente esegue userdel con parametro il nome dell'utente appena aggiunto
# (vengono rimossi i primi 2 caratteri perchè sono i simboli generati da diff).


## Elimina tutti gli utenti presenti nel file

awk '{ system( "userdel " $1 ) }' utenti && echo "Utenti rimossi"

## sudo variant

sudo awk '{ system( "userdel " $1 ) }' utenti && echo "Utenti rimossi"

## Descrizione:
# Quasi identico al comando per aggiungerli.