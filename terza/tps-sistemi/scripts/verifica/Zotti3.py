#!/usr/bin/env python3

import os

def status(pid):
    """
    funzione che ritorna lo status di un processo come lista
    pid: pid del processo
    """
    path = "/proc/{}/status".format(pid)
    with open(path, "r") as f:
        content = f.read()

    content = content.split("\n")
    content2 = list()
    for line in content:
        content2.append(line.split(":\t"))

    return content2

pid1 = input("?> Inserire PID: ")

pid2 = list() #figli di pid1

procs = os.listdir("/proc")
for proc in procs:
    if proc.isdigit():
        if status(proc)[6][1] == pid1:
            pid2.append(proc)

print(">> Figli (tot: {}):".format(len(pid2)))
for x in pid2:
    print(x)

pid3 = list() #nipoti di pid1

for proc in procs:
    if proc.isdigit():
        if status(proc)[6][1] in pid2:
            pid3.append(proc)

print("\n>> Nipoti (tot: {}):".format(len(pid3)))
for x in pid3:
    print(x)

print("\n>> Totale progenie: {}".format(len(pid2)+len(pid3)))
