#!/usr/bin/env python3

import os


def status(pid):
    """
    funzione che ritorna lo status di un processo come lista
    pid: pid del processo
    """
    path = "/proc/{}/status".format(pid)
    with open(path, "r") as f:
        content = f.read()

    content = content.split("\n")
    content2 = list()
    for line in content:
        content2.append(line.split(":\t"))

    return content2


def getUid(stat):
    """
    funzione per prendere l'UID dallo status
    stat: status del processo
    """
    for line in stat:
        if line[0] == "Uid":
            u = line[1]
            u = u.split("\t")
            return u[0]

    return "Error"


def getOwner(stat):
    """
    funzione che ritorna il proprietario del processo
    stat: status del processo
    """
    uid = getUid(stat)

    path = "/etc/passwd"
    with open(path, "r") as f:
        content = f.read()
    lines = content.split("\n")
    lines2 = list()
    for line in lines:
        lines2.append(line.split(":"))

    for x in lines2:
        if uid == x[2]:
            return x[0]

    return "Utente non trovato"


def getPpid(stat):
    """
    funzione per prendere il PPID dallo status
    stat: status del processo
    """
    for line in stat:
        if line[0] == "PPid":
            return line[1]

    return "Error"


def getSSB(stat):
    """
    funzione che ritorna lo stato di speculation store bypass
    """
    for line in stat:
        if line[0] == "Speculation_Store_Bypass":
            return line[1]

    return "Error"

pid = input("?> PID del processo: ")
stat = status(pid)

print(">> UID: {}".format(getUid(stat)))
print(">> Owner: {}".format(getOwner(stat)))
print(">> PPID: {}".format(getPpid(stat)))

if getSSB(stat) == "thread vulnerable":
    print(">> Processo sensibile a: Speculation_Store_Bypass")


print("\n>> Processi vulnerabili a Speculation_Store_Bypass")
v = 0
for proc in os.listdir("/proc"):
    if proc.isdigit():
        if getSSB(status(proc)) == "thread vulnerable":
            print(proc)
            v += 1
if v:
    print("Totale processi: {}".format(v))
else:
    print("Nessun processo")
