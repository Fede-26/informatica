#!/usr/bin/env python3

import sys
import os

def arg():
    if len(sys.argv)==3:
        return sys.argv[1], sys.argv[2]
    else:
        print("ERRORE: inserire i parametri giusti (percorso e stringa da cercare)")
        exit()

def searchFile(directory, toFind):
    """
    str directory: il percorso della directory che scansiona
    """
    try:
        files = os.listdir(directory)
    except:
        print("error")
        return

    files.sort()
    for x in files:
        percorso = directory+"/"+x
        if os.path.isdir(percorso):
            searchFile(percorso, toFind)

        else:
            with open(percorso, "r") as f:
                content = f.read()

            content = content.lower()
            found = content.count(toFind)
            if found:
                print("stringa trovata {} volta/e in {}".format(found, percorso))

path, toFind = arg()

searchFile(path, toFind.lower())

print("File con dent")
searchFile(path, "dent")
