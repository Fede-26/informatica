#!/usr/bin/env python

import os
import sys

def viewFile(directory, depth):
    """
    str directory: il percorso della directory che scansiona
    int depth: distanza dalla directory di partenza
    """
    #os.chdir(directory)
    #print(">> dir", directory)
    try:
        files = os.listdir(directory)
    except:
        print("error")
        return

    files.sort()
    for file in files:
        if os.path.isdir(directory+"/"+file):
            print("\t"*depth, "Dir:", file)
            viewFile(directory+"/"+file, depth+1)
            
        else:
            print("\t"*depth, file)

def parametro():
    if len(sys.argv)>1:
        return sys.argv[1]
    else:
        return "."
        
viewFile(parametro(), 0)
