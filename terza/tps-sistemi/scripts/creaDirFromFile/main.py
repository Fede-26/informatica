#!/usr/bin/env python

"""
Crea file o directory da un file preesistente.
Read the org file for more info.

prima lettera ->    f file
                    f file2
                    d dir
"""

import os


def readFile(fn):
    """
    desc: legge un file e ritorna una lista con ogni riga
    divisa in tipo di file e percorso

    str fn: file name
    """
    with open(fn, "r") as f:
        lines = f.read()

    lines = lines.split("\n")
    lines2 = list()
    for line in lines:
        if len(line) > 0:
            lines2.append(line.split(" "))

    return lines2


def makeFile(fn):
    """
    desc: crea un file in un dato percorso (se relativo, dalla cartella di
    esecuzione)
    str fn: file name (path)
    """

    if os.path.isfile(fn):
        return False

    if "/" in fn:
        path = fn.split("/")    #lista con gli elementi del percorso
        makeDir("/".join(path[:-1]))

    with open(fn, "w") as f:
        pass

    return True


def makeDir(dn):
    """
    desc: crea una directory in un dato percorso (se relativo, dalla cartella
    di esecuzione)
    str dn: directory name (path)
    """

    if os.path.isdir(dn):
        return False

    # os.system("mkdir -p " + dn)
    path = dn.split("/")
    path2 = ""
    for x in path:
        # makedir path2
        path2 += x + "/"
        if not os.path.isdir(path2):
            os.mkdir(path2)

    return True


def main():
    toMake = readFile("config.txt")

    for x in toMake:
        if x[0] == "f":
            # make a file
            if makeFile(x[1]):
                print(">> file {} creato".format(x[1]))
            else:
                print("!> file {} non creato perchè già presente".format(x[1]))
        elif x[0] == "d":
            # make a directory
            if makeDir(x[1]):
                print(">> directory {} creata".format(x[1]))
            else:
                print("!> directory {} non creata perchè già presente".
                      format(x[1]))

        else:
            print("!> errore nel file")


if __name__ == "__main__":
    main()
