#!/bin/python3
import os

def leggiFile(pf):
	with open(pf,"r") as f:       #apre il file
		s = f.read()       #legge e stampa il contenuto
	print("Ho aperto il file, ecco il contenuto: \n",s)     #mostra il contenuto
	if input("Si desidera eliminarlo? [S/n]: ").lower() == "s":     #se si vuole eliminare il file
		os.remove(pf)
		print("Dovrei aver eliminato il file... Verifico il contenuto della cartella: ", os.listdir()) #verifica eliminazione

def creaFile(pf):       #se non c"è il file
	if input("Si desidera crearlo? [S/n] ").lower() == "s":     #se si vuole creare un file

		with open(pf,"w") as f:
			f.write("")
	
		print("Dovrei aver creato il file qui: ", os.getcwd(), "\nVerifico il contenuto della cartella: ", os.listdir())
		
		if input("Si desidera eliminarlo? [S/n]: ").lower() == "s":      #se si vuole eliminare il file
			os.remove(pf)
			print("Dovrei aver eliminato il file... Verifico il contenuto della cartella: ", os.listdir()) #verifica eliminazione



print ("Lo script è qui: ", os.getcwd())

os.chdir("/home/mirko/Documenti")	#dove cerca il file

nfile = input("Dimmi il nome del file da cercare qui: " + os.getcwd() + "\n>>")

files = os.listdir()
if nfile in files:
	if os.path.isfile(nfile):
		print("Leggo il file")
		leggiFile(nfile)

else:
	print("Creo il file")
	creaFile(nfile)

