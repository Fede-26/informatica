#!/bin/python3
import os
import sys      #per gli errori

posizione = os.getcwd()      #trova e stampa il percorso file
print ('Lo script è qui: ',posizione)

percorso = "/home/mirko/Documenti"       #dove cerca il file
print('Dimmi il nome del file da cercare qui: ',percorso)
nfile = input()

pf = percorso + '/' + nfile
print("Cerco il file: ",percorso)  


try:
	with open(pf,"r") as f:       #apre il file
		s = f.read()       #legge e stampa il contenuto
		print('Ho aperto il file, ecco il contenuto: \n',s)     #mostra il contenuto
		if input('Si desidera eliminarlo? [S/n]: ').lower() == 's':     #se si vuole eliminare il file
			os.remove(pf)
			print("Dovrei aver eliminato il file... Verifico il contenuto della cartella: ",os.listdir(percorso)) #verifica eliminazione
except:         #se non c'è il file
	print('Qualcosa è andato storto... error type: ',sys.exc_info()[0])    #stampa il tipo di errore
	if input('Si desidera crearlo? [S/n] ').lower() == 's':     #se si vuole creare un file
		try:
			open(pf,"w")
			print("Dovrei aver creato il file qui: ",percorso,"\nVerifico il contenuto della cartella: ",os.listdir(percorso))
			if input('Si desidera eliminarlo? [S/n]: ').lower() == 's':      #se si vuole eliminare il file
				os.remove(pf)
				print("Dovrei aver eliminato il file... Verifico il contenuto della cartella: ",os.listdir(percorso)) #verifica eliminazione
		except:
			print('Errore nella creazione del file, error type: ',sys.exc_info()[0])    #se non riesce a creare il file
