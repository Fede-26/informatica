#!/usr/bin/env python

"""
Trova una stringa in un file.
"""

import sys
import os

def findStr(fn, stringa):
# Prova a vedere se fn esiste
    if os.path.isfile(fn):
        with open(fn) as f:
            s = f.read()

        return bool(stringa in s)

    else:
        return False


if len(sys.argv) == 3:
    fn, stringa = sys.argv[1], sys.argv[2]
else:
    print("!> Inserire file e stringa")
    sys.exit()

print(findStr(fn, stringa))
