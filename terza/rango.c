#include<stdio.h>

int main()
{
	int m, n;
	int p, minore, ad, rango=0;
	int i, x;
	int nulla=1;
	
	printf("Righe matrice: "); 
	scanf("%d", &m);
	while(m==0 || m>3)
	{
		printf("ERRORE! Inserisci numero diverso da 0 e minore o uguale a 3");
		printf("\nRighe matrice: "); 
		scanf("%d", &m);
	} 

	printf("Colonne matrice: "); 
	scanf("%d", &n);
	while(n==0)
	{
		printf("ERRORE! Inserisci numero diverso da 0");
		printf("\nColonne matrice: "); 
		scanf("%d", &n);
	} 
	
	int matrice[m][n];
	
	//p = dimensione minore (p<=m && p<=n)
	if(m<n)
		p=m;
	else
		p=n;
		
	int a[p][p]; //creo sottomatrice
			
	//Riempio matrice
	for(i=0;i<m;i++)
		for (x=0;x<n;x++)
		{
			printf("Inserisci elemento %d %d: ",i+1,x+1);
			scanf("%d",&matrice[i][x]);
		}
	
	//Verifico se la matrice � nulla
	for(i=0;i<m;i++)
		for (x=0;x<n;x++)
			if(matrice[i][x]!=0)
				nulla=0;
	if (nulla==1)
	{
		printf("\nIl rango della matrice: 0 (matrice nulla)");
		return;
	}
		
	//Stampo matrice inserita
	for(i=0;i<m;i++)
	{
		printf("\n");
		for (x=0;x<n;x++)
			printf("%d	",matrice[i][x]);
	}
	
	while(p>0)
	{
		//Riempio sottomatrice
		for(i=0;i<p;i++)
			for (x=0;x<p;x++)
				a[i][x]=matrice[i][x];
		
		//Calcolo minore (determinante sottomatrice)
		//P non pu� essere maggiore di 3 per i controlli sull'input di m e n
		i=0;
		x=0;
		if (p==1)
			minore = p;
		else if (p==2)	
			minore = a[x][i]*a[x+1][i+1] - a[x+1][i]*a[x][i+1];
		else if (p==3)
		{
			//regola di sarrus per matrici 3x3
			minore = (a[x][i]   * a[x+1][i+1] * a[x+2][i+2])
				   + (a[x][i+1] * a[x+1][i+2] * a[x+2][i])
				   + (a[x][i+2] * a[x+1][i]   * a[x+2][i+1]);

			ad =  (a[x][i+2] * a[x+1][i+1] * a[x+2][i])
				+ (a[x][i+1] * a[x+1][i]   * a[x+2][i+2])
				+ (a[x][i]   * a[x+1][i+2] * a[x+2][i+1]);

			minore-= ad;
		}
		
		printf("\n\nStampa della sottomatrice e del suo minore: \n");
		for(i=0;i<p;i++)
		{
			printf("\n");
			for (x=0;x<p;x++)
				printf("%d	",a[i][x]);
		}
		printf("Minore: %d", minore);
		
		if(minore!=0 && p>rango)
			rango = p;
			
		p--;
	}
	
	//Comunico RANGO
	printf("\n\nIl rango della matrice: %d", rango);
	return 0;
}
