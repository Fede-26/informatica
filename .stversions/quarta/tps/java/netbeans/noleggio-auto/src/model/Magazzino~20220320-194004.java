package model;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Magazzino {
    
    public static List <Auto> elencoDisponibili= new ArrayList();
    public static List <Auto> elencoNoleggiate= new ArrayList();

    public static List<Auto> getElencoDisponibili() {
        return elencoDisponibili;
    }
    
    public static List<Auto> getElencoNoleggiate(){
        return elencoNoleggiate;
    }
    
    public Auto gDisponibili(int i) //ritorna l'elemento dediserato da elenco persone se esiste
    { 
        return elencoDisponibili.get(i);
    }
    
    public Auto gNoleggiate(int i) //ritorna l'elemento dediserato da elenco persone se esiste
    { 
        return elencoNoleggiate.get(i);
    }
    
    public int SizeDisponibili(){
      return elencoDisponibili.size();
    }
    
    public int SizeNoleggiate(){
      return elencoDisponibili.size();
    }
    
    public boolean AggiungiN(Auto a){
        return elencoNoleggiate.add(a);
    }
    
    public boolean AggiungiD(Auto n){
        return elencoDisponibili.add(n);
    }
    
    public Auto eliminaD(int i){
        return elencoDisponibili.remove(i);
    }
    
    public Auto eliminaN(int i){
        return elencoNoleggiate.remove(i);
    }
     
}
