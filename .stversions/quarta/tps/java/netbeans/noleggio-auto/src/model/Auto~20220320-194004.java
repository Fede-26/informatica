package model;

public class Auto 
{
    public String marca;
    public String modello;
    public String targa;
    public String costo;
    public String data_noleggio;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
    
    public String getModello() {
        return modello;
    }
    
    public void setModello(String modello) {
        this.modello = modello;
    }

    public String getTarga() {
        return targa;
    }
    
    public void setTarga(String targa) {
        this.targa = targa;
    }

    public String getCosto() {
        return costo;
    }
    
    public void setCosto(String costo) {
        this.costo = costo;
    }


    public Auto(String marca, String modello, String targa, String costo, String data_noleggio) {
        this.marca = marca;
        this.modello = modello;
        this.targa = targa;
        this.costo = costo;
        this.data_noleggio= data_noleggio;
    }
    
    public Auto(){
    
    }
    
    
    
    
}
